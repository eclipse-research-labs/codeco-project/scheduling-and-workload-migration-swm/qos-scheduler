<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# How to Install a Release with the Helm Chart

For further details on what the parameters do, please see document
“[Getting Started][1]”.

```bash
YOUR_EMAIL=your.email@yourcompany.com
YOUR_TOKEN=your-GitLab-access-token
YOUR_REGISTRY=cr.siemens.com
helm install qostest . \
     --namespace=he-codeco-swm --create-namespace \
     --set global.image.credentials.username=${YOUR_EMAIL} \
     --set global.image.credentials.password=${YOUR_TOKEN} \
     --set global.image.credentials.email=${YOUR_EMAIL} \
     --set global.image.credentials.registry=${YOUR_REGISTRY}
```

If you make changes to the Helm chart and just want to apply those:

```bash
helm upgrade qostest . \
     --namespace=he-codeco-swm --create-namespace \
     --set global.image.credentials.username=${YOUR_EMAIL} \
     --set global.image.credentials.password=${YOUR_TOKEN} \
     --set global.image.credentials.email=${YOUR_EMAIL} \
     --set global.image.credentials.registry=${YOUR_REGISTRY}
```

To uninstall the release:

```bash
helm uninstall qostest -n he-codeco-swm
kubectl delete crds --all
```

[1]: ../../Documentation/getting-started.md
