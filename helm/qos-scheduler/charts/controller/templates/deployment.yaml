# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

apiVersion: apps/v1
kind: Deployment
metadata:
  name: controller-manager
  namespace: {{ .Values.global.qosNamespace }}
  labels:
    app: controller-manager
spec:
  selector:
    matchLabels:
      app: controller-manager
  replicas: 1
  template:
    metadata:
      labels:
        app: controller-manager
    spec:
      securityContext:
        runAsNonRoot: true
      imagePullSecrets:
      {{ include "imagePullSecrets" . | indent 8 }}
      containers:
      - name: kube-rbac-proxy
        image: gcr.io/kubebuilder/kube-rbac-proxy:v0.8.0
        args:
        - "--secure-listen-address=0.0.0.0:8443"
        - "--upstream=http://127.0.0.1:8080/"
        - "--logtostderr=true"
        - "--v=10"
        ports:
        - containerPort: 8443
          name: https
      - name: controller
        command:
        - /bin/controller
        args:
        {{- toYaml .Values.controllerArgs | nindent 10 }}
          - "--optimizer-service={{ .Values.optimizerService }}.{{ .Values.global.qosNamespace }}:80"
        ports:
        - containerPort: 8081
          name: health 
        - containerPort: 8080
          name: metrics
        image: {{ include "qosImage" . | quote }}
        imagePullPolicy: {{ include "imagePullPolicy" . }}
        securityContext:
          allowPrivilegeEscalation: false
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        resources:
          {{- toYaml .Values.resources | nindent 12 }}
      serviceAccountName: {{ .Values.serviceAccount.name }}
      terminationGracePeriodSeconds: 10
