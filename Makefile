# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

.DEFAULT_GOAL:=help
.RECIPEPREFIX=>
ifeq ($(filter undefine,$(value .FEATURES)),)
$(error Make v$(MAKE_VERSION) is not supported. $\
        Use at least v3.82)
endif

VERSION_ALPINE?=3.18.6

define mkdir # path(s)
$(eval $(1): ; mkdir -p $$@)
endef
define copy-rule # dst,src,pat
$(1)/: ; mkdir -p $$@
$(1)/$(3): $(2)/$(3) | $(1)/ ; cp $$< $$@
endef
define cleaner # name,paths,dep
.PHONY: clean-$(1)
.IGNORE: clean-$(1)
clean: clean-$(1)
clean-$(1): $(3)
>rm -rf $$(to-remove) $(2)
endef
clean-up=$(eval $(call cleaner,$(1),$(2)))

ifndef SCHEDULER_IGNORE_SETTINGS
ifneq "$(USER)" ""
-include $(USER).mk
endif
endif
SCHEDULER_BUILD_MODE?=local
ifndef CI
ifeq ($(SCHEDULER_BUILD_MODE),docker)
useDocker:=yes
endif
else
ifeq ($(CI),local)
useDocker:=yes
endif
export CI_REGISTRY_IMAGE?=cr.siemens.com/itp_cloud_research/qos-scheduler
regis:=$(CI_REGISTRY_IMAGE)/
endif
export DOCKER_CLI_HINTS=false
define run # tool,image,dir,var(s)
$(if $(useDocker),docker run$(if $(DRFLAGS), $(DRFLAGS)) --rm $\
--volume $${PWD}:/project --workdir=/project$(if $(3),/$(3)) $\
$(if $(4),$(addprefix --env ,$(4)) )$($(2)IN):$($(2)V) $(1),$\
$(if $(3),cd $(3) ; )$(if $(4),$(strip $(4)) )$(1))
endef

lparen:=(
rparen:=)
comma:=,
space:=$(subst X, ,X)

make-list=$(subst $(space),$(comma),$(1))
arch=$(strip $(1)$(lastword $(subst /, ,$(2))))
pkg-name=$(subst /,-,$(1))

define get-curr-branch
{ git symbolic-ref --short HEAD 2>/dev/null $\
  || git describe --all HEAD $\
     | sed -ne "/^heads/{s|heads/||;p;}" $\
     | head -n 1 ; $\
}
endef

local-build=$(if $(if $(strip $(CI)),$(strip $(filter local,$(CI))),t),t,)
local-arch:=$(if $(filter-out x86_64,$(shell uname -m)),arm64,amd64)
on-darwin:=$(if $(strip $(subst Darwin,,$(shell uname))),,yes)
curr-branch:=$(if $(local-build),$(shell $(get-curr-branch)),$\
    $(or $(CI_COMMIT_REF_SLUG),$(shell $(get-curr-branch))))
main-branch:=$(if $(strip $(filter-out $(or $(CI_DEFAULT_BRANCH),main master),$(curr-branch))),,yes)

moduleV:=$(strip $(if $(main-branch),$(shell cat VERSION),$(curr-branch)))
moduleN:=$(strip $(shell \
    sed -Ene "/^module/{s/^module[ \t]+([^ \t]+).*/\1/;p;}" scheduler/go.mod))
moduleNN:=$(lastword $(subst -, ,$(lastword $(subst /, ,$(moduleN)))))

archs-ci:=$(if $(local-build),$(local-arch),$\
    $(or $(strip $(subst $(comma),$(space),$(runner_archs))),amd64 arm64))
archs-pi:=$(or $(strip $(subst $(comma),$(space),$(target_archs))),amd64 arm64)
archs-dirs:=deliv/bin/ $(addprefix deliv/bin/,$(addsuffix /,$(archs-pi)))

#
#   #    # ###### #      #####
#   #    # #      #      #    #
#   ###### #####  #      #    #
#   #    # #      #      #####
#   #    # #      #      #
#   #    # ###### ###### #
#

define awk-gen-help:=
function line(t,d) {printf "%3s%-10s %s\n"," ",t,d}
/^## [a-zA-Z-]+ ::/ {
    n=split(substr($$0,4),a," :: ");
    line(a[1],a[2]);
    next
}
/^##\+ / { line("",substr($$0,5)); next }
/^##/ { printf "%s\n",substr($$0,4) }
END { if (n>0) { printf "\n" } }
endef
userFiles:=$(wordlist 2,$(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))

.PHONY: help
##
## Use one of the following goals:
##
## help :: Show this help text
help:
>@awk '$(strip $(awk-gen-help))' $(firstword $(MAKEFILE_LIST))
>$(if $(userFiles),@awk '$(strip $(awk-gen-help))' $(userFiles))

#
#    ####   ####  #    # ###### #####  #    # #      ###### #####
#   #      #    # #    # #      #    # #    # #      #      #    #
#    ####  #      ###### #####  #    # #    # #      #####  #    #
#        # #      #    # #      #    # #    # #      #      #####
#   #    # #    # #    # #      #    # #    # #      #      #   #
#    ####   ####  #    # ###### #####   ####  ###### ###### #    #
#

gitlab-ci:=.gitlab-ci.yml

.PHONY: scheduler all
## scheduler :: Create deliverables and documentation (synonym: all)
scheduler all: c-images lint reuse $(gitlab-ci) p-images chart

versions:=SCHEDULER:VERSION\
    SCRIPT:build/ci/script/VERSION \
    GOENV:build/ci/goenv/VERSION \
    REUSE:build/ci/reuse/VERSION \
    MANIFEST:build/ci/manifest/VERSION \
    HELM:build/ci/helm/VERSION \
    SHELLCHECK:build/ci/shellcheck/VERSION

$(foreach v,$(versions),$(eval \
VERSION_$(firstword $(subst :, ,$(v)))=$$(strip \
    $(if $(main-branch),$\
        $$(shell cat $(lastword $(subst :, ,$(v)))),$\
        $(curr-branch)))))

define sed-set-versions
$(foreach v,$(1),-e "/^ *VERSION_$(firstword $(subst :, ,$(v))):/s/[0-9.]+/$\
    $(shell cat $(lastword $(subst :, ,$(v))))/")
endef

sed-in-situ:=sed -i$(if $(on-darwin), "")

$(gitlab-ci): $(patsubst %:,,$(subst :,: ,$(versions)))
>$(sed-in-situ) -E $(call sed-set-versions,$(versions)) $@

.PHONY: defaults
## defaults :: Update user’s settings file using
##+    mode=(local|docker)
defaults: mode?=docker
defaults: $(USER).mk
>grep "SCHEDULER_BUILD_MODE?=" $< >/dev/null \
    && $(sed-in-situ) -Ee "/^(SCHEDULER_BUILD_MODE\\?=).+$$/s//\\1$(mode)/" $<\
    || printf "SCHEDULER_BUILD_MODE?=$(mode)\n" >>$<

define user-defaults
# Variable SCHEDULER_BUILD_MODE defines which tools the Makefile uses:
#   - local: Use tools installed locally
#   - docker: Use tools in containers
SCHEDULER_BUILD_MODE=local
endef

$(USER).mk: export defaults=$(user-defaults)
$(USER).mk:
>@if [ ! -e $@ ] ; then printf "$${defaults}\n" >$@ ; else touch $@ ; fi

#
#    ####        # #    #   ##    ####  ######  ####
#   #    #       # ##  ##  #  #  #    # #      #
#   #      ##### # # ## # #    # #      #####   ####
#   #            # #    # ###### #  ### #           #
#   #    #       # #    # #    # #    # #      #    #
#    ####        # #    # #    #  ####  ######  ####
#

.PHONY: c-images clean-c-images
## c-images :: Create images for pipeline

c-image-names:=script manifest goenv reuse helm shellcheck

include build/images.mk
include $(foreach n,$(c-image-names),build/ci/$(n)/Makefile)

.PHONY: clean-c-images
clean-c-images: $(foreach n,$(c-image-names),clean-ci-$(n))

$(foreach n,$(c-image-names),$(eval $\
    need-$n=$(if $(useDocker),$($(n)-$(local-arch)F))))

#
#   #####  ####   ####  #       ####
#     #   #    # #    # #      #
#     #   #    # #    # #       ####
#     #   #    # #    # #           #
#     #   #    # #    # #      #    #
#     #    ####   ####  ######  ####
#

export GOPROXY?=https://proxy.golang.org,direct
GO_VARS=CGO_ENABLED=0
GO=$(GO_VARS) go
GOFLAGS+=-ldflags "-s -w$(if $(CI_PIPELINE_ID),\
 -X 'main.toolVersion=$(moduleV)+$(CI_PIPELINE_ID)')"

$(call mkdir,$(archs-dirs))
.PHONY: tools
## tools :: Build all executables

define tools-for/files # pkgN,pkgD,srcs
$(1)GF:=$$(wildcard scheduler/$(if $(2),$(2)/)*.go) $(3)
$(1)PN:=$(moduleN)$(if $(2),/$(2))
endef

define tools-for/vars # var,pkgN,fileExt,arch,image
$(1):=deliv/bin$(if $(4),/$(4))/$(2)$(3)
.PHONY: $$(subst deliv/,,$$($(1)))
$$(subst deliv/,,$$($(1))): $$($(1))$(if $(5), pc-$(5)$(if $(4),-$(4))
>rm -f $$(imageDir)/$(5)$(if $(4),-$(4))/$$(notdir $$($(1))))
tools: $$($(1))
clean-tools: to-remove+=$$($(1))
fake-tools: tool-names+=$$($(1))
endef

define tools-for/goCmd # cmd,dir,arch,pkgN
$(if $(2),cd $(2) ;\
)$(if $(3),GOOS=linux GOARCH=$(3) \
)$(GO) $(1) $$(GOFLAGS)$(if $(strip $(GO_FLAGS)), $(GO_FLAGS)) -o $(if $(2),../)$$@ $$($(4)PN)
endef

define tools-for/main # pkgN,pkgD,arch,var,image
$(call tools-for/vars,$(4),$(1),,$(3),$(5))
$$($(4)): $$(filter-out %_test.go,$$($(1)GF)) $$(need-goenv) | $$(dir $$($(4)))
>$(call tools-for/goCmd,build,scheduler,$(3),$(1))
endef

define tools-for/tests # pkgN,pkgD,arch,var
$(call tools-for/vars,$(4),$(1),.test,$(3))
$$($(4)): $$($(1)GF) $$(need-goenv) | $$(dir $$($(4)))
>$(call tools-for/goCmd,test -c -cover,scheduler,$(3),$(1))
>chmod +x $$@
endef

define tools-for # pkgN,pkgD,{main|test},image
$(eval $(call tools-for/files,$(1),$(2))\
$(foreach p,native amd64 arm64,\
$(if $(filter main,$(3)),
$(call tools-for/main,$(1),$(2),$(if $(filter-out native,$(p)),$(p)),$(1)B_$(p),$(if $(filter-out native,$(p)),$(or $(4),$(1)))))\
$(if $(filter test,$(3)),
$(call tools-for/tests,$(1),$(2),$(if $(filter-out native,$(p)),$(p)),$(1)BT_$(p)))))
endef

$(call tools-for,controller,cmd/controller,main)
$(call tools-for,scheduler,cmd/scheduler,main)
$(call tools-for,network-k8s,cmd/network-k8s,main)
$(call tools-for,network-l2sm,cmd/network-l2sm,main)
$(call tools-for,network-tsn,cmd/network-tsn,main)
$(call tools-for,network-topology,cmd/network-topology,main)
$(call tools-for,node-daemon,cmd/node-daemon,main)
$(call tools-for,validator,cmd/validator,main)
$(call tools-for,optimizer-stub,cmd/optimizer-stub,main)
$(call tools-for,cni-vlan,cmd/cni-vlan,main)

GO_FLAGS=--tags=mock
$(call tools-for,application,application,test)
$(call tools-for,cmd-cni-vlan,cmd/cni-vlan,test)
$(call tools-for,controllers,controllers,test)
$(call tools-for,infrastructure,infrastructure,test)
$(call tools-for,network-k8s-nwcontroller,network/k8s-nwcontroller,test)
$(call tools-for,network-lib,network/lib,test)
$(call tools-for,optimizer-client,optimizer/client,test)
$(call tools-for,webhooks,webhooks,test)
GO_FLAGS=

$(call clean-up,tools,)

.PHONY: fake-tools
fake-tools: | $(archs-dirs)
>for t in $(tool-names) ; do\
     if [ ! -e $${t} ] ; then\
          echo "#!/bin/sh\necho This is fake $${t}." >$${t} ;\
          chmod +x $${t} ;\
     fi ;\
 done ;\
 touch -r README.md $$(find deliv/bin -type f)

#
#   #      # #    # #####
#   #      # ##   #   #
#   #      # # #  #   #
#   #      # #  # #   #
#   #      # #   ##   #
#   ###### # #    #   #
#

.PHONY: lint clean-lint
## lint :: Perform static checks on source code
lint: go-fmt go-vet staticcheck shellcheck
clean-lint: clean-go-fmt clean-go-vet clean-staticcheck clean-shellcheck

$(call mkdir,deliv/lint/ tmp/)

.PHONY: reuse
## reuse :: Check REUSE compliance

REUSE=$(call run,reuse,reuse-amd64)
reuse-report:=deliv/lint/reuse-report.txt

reuse: $(need-reuse) | deliv/lint/
>@printf "Run reuse\n"
>@LANGUAGE=C $(REUSE) lint >$(reuse-report) ; status=$$? ;\
 [ "$${status}" == 0 ] && echo "REUSE compliant" || cat $(reuse-report) ;\
 exit $${status}
$(call clean-up,reuse,$(reuse-report))

fmt-report:=deliv/lint/gofmt-report.txt
go-pkg-paths:=$(shell find scheduler -type f -name "*.go" \
    | sed -Ee '/_test.go$$/d' -e 's/\/[^/]+$$//' -e '/\/test\//d' -e 's/^/.\//' \
    | sort -u)

.PHONY: go-fmt
go-fmt: | deliv/lint/
>@printf "Run go fmt\n"
>@printf "Files changed by “gofmt”\n\n" >$(fmt-report) ;\
 gofmt -l $(go-pkg-paths) >>$(fmt-report) ;\
 fn=`sed -e "1,2d" $(fmt-report)` ;\
 if [ -n "$${fn}" ] ; then cat $(fmt-report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "go fmt changes no files" ; fi
$(call clean-up,go-fmt,$(fmt-report))

vet-report:=deliv/lint/govet-report.txt
go-check-paths:=$(filter-out ./scheduler ./api/v1alpha1 ./assignment, \
    $(subst /scheduler/,/,$(go-pkg-paths)))

.PHONY: go-vet
go-vet: | deliv/lint/
>@printf "Run go vet\n"
>@printf "Issues found by “go vet”\n\n" >$(vet-report) ;\
 cd scheduler ;\
 GOOS=linux go vet -tags=mock $(go-check-paths) 2>&1 >>../$(vet-report) ;\
 cd .. ; txt=`sed -e "1,2d" $(vet-report)` ;\
 if [ -n "$${txt}" ] ; then cat $(vet-report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "go vet found nothing" ; fi
$(call clean-up,go-vet,$(vet-report))

STATICCHECK=$(call run,staticcheck,goenv-amd64)
STATICCHECK_OPTIONS=-f stylish -tags=mock

.PHONY: staticcheck
staticcheck: | deliv/lint/
>@rs=`awk -f scripts/reports-with-problems.awk deliv/lint/staticcheck-*.txt` ;\
 if [ -n "$${rs}" ] ; then\
 printf "\033[1m%s\033[0m\n" "Problems Reported by Staticcheck\n" ;\
 for f in $${rs} ; do printf "\n" ; cat $${f} ; done ; exit 1 ;\
 else printf "\033[1m%s\033[0m\n" "staticcheck found nothing" ;\
 fi

staticcheck-of/file=deliv/lint/staticcheck-$(call pkg-name,$(1)).txt
define staticcheck-of # pkgD
clean-staticcheck: to-remove+=$(call staticcheck-of/file,$(1))
staticcheck: $(call staticcheck-of/file,$(1))
$(call staticcheck-of/file,$(1)): $\
    $$($(call pkg-name,$(1))GF) $$(grpcSources) $$(need-goenv) | deliv/lint/
>@printf "staticcheck $(moduleN)/$(1)\n"
>@(printf "Lint Report for “scheduler/$(1)”\n\n" ; cd scheduler ;$\
 GOOS=linux $$(STATICCHECK) $$(STATICCHECK_OPTIONS) $(moduleN)/$(1)) >$$@ ;$\
 exit 0
endef
$(foreach d,$(go-check-paths),\
  $(eval $(call staticcheck-of,$(subst ./,,$(d)))))
$(call clean-up,staticcheck)

SHELLCHECK=$(call run,shellcheck,shellcheck-amd64)
SHELLCHECK_OPTIONS=--format=tty --color=never -x
shellcheck-report:=deliv/lint/shellcheck-report.txt
script-files:=$(filter-out scripts/gitlab-infos.sh,$(wildcard scripts/*.sh)) \
    scheduler/hack/maybe-update-deps.sh start_cluster-hybrid.sh label_nodes.sh \
    start_edge-cluster.sh start_cluster.sh

.PHONY: shellcheck
shellcheck: report:=$(shellcheck-report)
shellcheck: | deliv/lint/
>@printf "Run shellcheck\n"
>@printf "Report from “shellcheck”\n\n" >$(report) ;\
 $(SHELLCHECK) $(SHELLCHECK_OPTIONS) $(script-files) >>$(report) ;\
 txt="`sed -ne "3,+10p" $(report)`" ;\
 if [ -n "$${txt}" ] ; then cat $(report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "shellcheck found nothing" ; fi
$(call clean-up,shellcheck,$(shellcheck-report))

#
#    ####  #    # ######  ####  #    #
#   #    # #    # #      #    # #   #
#   #      ###### #####  #      ####
#   #      #    # #      #      #  #
#   #    # #    # #      #    # #   #
#    ####  #    # ######  ####  #    #
#

GO-JUNIT-REPORT=$(call run,go-junit-report,goenv-amd64)
GO-JUNIT-OPTIONS=-set-exit-code -p go.version=$(VERSION_GOLANG)

$(call mkdir,deliv/report/)
check-to-remove:=tmp/coverage
.PHONY: check
## check :: Run tests and get coverage
check:

define reports-for/vars # pkgN,dir
$(1)GCD:=tmp/coverage/$(1)
$(1)Cmd=$(if $(2),cd $(2) ; )$$(call run,$(if $(2),../)deliv/bin/$(1).test,script-amd64,,$\
    GOCOVERDIR=$$(if $$(useDocker),/project,$$$${PWD}$(if $(2),/..))/$$($(1)GCD))
endef
define test-report-for # main?,pkgN,pkgD
$(2)TR:=deliv/report/$(2)-tests.xml
.PHONY: report/$(2)
report/$(2): $$($(2)TR) | deliv/report/
check: report/$(2)
test-reports+=$$($(2)TR)
clean-check: to-remove+=$$($(2)TR)
$$($(2)TR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
    | $$(dir $$($(2)TR)) $$($(2)GCD)/
>$$($(2)Cmd) -test.v 2>&1 \
 | $$(GO-JUNIT-REPORT) $$(GO-JUNIT-OPTIONS) \
       -package-name $(moduleN)/$(3) >../$$@
endef
define coverage-report-for # main?,pkgN,pkgD
$(2)CR:=deliv/report/$(2)-coverage.html
report/$(2): $$($(2)CR)
clean-check: to-remove+=$$($(2)CR)
$(2)CVI:=$$($(2)GCD)/coverage.out
$$(call mkdir,$$(dir $$($(2)CVI)))
$$($(2)CR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
    | $$(dir $$($(2)CR)) $$($(2)GCD)/
$(if $(1),>$$($(2)Cmd)
>cd scheduler ; $$(GO) tool covdata textfmt -i=../$$($(2)GCD) -o ../$$($(2)CVI)
>cd scheduler ; $$(GO) tool cover -html=../$$($(2)CVI) -o ../$$@,>$$($(2)Cmd) -test.coverprofile=../$$($(2)CVI)
>cd scheduler ; $$(GO) tool cover -html=../$$($(2)CVI) -o ../$$@)
endef
define reports-for # main?,pkg
$(eval $(call reports-for/vars,$(subst /,-,$(2)),scheduler))
$(eval $(call test-report-for,$(1),$(subst /,-,$(2)),$(2)))
$(eval $(call coverage-report-for,$(1),$(subst /,-,$(2)),$(2)))
endef

$(call reports-for,,application)
$(call reports-for,,cmd/cni-vlan)
$(call reports-for,,controllers)
$(call reports-for,,infrastructure)
$(call reports-for,,network/k8s-nwcontroller)
$(call reports-for,,network/lib)
$(call reports-for,,optimizer/client)
$(call reports-for,,webhooks)
$(call clean-up,check)

.PHONY: coverage
## coverage :: Get total code coverage
coverage: $(test-reports)
>awk -f scripts/total-coverage.awk $^ | tr , .

.PHONY: check-linkage
linkage-report/tools=$(addprefix deliv/bin/$(1)/,\
    cni-vlan controller network-k8s network-l2sm network-tsn network-topology \
    node-daemon optimizer-stub scheduler validator)
linkage-report/file=deliv/report/linkage-report-$(1).xml
define linkage-report # arch
$(eval check-linkage: check-linkage-$(1)
check-linkage-$(1): $(call linkage-report/file,$(1))
clean-check-linkage: to-remove+=$(call linkage-report/file,$(1))
$(call linkage-report/file,$(1)): $(call linkage-report/tools,$(1)) $\
    $(need-script) | deliv/report/
>file $$(filter deliv/bin/%,$$^) |\
 awk -v timestamp=`date +"%FT%T"` -f scripts/check-tool-props.awk >$$@)
endef
$(foreach a,$(archs-pi),$(call linkage-report,$(a)))
$(call clean-up,check-linkage)

#
#   #    # ###### #      #    #
#   #    # #      #      ##  ##
#   ###### #####  #      # ## #
#   #    # #      #      #    #
#   #    # #      #      #    #
#   #    # ###### ###### #    #
#

helmDir=helm/qos-scheduler
tmpHelmDir:=tmp/helm
charts:=cni common controller network nodedaemon nsconfig optimizer\
    prometheusOperator qos-crds scheduler solver
helm-chart:=deliv/helm/qos-scheduler-$(moduleV).tgz
helm-index:=deliv/helm/index.yaml
helm-repo:=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api
helm-repo-path?=$(if $(main-branch),stable/charts,devel/charts)
helm-policy?=Always
helm-image-tag?=$(if $(main-branch),$(moduleV),$(curr-branch))

.PHONY: chart
## chart :: Create archive with Helm chart
chart: $(helm-chart) $(helm-index)

chartFiles:=$(wildcard $(helmDir)/*) \
    $(foreach c,$(charts),\
        $(wildcard $(helmDir)/charts/$(c)/*) \
        $(wildcard $(helmDir)/charts/$(c)/templates/*))

$(call mkdir,$(dir $(helm-chart)) $(tmpHelmDir))
$(helm-chart): $(chartFiles) | $(dir $(helm-chart)) $(tmpHelmDir)
>rsync -qru --delete $(helmDir)/ $(tmpHelmDir)
>for src in $(helmDir)/values.yaml $(helmDir)/charts/*/values.yaml ; do\
    sed -Ee "/^ +pullPolicy:/s/:.+/: $(helm-policy)/" \
        -e "1,/^# External/{/^ +tag:/s/:.+/: $(helm-image-tag)/;}" \
    $${src} >$(tmpHelmDir)$${src#"$(helmDir)"} ; \
done
>helm dependency update $(tmpHelmDir)
>helm package --app-version=$(moduleV) --version=$(moduleV) \
     -d $(dir $(helm-chart)) $(tmpHelmDir)

$(helm-index): $(chartFiles) | $(dir $(helm-chart)) $(tmpHelmDir)
>helm repo index $(dir $(helm-index)) --url $(helm-repo)/stable/charts

$(call clean-up,chart,$(dir $(helm-chart)) $(tmpHelmDir))

.PHONY: helm
## helm :: Publish Helm chart
helm: $(helm-chart) $(helm-index)
>curl --request POST --user gitlab-ci-token:$${CI_JOB_TOKEN} \
     --form "chart=@$<" $(helm-repo)/$(helm-repo-path)

#
#    ####   ####  #    # ###### #  ####
#   #    # #    # ##   # #      # #    #
#   #      #    # # #  # #####  # #
#   #      #    # #  # # #      # #  ###
#   #    # #    # #   ## #      # #    #
#    ####   ####  #    # #      #  ####
#

config-archive:=deliv/configs.tgz

.PHONY: config
## config :: Create archive with configuration files
config: $(config-archive)

config-files:=start_cluster.sh \
    $(shell find config -type f) \
    $(shell find helm -type f)

$(call mkdir,deliv/)
deliv/configs.tgz: $(config-files) | deliv/
>tar czf $@ $(config-files)

#
#   #####        # #    #   ##    ####  ######  ####
#   #    #       # ##  ##  #  #  #    # #      #
#   #    # ##### # # ## # #    # #      #####   ####
#   #####        # #    # ###### #  ### #           #
#   #            # #    # #    # #    # #      #    #
#   #            # #    # #    #  ####  ######  ####
#

.PHONY: p-images clean-p-images
## p-images :: Build images with executables

p-image-names:=cni-vlan configs controller network-k8s network-l2sm \
    network-tsn network-topology node-daemon optimizer-stub scheduler validator

clean-p-images: $(foreach i,$(p-image-names),clean-pi-$(i))

$(call production-image,configs,$(moduleV),,,\
    deliv/configs.tgz,,$(VERSION_ALPINE))
$(configsD)/configs.tgz: deliv/configs.tgz
>cp $< $@

$(foreach n,$(filter-out configs,$(p-image-names)),\
$(call tool-images,$(n),$(archs-pi)))

$(foreach n,$(filter-out configs,$(p-image-names)),\
$(call multi-arch-image,pi,$(n),$(moduleV),$(archs-pi)))

#
#   #    # ###### #####   ####  #  ####  #    #
#   #    # #      #    # #      # #    # ##   #
#   #    # #####  #    #  ####  # #    # # #  #
#   #    # #      #####       # # #    # #  # #
#    #  #  #      #   #  #    # # #    # #   ##
#     ##   ###### #    #  ####  #  ####  #    #
#

kind?=revision

version-file=$(if $(image),build/ci/$(image)/VERSION,VERSION).inc

.PHONY: version
## version :: Increment version number (default: revision)
##+    kind=(major|minor|revision)
##+    image=(script|goenv|reuse|manifest|helm|shellcheck)
version: $(version-file) $(gitlab-ci)

.PHONY: $(version-file)
$(version-file): | tmp/
>awk -v FS=. -v kind=$(kind) -f scripts/inc-version.awk $(@:.inc=) >tmp/VERSION
>mv tmp/VERSION $(@:.inc=)

.PHONY: release
## release :: Create release from current version
release: | tmp/
>scripts/release.sh

.PHONY: export
## export :: Push current or given release to target repository
##+    target=(codeco|eclipse)
##+    version=version-number (default: see VERSION)
export: version:=$(if $(version),$(version),$(moduleV))
export: | tmp/
>scripts/export.sh "$(target)" "$(version)"

#
#    ####  #      ######   ##   #    #
#   #    # #      #       #  #  ##   #
#   #      #      #####  #    # # #  #
#   #      #      #      ###### #  # #
#   #    # #      #      #    # #   ##
#    ####  ###### ###### #    # #    #
#

.PHONY: clean
.IGNORE: clean
## clean :: Remove images and generated files
clean: clean-c-images clean-p-images
>rm -rf deliv/ tmp/
