// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package base contains reusable code for network operators.
package base

import (
	"context"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// The BaseLinkReconciler implements useful functions for reconcilers
// that update logical links based on the status of physical links.
type BaseLinkReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme

	// The network implementation class of the logical links
	// that this controller manages.
	NetworkImplementation nwlib.NetworkImplementationClass
}

// GetAffectedLink determines the logical link that needs to be updated given a change
// to a physical link.
func (b *BaseLinkReconciler) GetAffectedLink(ctx context.Context, physicalName types.NamespacedName) (*crd.NetworkLink, error) {

	physicalNi, err := nwlib.NiFromNamespace(physicalName.Namespace)
	if err != nil {
		return nil, err
	}

	myNamespace := nwlib.NamespaceForNi(string(b.NetworkImplementation))
	link := &crd.NetworkLink{}
	err = b.Get(ctx, types.NamespacedName{Name: physicalName.Name, Namespace: myNamespace}, link)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		} else {
			return nil, err
		}
	}

	if link.Spec.PhysicalBase == physicalNi {
		return link, nil
	}
	return nil, nil
}
