// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package k8snw contains a network controller that manages the default Kubernetes
// network connecting all nodes.
package k8snw

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strings"

	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

const (
	// The network label for the k8s network.
	K8SNI = string(nwlib.K8sNetwork)
)

// K8sNetworkReconciler watches Kubernetes nodes and creates network links and paths for them.
type K8sNetworkReconciler struct {
	base.BaseNetworkReconciler
}

func (k *K8sNetworkReconciler) createOrUpdateNetworkTopology(ctx context.Context) error {

	// find cluster nodes
	var k8sNodes corev1.NodeList
	err := k.List(ctx, &k8sNodes)
	if err != nil {
		return err
	}

	// find network endpoints
	networkEndpoints, err := k.GetExistingNetworkEndpoints(ctx)
	if err != nil {
		return err
	}

	// create or update network topology based on cluster nodes
	newTopSpec, err := k.topologyFromKubernetesNodes(k8sNodes, networkEndpoints)
	if err != nil {
		return err
	}
	ns := nwlib.NamespaceForNi(K8SNI)
	k8sTopName := strings.ToLower(K8SNI)
	top, err := k.GetExistingTopology(ctx, K8SNI, k8sTopName)
	if err != nil {
		return err
	}
	if top != nil {
		//TODO should only update on actual changes
		top.Spec = newTopSpec
		err = k.Update(ctx, top)
		if err != nil {
			k.Log.Error(err, "failed to update network topology")
			return err
		}
		k.Log.Info("updated network topology", "name", top.Name, "namespace", top.Namespace)
	} else {
		top = &crd.NetworkTopology{
			ObjectMeta: metav1.ObjectMeta{
				Name:      k8sTopName,
				Namespace: ns,
			},
			Spec: newTopSpec,
		}
		err = k.Create(ctx, top)
		if err != nil {
			k.Log.Error(err, "failed to create network topology")
			return err
		}
		k.Log.Info("created network topology", "name", top.Name, "namespace", top.Namespace)
	}
	top.Status = crd.TopologyStatus{Status: crd.TopologyRequested}
	k.Client.Status().Update(ctx, top)
	return nil
}

func (k *K8sNetworkReconciler) handleDeletedNode(ctx context.Context, name string) error {
	var eps crd.NetworkEndpointList
	var ep crd.NetworkEndpoint
	err := k.List(ctx, &eps)
	found := false
	if err != nil {
		return err
	}
	for _, e := range eps.Items {
		if e.Spec.NodeName == name {
			ep = e
			found = true
			break
		}
	}
	// Delete the k8sni network from the corresponding network endpoint
	if found {
		nwlib.DeleteNetworkImplementation(&ep, K8SNI)
		err = k.Update(ctx, &ep)
		if err != nil {
			k.Log.Error(err, "failed to remove network map entry for K8SNI from network endpoint")
		}
	}
	return err
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networklinks,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networklinks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkpaths,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkendpoints,verbs=get;list;update;patch
//+kubebuilder:rbac:groups="",resources=nodes,verbs=get;list;watch

// Reconcile gets called whenever a Kubernetes Node is created, updated,
// or deleted. It updates NetworkLinks and NetworkPaths accordingly.
func (k *K8sNetworkReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	err := k.createOrUpdateNetworkTopology(ctx)
	if err != nil {
		k.Log.Error(err, "Failure to create or update network topology")
		return ctrl.Result{}, err
	}

	var node corev1.Node
	err = k.Get(ctx, req.NamespacedName, &node)
	if err != nil && errors.IsNotFound(err) {
		err := k.handleDeletedNode(ctx, req.NamespacedName.Name)
		if err != nil {
			k.Log.Error(err, "Failure to handle node deletion")
		}
		return ctrl.Result{}, err
	} else if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, err
}

// SetupWithManager initializes the controller and make it watch Kubernetes nodes.
func (k *K8sNetworkReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.NetworkEndpoint{}).Complete(k)
}
