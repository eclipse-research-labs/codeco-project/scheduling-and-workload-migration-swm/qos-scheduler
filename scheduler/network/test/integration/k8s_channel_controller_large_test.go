// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("k8s channel controller", func() {

	const (
		timeout  = time.Second * 20
		interval = time.Millisecond * 250
	)

	var (
		ctx context.Context
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("processing a channel", func() {
		It("channel on k8s network should progress to running", func() {
			channel := nwlib.MakeChannel("requested-channel", "default", string(nwlib.K8sNetwork))
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "requested-channel", Namespace: "default"}, channel)
				if err != nil {
					return false
				}
				return channel.Status.Status == crd.ChannelRunning
			}, timeout, interval).Should(Equal(true))
		})
		It("should delete a canceled channel", func() {
			channel := nwlib.MakeChannel("canceled-channel", "default", string(nwlib.K8sNetwork))
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "canceled-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "canceled-channel", Namespace: "default"}, channel)
				if err != nil {
					return false
				}
				channel.Status = crd.ChannelStatus{
					Status: crd.ChannelCanceled,
				}
				err = k8sClient.Status().Update(ctx, channel)
				if err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(Equal(true))

			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "canceled-channel", Namespace: "default"}, channel)
				return err != nil && errors.IsNotFound(err)
			}, timeout, interval).Should(Equal(true))
		})
	})
})
