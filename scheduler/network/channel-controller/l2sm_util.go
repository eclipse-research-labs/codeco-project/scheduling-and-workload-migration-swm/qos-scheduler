// SPDX-FileCopyrightText: 2024 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/netip"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	l2sm "siemens.com/qos-scheduler/scheduler/api/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

// MakeVlink creates a vlink CR for a Channel. There is a 1-to-1 relationship between a Channel and a vlink.
// The vlink CR is handled by the NetMA component in CODECO.
// MakeVlink labels the vlink resource with the kv pair "channel": ChannelId (channel namespace and name)
// It does not write the resource to Kubernetes.
func MakeVlink(channel crd.Channel, path *crd.NetworkPath) l2sm.L2Network {

	var config vlinkConfig
	var patharr = []string{""}

	// Read values for vlink from Channel CR
	// create vlink CR (goes to NetMA component)
	/*	{
		"overlay-parameters": {
		"path": {
			"name": "app1-w2-app1-w1",			=> channel ID (combination of channel namespace and name)
			"FromEndpoint": "netma-test-3",		=> channel.Spec.ChannelFrom
			"ToEndpoint": "netma-test-2",		=> channel.Spec.ChannelTo
			"links": ["25117dc4"],				=> get from NetworkPath: Path=channel.Spec.ChannelPath; Links[]=Path.Spec.Links
			"capabilities": {
			"bandwidthBits": "1000000",			=> channel.Spec.MinBandwidthBits
			"latencyNanos": "10e-3"				=> channel.Spec.MaxDelayNanos / 1E9
			}
		}
	} */

	config.OverlayParameters.Path.Name = infrastructure.ChannelId(channel)
	config.OverlayParameters.Path.FromEndpoint = channel.Spec.ChannelFrom
	config.OverlayParameters.Path.ToEndpoint = channel.Spec.ChannelTo
	patharr[0] = LinkstrFromPath(path)
	config.OverlayParameters.Path.Links = patharr
	config.OverlayParameters.Path.Capabilities.BandwidthBits = fmt.Sprintf("%d", channel.Spec.MinBandwidthBits)
	config.OverlayParameters.Path.Capabilities.LatencyNanos = fmt.Sprintf("%f", float64(channel.Spec.MaxDelayNanos)/1e9)

	// Marshal the struct back to a JSON string
	vlinkConfig, _ := json.MarshalIndent(config, "", "  ")
	vlinkConfigStr := string(vlinkConfig)
	vlink := l2sm.L2Network{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "l2sm.l2sm.k8s.local/v1",
			Kind:       "L2Network",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      channel.Name, // using only the channel name is unambiguous, since vlink is currently in the same namespace as the channel
			Namespace: channel.Namespace,
			Labels: map[string]string{
				"channel": infrastructure.ChannelId(channel),
			},
		},
		Spec: l2sm.L2NetworkSpec{
			Type:   l2sm.NetworkTypeVlink,
			Config: &vlinkConfigStr,
		},
	}
	return vlink
}

// LinkstrFromPath returns the NetworkLinks that form the specified NetworkPath as concatenated string
func LinkstrFromPath(path *crd.NetworkPath) string {
	linkstr := ""
	for _, l := range path.Spec.Links {
		if linkstr == "" {
			linkstr = l
		} else {
			linkstr = linkstr + " " + l
		}
	}
	return linkstr
}

type vlinkConfig struct {
	OverlayParameters struct {
		Path struct {
			Name         string   `json:"name"`
			FromEndpoint string   `json:"FromEndpoint"`
			ToEndpoint   string   `json:"ToEndpoint"`
			Links        []string `json:"links"`
			Capabilities struct {
				BandwidthBits string `json:"bandwidthBits"`
				LatencyNanos  string `json:"latencyNanos"`
			} `json:"capabilities"`
		} `json:"path"`
	} `json:"overlay-parameters"`
}

// GetPodIpv4Cidr returns the second and third IPv4 address from the passed Ipam CRD as strings in CIDR format (e.g. "10.10.1.1/29")
func GetPodIpv4Cidr(ipam crd.Ipam) (string, string, error) {

	myprefix, err := netip.ParsePrefix(ipam.Status.IPv4Cidr)
	if err != nil {
		return "", "", err
	}

	// First IP address reserved for gateway
	// Second IP address used for source Pod
	// Third IP adress used for target Pod
	ipAddr1 := myprefix.Addr()
	ipAddr1 = ipAddr1.Next()
	ipAddr2 := ipAddr1.Next()

	// Append the prefix length to each IP address to form CIDR format
	sourceIP := fmt.Sprintf("%s/%d", ipAddr1.String(), myprefix.Bits())
	targetIP := fmt.Sprintf("%s/%d", ipAddr2.String(), myprefix.Bits())
	return sourceIP, targetIP, nil
}

// GetPodIpv4 returns the second and third IPv4 address from the passed Ipam CRD as strings (e.g. "10.10.1.1")
func GetPodIpv4(ipam crd.Ipam) (string, string, error) {

	myprefix, err := netip.ParsePrefix(ipam.Status.IPv4Cidr)
	if err != nil {
		return "", "", err
	}

	// First IP address reserved for gateway
	// Second IP address used for source Pod
	// Third IP adress used for target Pod
	ipAddr1 := myprefix.Addr()
	ipAddr1 = ipAddr1.Next()
	ipAddr2 := ipAddr1.Next()

	return ipAddr1.String(), ipAddr2.String(), nil
}

// writeL2smAnnotations creates network annotations and labels for l2sm and writes them to the Pod
// update of the k8s Pod resource needs to be done outside
func writeL2smAnnotations(pod *corev1.Pod, vlink string, ip string, node string) error {

	var podAnnotationsNetwork, podAnnotationsNode = "", ""

	// TO DO: Remove this workaround for bug in ls2m network handling in NetMA
	// Workaround: The writing of the l2sm label is deferred until all network annotations for the Pod are written
	// The label is then written by the scheduler (in the Permit() plugin)
	// Alternative: Check here, whether annotations for all Channels of the Pod have been written (very complicated)
	// if pod.Labels == nil {
	//	pod.Labels = make(map[string]string)
	// }
	// pod.Labels["l2sm"] = "true"

	if pod.Annotations == nil {
		pod.Annotations = make(map[string]string)
	}

	// read existing annotations
	podAnnotationsNetwork, exists := pod.Annotations[L2SMAnnotationNetwork]
	podAnnotationsNode = pod.Annotations[L2SMAnnotationNode]

	var configs []*L2SMConfig

	// unmarshal existing JSON annotation string into L2SMConfig type
	if exists && podAnnotationsNetwork != "" {
		if err := json.Unmarshal([]byte(podAnnotationsNetwork), &configs); err != nil {
			log.Printf("Failed to unmarshal l2sm network annotation: %v", err)
			return err
		}
	} else {
		configs = make([]*L2SMConfig, 0)
	}

	updateRequired := true
	// Check if the annotation already exists
	for _, config := range configs {
		if config.Name == vlink {
			for _, existingIP := range config.Ips {
				if existingIP == ip {
					log.Printf("Annotation with vlink %s and ip %s already exists", vlink, ip)
					return nil
				}
			}
			// If the vlink exists but the IP is different, append the new IP to the existing vlink
			config.Ips = append(config.Ips, ip)
			updateRequired = false
		}
	}

	if updateRequired {
		// Create the new annotation if it doesn't already exist
		newConfig := &L2SMConfig{
			Name: vlink,
			Ips:  []string{ip},
		}

		// Append the new annotation
		configs = append(configs, newConfig)
	}

	// Marshal the slice back to JSON
	updatedAnnotationsNetwork, err := json.Marshal(configs)
	if err != nil {
		log.Printf("Failed to marshal updated l2sm network annotation: %v", err)
		return err
	}

	// Update the pod's network annotations with the new JSON string
	pod.Annotations[L2SMAnnotationNetwork] = string(updatedAnnotationsNetwork)

	// write the preferred node annotation
	if podAnnotationsNode == "" {
		pod.Annotations[L2SMAnnotationNode] = node
	}

	return nil
}
