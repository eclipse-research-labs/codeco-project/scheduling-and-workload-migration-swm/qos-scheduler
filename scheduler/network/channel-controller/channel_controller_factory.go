// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"fmt"
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// ChannelHandlerCreatorFunc returns a pair of implementations, one of a logical channel
// handler, the other of a physical channel handler.
// One of the implementations can be nil.
// The function should use the args map as well as the k8sClient, k8sScheme and logger arguments
// to configure the handlers it returns.
type ChannelHandlerCreatorFunc func(args map[string]string,
	k8sClient client.Client, k8sScheme *runtime.Scheme, logger logr.Logger) (
	LogicalChannelHandlerI, PhysicalChannelHandlerI)

// ChannelControllerFactory completes a channel controller
// based on a name. It will instantiate logical and/or
// physical channel handlers and insert them into a skeleton
// channel controller.
type ChannelControllerFactory struct {
	controllers map[string]ChannelHandlerCreatorFunc
}

// NewChannelControllerFactory creates and returns a ChannelControllerFactory.
// It might be nice to have a registration mechanism where you can add other
// handler implementations more easily.
func NewChannelControllerFactory() ChannelControllerFactory {
	return ChannelControllerFactory{
		controllers: map[string]ChannelHandlerCreatorFunc{
			"base":    baseCreatorFunc,
			"cnidemo": cniDemoCreatorFunc,
		},
	}
}

func baseCreatorFunc(args map[string]string, k8sClient client.Client, k8sScheme *runtime.Scheme, logger logr.Logger) (
	LogicalChannelHandlerI, PhysicalChannelHandlerI) {
	return &BaseLogicalChannelHandler{
			Client: k8sClient,
			Log:    logger,
			Scheme: k8sScheme,
		},
		&BasePhysicalChannelHandler{
			Client: k8sClient,
			Log:    logger,
			Scheme: k8sScheme,
		}
}

func cniDemoCreatorFunc(args map[string]string, k8sClient client.Client, k8sScheme *runtime.Scheme, logger logr.Logger) (
	LogicalChannelHandlerI, PhysicalChannelHandlerI) {
	var nwad string
	nwad, exists := args["network-attachment-name"]
	if !exists {
		nwad = ""
	}
	return &CniDemoLogicalChannelHandler{
			Client:                k8sClient,
			Log:                   logger,
			Scheme:                k8sScheme,
			NetworkAttachmentName: nwad,
		},
		&BasePhysicalChannelHandler{
			Client: k8sClient,
			Log:    logger,
			Scheme: k8sScheme,
		}
}

// CreateChannelHandlers creates a logical and/or physical channel handler.
// It uses the identifier to look up channel handler implementations and instantiates
// them, passing the provided args to their creation functions.
// It returns a tuple of logical handler, physical handler, and error.
// The logical or physical handler may be nil if there is no corresponding handler implementation.
func (f *ChannelControllerFactory) CreateChannelHandlers(identifier string, args map[string]string,
	k8sClient client.Client, k8sScheme *runtime.Scheme, logger logr.Logger) (
	LogicalChannelHandlerI, PhysicalChannelHandlerI, error) {
	creatorFunc, exists := f.controllers[identifier]
	if !exists {
		return nil, nil, fmt.Errorf("no channel handler creator function found for identifier %s", identifier)
	}
	logicalHandler, physicalHandler := creatorFunc(args, k8sClient, k8sScheme, logger)
	if logicalHandler == nil && physicalHandler == nil {
		return nil, nil, fmt.Errorf("creator function for identifier %s returned no handlers", identifier)
	}
	return logicalHandler, physicalHandler, nil
}

// AddPluginToLogicalChannelController creates a logical channel handler based on the
// identifier and attaches it to the logical channel reconciler.
func (f *ChannelControllerFactory) AddPluginToLogicalChannelController(identifier string, args map[string]string,
	controller *LogicalChannelReconciler) error {
	logicalHandler, _, err := f.CreateChannelHandlers(identifier, args, controller.Client, controller.Scheme, controller.Log)
	if err != nil {
		return err
	}
	if logicalHandler == nil {
		return fmt.Errorf("no logical channel handler found for identifier %s", identifier)
	}
	controller.Handler = logicalHandler
	return nil
}

// AddPluginToPhysicalChannelController creates a physical channel handler based on the
// identifier and attaches it to the physical channel reconciler.
func (f *ChannelControllerFactory) AddPluginToPhysicalChannelController(identifier string, args map[string]string,
	controller *PhysicalChannelReconciler) error {
	_, physicalHandler, err := f.CreateChannelHandlers(identifier, args, controller.Client, controller.Scheme, controller.Log)
	if err != nil {
		return err
	}
	if physicalHandler == nil {
		return fmt.Errorf("no physical channel handler found for identifier %s", identifier)
	}
	controller.Handler = physicalHandler
	return nil
}
