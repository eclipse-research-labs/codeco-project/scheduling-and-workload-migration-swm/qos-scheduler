// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	ctrl "sigs.k8s.io/controller-runtime"
)

// BasePhysicalChannelHandler is a sample implementation of a physical channel
// handler that implements all required methods but does nothing special.
type BasePhysicalChannelHandler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// HandleReviewedChannel determins whether the channel can be implemented on the
// chosen physical network.
func (b *BasePhysicalChannelHandler) HandleReviewedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	physicalNetworkImplementation := infrastructure.PhysicalChannelImplementation(&channel)
	potentialPaths, err := CanImplementChannel(ctx, b.Client, channel, string(physicalNetworkImplementation))
	if err != nil {
		return ctrl.Result{}, err
	}
	if len(potentialPaths) == 0 {
		channel.Status.Status = crd.ChannelRejected
		err = b.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	// There should be at most one path now if the logical channel handler has
	// recorded a preferred path on the channel, otherwise there might be several.
	// That is ok.
	channel.Status.Status = crd.ChannelImplementing
	err = b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementingChannel is just a stub implementation here. Normally, this is where
// you could trigger networking config changes as necessary, or maybe choose a vlan id
// to give to the channel.
func (b *BasePhysicalChannelHandler) HandleImplementingChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// TODO: actual implementation goes here
	// TODO: update the network link status by reducing available bandwidth and adding
	// this channel to the embedded channels
	channel.Status.Status = crd.ChannelImplemented
	err := b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleDeletedChannel is just a stub implementation here. Normally, this where you could
// perform cleanup of resources held for this channel such as vlan ids, and you could
// update bandwidth on links here to reflect that the channel is no longer using anything.
func (b *BasePhysicalChannelHandler) HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// TODO: free resources, clean up as necessary
	err := b.Delete(ctx, &channel)
	return ctrl.Result{}, err
}
