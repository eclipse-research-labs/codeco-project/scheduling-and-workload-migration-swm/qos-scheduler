// SPDX-FileCopyrightText: derived from L2S-M Project located at l2sm.io
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

/*
type NetworkType string

const (

	NetworkTypeExtVnet NetworkType = "ext-vnet"
	NetworkTypeVnet    NetworkType = "vnet"
	NetworkTypeVlink   NetworkType = "vlink"

)

type ConnectivityStatus string

const (

	OnlineStatus  ConnectivityStatus = "Available"
	OfflineStatus ConnectivityStatus = "Unavailable"
	UnknownStatus ConnectivityStatus = "Unknown"

)

// ProviderSpec defines the provider's name and domain. This is used in the inter-cluster scenario, to allow managing of the network in the external environment by this certified SDN provider.

	type ProviderSpec struct {
		Name   string `json:"name"`
		Domain string `json:"domain"`
	}

// L2NetworkSpec defines the desired state of L2Network
type L2NetworkSpec struct {

		// NetworkType represents the type of network being configured.
		Type NetworkType `json:"type"`

		// Config is an optional field that is meant to be used as additional configuration depending on the type of network. Check each type of network for specific configuration definitions.
		Config *string `json:"config,omitempty"`

		// Provider is an optional field representing a provider spec. Check the provider spec definition for more details
		Provider *ProviderSpec `json:"provider,omitempty"`
	}

// L2NetworkStatus defines the observed state of L2Network
type L2NetworkStatus struct {

		// Existing Pods in the cluster, connected to the specific network
		ConnectedPods []string `json:"connectedPods,omitempty"`

		// Status of the connectivity to the internal SDN Controller. If there is no connection, internal l2sm-switches won't forward traffic
		InternalConnectivity *ConnectivityStatus `json:"internalConnectivity"`

		// Status of the connectivity to the external provider SDN Controller. If there is no connectivity, the exisitng l2sm-ned in the cluster won't forward packages to the external clusters.
		ProviderConnectivity *ConnectivityStatus `json:"providerConnectivity,omitempty"`
	}

// L2Network is the Schema for the l2networks API

	type L2Network struct {
		metav1.TypeMeta   `json:",inline"`
		metav1.ObjectMeta `json:"metadata,omitempty"`

		Spec   L2NetworkSpec   `json:"spec,omitempty"`
		Status L2NetworkStatus `json:"status,omitempty"`
	}

// L2NetworkList contains a list of L2Network

	type L2NetworkList struct {
		metav1.TypeMeta `json:",inline"`
		metav1.ListMeta `json:"metadata,omitempty"`
		Items           []L2Network `json:"items"`
	}
*/
