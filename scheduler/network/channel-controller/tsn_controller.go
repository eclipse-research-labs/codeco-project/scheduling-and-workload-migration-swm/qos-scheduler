// SPDX-FileCopyrightText: 2024 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	monc "siemens.com/qos-scheduler/scheduler/api/v2"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"siemens.com/qos-scheduler/scheduler/network/cni"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// TsnReconciler is a logical channel controller for TSN network.
type TsnReconciler struct {
	ChannelReconciler
	NetworkAttachmentName string
	Namespace             string
	QoSPrio               int
}

// Reconcile implements the main reconcile loop of the controller.
func (t *TsnReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var channel crd.Channel
	err := t.Get(ctx, req.NamespacedName, &channel)
	if err != nil {
		if apierrors.IsNotFound(err) {
			t.Log.Info("Channel could not be found", "channel", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	networkImplementation := infrastructure.ChannelImplementation(&channel)

	// Are we interested in this channel?
	interested := false
	for _, ni := range t.NetworkImplementations {
		if networkImplementation == ni {
			interested = true
			break
		}
	}
	if !interested {
		return ctrl.Result{}, nil
	}
	// Is this channel in a channelStatus that we care about?
	channelStatus := channel.Status.Status

	if channelStatus == crd.ChannelRequested {
		// check whether path exists and has enough resources (may be redundant)
		// next state: ChannelReviewed or ChannelRejected
		return t.HandleRequestedChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelReviewed {
		// create IPAM CRs and check that they are ready
		// next state: ChannelImplementing or ChannelRejected
		return t.HandleReviewedChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelRejected {
		// This can have been rejected by this controller or by the physical
		// controller. It means the channel cannot be implemented with the available
		// capabilities. This basic implementation just cancels rejected channels, but
		// it would also be conceivable to retry them later.
		return t.HandleRejectedChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelImplementing {
		// create vlink CR (is handled by NetMA component)
		// all information available from Channel CR
		// next state: ChannelImplemented
		return t.HandleImplementingChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelImplemented {
		// check vlink status
		// if vlink belonging to our Channel is ready, promote state to ChannelAck
		// if vlink cannot be set up, promote state to ChannelCanceled
		// next state: ChannelAck or ChannelCanceled
		return t.HandleImplementedChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelAck {
		// create and write network annotations and muluts annotations to Pods
		// next state: ChannelRunning
		return t.HandleAcknowledgedChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelRunning {
		// watch for vlinks to disappear
		// next state: ChannelCanceled
		return t.HandleRunningChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelCanceled {
		// network side cancelled the Channel
		// release all resources and promote to ChannelDeleted
		return t.HandleCanceledChannel(ctx, channel)
	}

	if channelStatus == crd.ChannelDeleted {
		return t.HandleDeletedChannel(ctx, channel)
	}

	if channelStatus == "" {
		infrastructure.ResetChannelStatus(&channel)
		return ctrl.Result{}, t.Status().Update(ctx, &channel)
	}

	return ctrl.Result{}, nil
}

// HandleRequestedChannel verifies that there is enough capacity for the channel in the
// chosen network, and that a corresponding physical network exists.
// It records the physical network to use in a label on the channel and updates the
// channel status to include the path to use.
func (t *TsnReconciler) HandleRequestedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("requesting new channel", "channel", channel.Name, "namespace", channel.Namespace)
	networkImplementation := infrastructure.ChannelImplementation(&channel)
	potentialPaths, err := CanImplementChannel(ctx, t.Client, channel, string(networkImplementation))
	if len(potentialPaths) == 0 {
		if err != nil {
			t.Log.Info("channel cannot be implemented because ", "channel", channel.Name, "error", err)
		} else {
			t.Log.Info("channel cannot be implemented", "channel", channel.Name)
		}
		channel.Status.Status = crd.ChannelRejected
		err = t.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if err != nil {
		return ctrl.Result{}, err
	}

	physicalLabel := infrastructure.PhysicalChannelImplementation(&channel)
	physicalBase, pathIndex, err := GetPhysicalBaseForChannel(ctx, t.Client, channel, potentialPaths, string(networkImplementation), string(physicalLabel))
	if err != nil {
		t.Log.Info("channel cannot be implemented", "channel", channel.Name, "error", err)
		channel.Status.Status = crd.ChannelRejected
		err = t.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if string(physicalLabel) == "" {
		t.Log.Info("setting channel's physical label", "channel", channel.Name, "label", physicalBase)
		// The channel does not have a physical label yet.
		channel.Labels[crd.PhysicalNetworkLabel] = physicalBase
		err = t.Update(ctx, &channel)
		// Return here to avoid timing issues with the status update that follows.
		// This means the code section determining the channel will be executed twice.
		return ctrl.Result{}, err
	}

	channel.Status.CniMetadata.ChannelPath = crd.ChannelPathRef{
		Name:      potentialPaths[pathIndex].Name,
		Namespace: potentialPaths[pathIndex].Namespace,
	}

	channel.Status.Status = crd.ChannelReviewed
	err = t.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleReviewedChannel requests a CIDR range for the channel to annotate its pods.
func (t *TsnReconciler) HandleReviewedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("reviewing requested channel", "channel", channel.Name, "namespace", channel.Namespace)
	// Do we have the pod and node ipams for this channel yet?
	candidateIpams, err := GetIpamsForChannel(ctx, t.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	// Go through the list of existing ipams to see if any of them are for our pods
	// and nodes. We already know they are for the current channel because of the label
	// match in the list options above.
	podIndex, nodeIndex := t.getPodAndNodeIpam(candidateIpams.Items)
	var podIpam crd.Ipam
	var nodeIpam crd.Ipam
	haveToWaitForIpam := false
	if nodeIndex == -1 {
		haveToWaitForIpam = true
		nodeIpam = MakeIpam(channel, "nodes", 4)
		t.Log.Info("Create Node Ipam", "ipam", fmt.Sprintf("%s/%s", nodeIpam.Namespace, nodeIpam.Name))
		err = t.Create(ctx, &nodeIpam)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		nodeIpam = candidateIpams.Items[nodeIndex]
	}
	if podIndex == -1 {
		haveToWaitForIpam = true
		// Two pods, need a gateway address on each node and an address for each pod.
		podIpam = MakeIpam(channel, "pods", 4)
		t.Log.Info("Create Pod Ipam", "ipam", fmt.Sprintf("%s/%s", nodeIpam.Namespace, nodeIpam.Name))
		err = t.Create(ctx, &podIpam)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		podIpam = candidateIpams.Items[podIndex]
	}
	// We know both ipams exist, but are they done yet?
	if podIpam.Status.StatusCode == crd.IpamRequested {
		// Keep waiting
		haveToWaitForIpam = true
	} else if podIpam.Status.StatusCode == crd.IpamUnfulfillable {
		// Time to give up
		return ctrl.Result{Requeue: false}, fmt.Errorf("ipam request for pods for channel %s cannot be fulfilled: %s", channel.Name, podIpam.Status.Error)
	} else if podIpam.Status.StatusCode != crd.IpamReserved {
		t.Log.Info("Unexpected ipam status", "status", podIpam.Status.StatusCode)
		haveToWaitForIpam = true
	}
	if nodeIpam.Status.StatusCode == crd.IpamRequested {
		// Keep waiting
		haveToWaitForIpam = true
	} else if nodeIpam.Status.StatusCode == crd.IpamUnfulfillable {
		// Time to give up
		return ctrl.Result{Requeue: false}, fmt.Errorf("ipam request for nodes for channel %s cannot be fulfilled: %s", channel.Name, podIpam.Status.Error)
	} else if nodeIpam.Status.StatusCode != crd.IpamReserved {
		t.Log.Info("Unexpected ipam status", "status", nodeIpam.Status.StatusCode)
		haveToWaitForIpam = true
	}
	if haveToWaitForIpam {
		// This reconciler also watches ipams, so there will be another reconciler
		// call when the ipam we're waiting for is created or its status changes.
		t.Log.Info("ipams are not ready yet, waiting")
		return ctrl.Result{}, nil
	}
	// We have ipams for this channel, and they are all ready, go to next state (ChannelImplementing)
	t.Log.Info("node ip range and pod ip range obtained", "nodecidr", nodeIpam.Status.IPv4Cidr,
		"podcidr", podIpam.Status.IPv4Cidr)

	channel.Status.Status = crd.ChannelImplementing
	if err := t.Status().Update(ctx, &channel); err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, err
}

// HandleRejectedChannel updates the channel status to canceled.
func (t *TsnReconciler) HandleRejectedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("canceling rejected channel", "channel", channel.Name, "namespace", channel.Namespace)
	channel.Status.Status = crd.ChannelCanceled
	err := t.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementingChannel creates stream request, which is handled by MoNC component
func (t *TsnReconciler) HandleImplementingChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("implementing channel", "channel", channel.Name, "namespace", channel.Namespace)
	_, err := t.getStream(ctx, channel)
	if err != nil {
		if apierrors.IsNotFound(err) {
			t.Log.Info("No stream request found. Creating new one", "channel", channel.Name)
			newStream := t.MakeStream(channel, t.Namespace)
			t.Log.Info("Create Stream", "stream", fmt.Sprintf("%s/%s", newStream.Namespace, newStream.Name))
			// TODO: cross-namespace owner references are disallowed
			// // set the Channel as owner in k8s for the stream to create
			// if err := ctrl.SetControllerReference(&channel, &newStream, t.Scheme); err != nil {
			// 	t.Log.Error(err, "unable to make channel a controller of stream")
			// 	return ctrl.Result{}, err
			// }
			err = t.Create(ctx, &newStream)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, err
	}

	channel.Status.Status = crd.ChannelImplemented
	err = t.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementedChannel requests two CIDR ranges for the channel (one for the pods, and one
// for the nodes). It waits until the CIDR ranges have been allocated and then adds them to
// the channel's cni metadata, then it updates the channel's status to Ack.
func (t *TsnReconciler) HandleImplementedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("implemented channel", "channel", channel.Name, "namespace", channel.Namespace)
	// Do we have a stream request for this channel yet?
	stream, err := t.getStream(ctx, channel)
	if err != nil {
		return ctrl.Result{}, err
	}
	streamStatus := stream.Status.Status

	if streamStatus == monc.RequestedStatus {
		t.Log.Info("stream is still requested, waiting for it to be queued", "stream", stream.Name)
		return ctrl.Result{}, nil
	}

	if streamStatus == monc.QueuedStatus {
		t.Log.Info("stream is still queued, waiting for it to be computed", "stream", stream.Name)
		return ctrl.Result{}, nil
	}

	if streamStatus == monc.ComputedStatus {
		t.Log.Info("stream is computed, waiting for it to be embedded", "stream", stream.Name)
		return ctrl.Result{}, nil
	}

	if streamStatus == monc.EmbeddedStatus {
		t.Log.Info("stream is embedded, continue processing channel", "stream", stream.Name)
		// Get the pod and node ipams for this channel
		candidateIpams, err := GetIpamsForChannel(ctx, t.Client, channel)
		if err != nil {
			return ctrl.Result{}, err
		}
		// We already know that there are Ipams for this channel because we created them in state Reviewed.
		podIndex, nodeIndex := t.getPodAndNodeIpam(candidateIpams.Items)
		podIpam := candidateIpams.Items[podIndex]
		nodeIpam := candidateIpams.Items[nodeIndex]
		t.Log.Info("node ip range and pod ip range obtained", "nodecidr", nodeIpam.Status.IPv4Cidr, "podcidr", podIpam.Status.IPv4Cidr)
		// fill in cni metadata
		channel.Status.CniMetadata.IpRange = podIpam.Status.IPv4Cidr
		channel.Status.CniMetadata.NodeIpRange = nodeIpam.Status.IPv4Cidr
		if t.NetworkAttachmentName != "" {
			channel.Status.CniMetadata.NetworkAttachmentName = t.NetworkAttachmentName
			randId := rand.Intn(4094) + 1
			if channel.Status.CniMetadata.ContainerInterfaceId == 0 {
				channel.Status.CniMetadata.ContainerInterfaceId = randId
			}
			if channel.Status.CniMetadata.VlanId == 0 {
				channel.Status.CniMetadata.VlanId = stream.Spec.VlanId
			}
		}
		channel.Status.Status = crd.ChannelAck
		err = t.Status().Update(ctx, &channel)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	if streamStatus == monc.UnscheduledStatus || streamStatus == monc.UnembeddedStatus {
		t.Log.Info("stream is in failure state, rejecting channel request", "stream", stream.Name, "status", streamStatus)
		channel.Status.Status = crd.ChannelRejected
		return ctrl.Result{}, t.Status().Update(ctx, &channel)
	}
	return ctrl.Result{}, err
}

// HandleAcknowledgedChannel constructs network annotation and labels and writes them to the Pod
func (t *TsnReconciler) HandleAcknowledgedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("acknowledged channel", "channel", channel.Name, "namespace", channel.Namespace)
	// Does the channel request cni parameters?
	if channel.Status.CniMetadata.NetworkAttachmentName == "" {
		return ctrl.Result{}, fmt.Errorf("channel %s does not have cni parameters", channel.Name)
	}

	t.Log.Info("Channel has been allocated. Update Pod cni metadata.", "channel", channel.Name, "status", channel.Status.Status)
	sourcePod, targetPod, err := GetPodsForChannel(t.Client, ctx, &channel)
	if err != nil || sourcePod == nil || targetPod == nil {
		return ctrl.Result{}, err
	}
	sourceNode, targetNode, err := GetNetworkEndpointsForChannel(t.Client, ctx, &channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	var podAnnotations = ""
	if sourcePod.Annotations == nil {
		sourcePod.Annotations = make(map[string]string)
	} else {
		podAnnotations = sourcePod.Annotations[cni.NetworkAttachmentKey]
	}
	nwad, err := cni.InsertNetworkAttachmentDefinition(podAnnotations,
		channel.Status.CniMetadata.NetworkAttachmentName, sourceNode, targetNode, channel, true)
	if err != nil {
		return ctrl.Result{}, err
	}
	sourcePod.Annotations[cni.NetworkAttachmentKey] = nwad
	err = t.Update(ctx, sourcePod)
	if err != nil {
		return ctrl.Result{}, err
	}

	podAnnotations = ""
	if targetPod.Annotations == nil {
		targetPod.Annotations = make(map[string]string)
	} else {
		podAnnotations = targetPod.Annotations[cni.NetworkAttachmentKey]
	}
	nwad, err = cni.InsertNetworkAttachmentDefinition(podAnnotations,
		channel.Status.CniMetadata.NetworkAttachmentName, sourceNode, targetNode, channel, false)
	if err != nil {
		return ctrl.Result{}, err
	}
	targetPod.Annotations[cni.NetworkAttachmentKey] = nwad
	err = t.Update(ctx, targetPod)
	if err != nil {
		return ctrl.Result{}, err
	}
	infrastructure.StartChannelRunning(&channel)
	t.Log.Info("Update channel status", "channel", channel.Name, "status", channel.Status.Status)
	err = t.Status().Update(ctx, &channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	// The scheduler will check the channel status in Permit(). It looks up the channels
	// that a pod waits for in the optimizer assignment plan, and then it looks at the
	// channel status by using its channel crd client. The pod can only proceed if all channels
	// that it depends on have been allocated.
	return ctrl.Result{}, nil
}

// HandleRunningChannel checks stream status and reacts if they got cancelled
func (t *TsnReconciler) HandleRunningChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("running channel", "channel", channel.Name, "namespace", channel.Namespace)
	// Do we have a stream request for this channel?
	stream, err := t.getStream(ctx, channel)
	if err != nil {
		if apierrors.IsNotFound(err) {
			t.Log.Info("no stream request found. cancelling channel", "channel", channel.Name)
			channel.Status.Status = crd.ChannelRejected
			err = t.Status().Update(ctx, &channel)
		}
		return ctrl.Result{}, err
	}
	// Update channel performance metrics
	changed, err := t.updateEmbeddedChannelPerformanceData(&channel, stream)
	if err != nil {
		return ctrl.Result{}, err
	}
	if changed {
		return ctrl.Result{}, t.Update(ctx, &channel)
	}
	return ctrl.Result{}, nil
}

// HandleCanceledChannel updates the channel's status to Deleted.
func (t *TsnReconciler) HandleCanceledChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("canceling channel resources (ipams, streams)", "channel", channel.Name, "namespace", channel.Namespace)
	channel.Status.Status = crd.ChannelDeleted
	err := t.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleDeletedChannel deletes the Channel.
// It also deletes any ipams and streams that are associated with the channel
func (t *TsnReconciler) HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	t.Log.Info("deleting channel CR", "channel", channel.Name, "namespace", channel.Namespace)
	candidateIpams, err := GetIpamsForChannel(ctx, t.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}
	for _, ipam := range candidateIpams.Items {
		err = t.Delete(ctx, &ipam)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	stream, err := t.getStream(ctx, channel)
	if err != nil {
		if !apierrors.IsNotFound(err) {
			return ctrl.Result{}, err
		}
	} else {
		err = t.Delete(ctx, &stream)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	err = t.Delete(ctx, &channel)
	return ctrl.Result{}, err
}

func (t *TsnReconciler) getPodAndNodeIpam(ipams []crd.Ipam) (int, int) {
	var podIndex int = -1
	var nodeIndex int = -1
	// Go through the list of ipams to see if any of them are for our pods and nodes.
	for i, ipam := range ipams {
		if ipam.Spec.Purpose != "" {
			if podIndex < 0 && ipam.Spec.Purpose == "pods" {
				podIndex = i
			} else if nodeIndex < 0 && ipam.Spec.Purpose == "nodes" {
				nodeIndex = i
			}
			if podIndex >= 0 && nodeIndex >= 0 {
				return podIndex, nodeIndex
			}
		}
	}
	return podIndex, nodeIndex
}

func (t *TsnReconciler) getStream(ctx context.Context, channel crd.Channel) (monc.Stream, error) {
	var stream monc.Stream
	err := t.Get(ctx, types.NamespacedName{Name: fmt.Sprintf("%s-%s", channel.Namespace, channel.Name), Namespace: t.Namespace}, &stream)
	return stream, err
}

// MakeStream creates and returns an stream resource for a channel
// It does not write the resource to Kubernetes.
func (t *TsnReconciler) MakeStream(channel crd.Channel, namespace string) monc.Stream {
	return monc.Stream{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-%s", channel.Namespace, channel.Name),
			Namespace: namespace,
		},
		Spec: monc.StreamSpec{
			SrcNode:      channel.Spec.ChannelFrom,
			DstNodes:     []string{channel.Spec.ChannelTo},
			Interval:     channel.Spec.SendInterval,
			MaxFrameSize: channel.Spec.Framesize,
			MaxLatency:   channel.Spec.MaxDelayNanos,
			Creator:      "MoNC K8s NBI",
		},
		Status: monc.StreamStatus{
			Status: monc.RequestedStatus,
		},
	}
}

func (t *TsnReconciler) updateEmbeddedChannelPerformanceData(channel *crd.Channel, stream monc.Stream) (bool, error) {
	changed := false
	var err error = nil

	if channel.Status.CniMetadata.VlanId > 0 && channel.Status.CniMetadata.VlanId != stream.Spec.VlanId {
		err = errors.New("cannot change vlan id")
		t.Log.Error(err, "cannot change vlan id of existing stream", "stream", channel.ObjectMeta.Name, "from", channel.Status.CniMetadata.VlanId, "to", stream.Spec.VlanId)
	}

	if channel.Status.Latency != stream.Spec.MaxLatency && stream.Spec.MaxLatency != 0 {
		t.Log.Info("Updating channel", "latency", stream.Spec.MaxLatency)
		channel.Status.Latency = stream.Spec.MaxLatency
		changed = true
	}

	if channel.Status.FrameSize != stream.Spec.MaxFrameSize && stream.Spec.MaxFrameSize != 0 {
		t.Log.Info("Updating channel", "frameSize", stream.Spec.MaxFrameSize)
		channel.Status.FrameSize = stream.Spec.MaxFrameSize
		changed = true
	}

	if channel.Status.Interval != stream.Spec.Interval && stream.Spec.Interval != 0 {
		t.Log.Info("Updating channel", "interval", stream.Spec.Interval)
		channel.Status.Interval = stream.Spec.Interval
		changed = true
	}

	if channel.Status.EarliestTransmissionOffset != stream.Spec.EarliestTransmitOffset && stream.Spec.EarliestTransmitOffset != 0 {
		t.Log.Info("Updating channel", "earliestTransmissionOffset", stream.Spec.EarliestTransmitOffset)
		channel.Status.EarliestTransmissionOffset = stream.Spec.EarliestTransmitOffset
		changed = true
	}

	if channel.Status.LatestTransmissionOffset != stream.Spec.LatestTransmitOffset && stream.Spec.LatestTransmitOffset != 0 {
		t.Log.Info("Updating channel", "latestTransmissionOffset", stream.Spec.LatestTransmitOffset)
		channel.Status.LatestTransmissionOffset = stream.Spec.LatestTransmitOffset
		changed = true
	}

	return changed, err
}

func (t *TsnReconciler) setupStreamHandlerMapFunc() handler.MapFunc {
	return func(_ context.Context, strm client.Object) []reconcile.Request {
		stream, ok := strm.(*monc.Stream)
		if !ok {
			return []reconcile.Request{}
		}
		// take the stream name, split it at the first "-", take the first part as name and the second as namespace
		nameNamespace := strings.SplitN(stream.Name, "-", 2)
		namespace := nameNamespace[0]
		name := nameNamespace[1]
		rec := reconcile.Request{
			NamespacedName: types.NamespacedName{
				Name:      name,
				Namespace: namespace,
			},
		}
		t.Log.V(1).Info("Map Stream to Channel", "stream", fmt.Sprintf("%s/%s", stream.Namespace, stream.Name), "channel", fmt.Sprintf("%s/%s", rec.Namespace, rec.Name))
		return []reconcile.Request{rec}
	}
}

// SetupWithManager initializes the controller and makes it watch channels.
// It also watches links that embed one of its channels as well as
// ipams that are used by one of its channels.
// It also watches the tsn streamd that implement the Channel.
func (t *TsnReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.Channel{}).
		Watches(&monc.Stream{}, handler.EnqueueRequestsFromMapFunc(t.setupStreamHandlerMapFunc())).
		Watches(&crd.NetworkLink{}, handler.EnqueueRequestsFromMapFunc(t.setupLinkHandlerMapFunc())).
		Watches(&crd.Ipam{}, handler.EnqueueRequestsFromMapFunc(t.setupIpamHandlerMapFunc())).
		// Watches(&corev1.Pod{}, handler.EnqueueRequestsFromMapFunc(t.setupPodHandlerMapFunc())).
		Complete(t)
}
