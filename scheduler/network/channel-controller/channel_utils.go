// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	infrastructure "siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// GetNetworkLinks returns the links for the given network implementation.
func GetNetworkLinks(ctx context.Context, cl client.Client, networkImplementation string) ([]crd.NetworkLink, error) {
	var links crd.NetworkLinkList
	namespace := nwlib.NamespaceForNi(networkImplementation)
	err := cl.List(ctx, &links, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return links.Items, nil
}

// GetNetworkPaths returns the paths for the given network implementation.
func GetNetworkPaths(ctx context.Context, cl client.Client, networkImplementation string) ([]crd.NetworkPath, error) {
	var paths crd.NetworkPathList
	namespace := nwlib.NamespaceForNi(networkImplementation)
	err := cl.List(ctx, &paths, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return paths.Items, nil
}

// GetChannelPath returns the path specified for the channel as NetworkPath crd or nil if it doesn't exist
func GetChannelPath(ctx context.Context, cl client.Client, channel *crd.Channel) (*crd.NetworkPath, error) {
	var paths crd.NetworkPathList

	if channel.Spec.ChannelPath.Name != "" {
		namespace := nwlib.NamespaceForNi(string(infrastructure.ChannelImplementation(channel)))
		err := cl.List(ctx, &paths, client.InNamespace(namespace))
		if err != nil {
			return nil, err
		}
		for _, p := range paths.Items {
			if channel.Spec.ChannelPath.Name == p.Name && channel.Spec.ChannelPath.Namespace == p.Namespace {
				return &p, nil
			}
		}
	}
	return nil, nil
}

// CanImplementChannel returns a list of paths where the channel could be implemented.
// The channel can only be implemented on paths that match its spec and that
// have enough capacity.
// Currently, this check is redundant, since the workload placement solver has already chosen a matching path
func CanImplementChannel(ctx context.Context, cl client.Client, channel crd.Channel, networkImplementation string) ([]crd.NetworkPath, error) {

	ret := make([]crd.NetworkPath, 0)
	paths, err := GetNetworkPaths(ctx, cl, networkImplementation)
	if err != nil {
		return ret, err
	}
	links, err := GetNetworkLinks(ctx, cl, networkImplementation)
	if err != nil {
		return ret, err
	}

	possiblePaths := nwlib.PathsForChannel(channel.Spec, paths)
	if len(possiblePaths) == 0 {
		return ret, fmt.Errorf("channel %s (spec %v) requires a path that does not exist in network %s", channel.Name, channel.Spec, networkImplementation)
	}
	for _, p := range possiblePaths {
		channelFits := nwlib.SufficientNetworkCapacityOnPath(p, links, channel)
		if channelFits {
			ret = append(ret, p)
		}
		// Keep looking if channelFits is false since there might be several paths.
	}
	// ret may still be empty, that just means there is no path for this channel.
	if len(ret) == 0 {
		return ret, fmt.Errorf("channel %s requires a path for which there is no capacity, spec: %v", channel.Name, channel.Spec)
	}
	return ret, nil
}

// GetPhysicalBaseForChannel identifies the physical network that the channel
// will be using.
func GetPhysicalBaseForChannel(ctx context.Context, cl client.Client,
	channel crd.Channel, potentialPaths []crd.NetworkPath, logicalNetwork string,
	preferredPhysicalLabel string) (string, int, error) {
	if len(potentialPaths) < 1 {
		return "", -1, fmt.Errorf("channel %s has no potential paths", channel.Name)
	}

	// Choose a path. If preferredPhysicalLabel is not empty, keep looking for a matching
	// path.
	for index, chosenPath := range potentialPaths {
		// What is the physical base of the chosen path.
		firstLink := chosenPath.Spec.Links[0]
		var link crd.NetworkLink

		err := cl.Get(ctx, types.NamespacedName{Name: firstLink, Namespace: chosenPath.Namespace}, &link)
		if err != nil {
			return "", -1, err
		}
		var physicalBase string
		if link.Spec.PhysicalBase != "" {
			physicalBase = link.Spec.PhysicalBase
		} else {
			// The link is physical
			physicalBase, err = nwlib.NiFromNamespace(link.Namespace)
			if err != nil {
				return "", -1, err
			}
		}
		if preferredPhysicalLabel == "" || preferredPhysicalLabel == physicalBase {
			return physicalBase, index, nil
		}
	}
	return "", -1, fmt.Errorf("failed to find a physical base for channel %s", channel.Name)
}

// GetIpamsForChannel retrieves a list of Ipam crds for a given channel.
func GetIpamsForChannel(ctx context.Context, cl client.Client, channel crd.Channel) (crd.IpamList, error) {
	var candidateIpams crd.IpamList
	channelId := infrastructure.ChannelId(channel)
	ni := infrastructure.ChannelImplementation(&channel)
	namespace := nwlib.NamespaceForNi(string(ni))
	listopts := []client.ListOption{
		client.InNamespace(namespace),
		client.MatchingLabels{"channel": channelId},
	}
	err := cl.List(ctx, &candidateIpams, listopts...)
	return candidateIpams, err
}

// MakeIpam creates and returns an ipam resource for a channel
// It labels the ipam resource with the kv pair "channel": ChannelId (channel namespace and name)
// It does not write the resource to Kubernetes.
func MakeIpam(channel crd.Channel, purpose string, numberOfAddresses int) crd.Ipam {
	ni := infrastructure.ChannelImplementation(&channel)
	namespace := nwlib.NamespaceForNi(string(ni))
	return crd.Ipam{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-%s", channel.Name, purpose),
			Namespace: namespace,
			Labels: map[string]string{
				"channel": infrastructure.ChannelId(channel),
			},
		},
		Spec: crd.IpamSpec{
			NumberOfAddresses: numberOfAddresses,
			Purpose:           purpose,
		},
	}
}

func GetNetworkEndpointsForChannel(c client.Client, ctx context.Context, channel *crd.Channel) (crd.NetworkEndpoint, crd.NetworkEndpoint, error) {
	eps := &crd.NetworkEndpointList{}
	err := c.List(ctx, eps)
	if err != nil {
		return crd.NetworkEndpoint{}, crd.NetworkEndpoint{}, err
	}

	var sourceNode crd.NetworkEndpoint
	var targetNode crd.NetworkEndpoint

	for _, ep := range eps.Items {
		if ep.Name == channel.Spec.ChannelFrom {
			sourceNode = ep
		}
		if ep.Name == channel.Spec.ChannelTo {
			targetNode = ep
		}
	}
	return sourceNode, targetNode, nil
}

func GetPodsForChannel(c client.Client, ctx context.Context, channel *crd.Channel) (*corev1.Pod, *corev1.Pod, error) {
	sourceApplicationName := infrastructure.ChannelSourceApplication(channel)
	targetApplicationName := infrastructure.ChannelTargetApplication(channel)
	if sourceApplicationName == "" {
		return nil, nil, fmt.Errorf("missing source application for channel %v", channel)
	}
	if targetApplicationName == "" {
		return nil, nil, fmt.Errorf("missing target application for channel %v", channel)
	}
	pods := &corev1.PodList{}
	err := c.List(ctx, pods, client.InNamespace(channel.Namespace))
	if err != nil {
		return nil, nil, err
	}
	var sourcePod *corev1.Pod
	for _, pod := range pods.Items {
		if channel.Spec.SourceWorkload == pod.Name {
			sourcePod = &pod
			break
		}
	}
	var targetPod *corev1.Pod
	for _, pod := range pods.Items {
		if channel.Spec.TargetWorkload == pod.Name {
			targetPod = &pod
			break
		}
	}
	return sourcePod, targetPod, nil
}

func getPod(c client.Reader, ctx context.Context, namespace, podName string) (*corev1.Pod, error) {
	pod := &corev1.Pod{}

	// Define the key for the object
	key := client.ObjectKey{
		Namespace: namespace,
		Name:      podName,
	}

	// Get the pod object
	if err := c.Get(ctx, key, pod); err != nil {
		return nil, err
	}

	return pod, nil
}
