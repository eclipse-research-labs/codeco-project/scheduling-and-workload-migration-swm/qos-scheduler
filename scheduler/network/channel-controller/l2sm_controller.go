// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"fmt"
	"time"

	// corev1 "k8s.io/api/core/v1"
	// "k8s.io/apimachinery/pkg/api/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	l2sm "siemens.com/qos-scheduler/scheduler/api/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/handler"
)

// L2SMReconciler is a logical channel controller for L2S-M network.
type L2SMReconciler struct {
	ChannelReconciler
}

const (
	L2SMAnnotationNode    = "l2sm/preferred-node"
	L2SMAnnotationNetwork = "l2sm/networks"
)

// L2SMConfig is the network annotation required for the Pods at both ends of an l2sm channel
type L2SMConfig struct {
	Name string   `json:"name"`
	Ips  []string `json:"ips"`
}

// Reconcile implements the main reconcile loop of the controller.
func (l *L2SMReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var channel crd.Channel
	err := l.Get(ctx, req.NamespacedName, &channel)
	if err != nil {
		if apierrors.IsNotFound(err) {
			l.Log.Info("Channel could not be found", "channel", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	networkImplementation := infrastructure.ChannelImplementation(&channel)

	// Are we interested in this channel?
	interested := false
	for _, ni := range l.NetworkImplementations {
		if networkImplementation == ni {
			interested = true
			break
		}
	}
	if !interested {
		return ctrl.Result{}, nil
	}

	// Is this channel in a status that we care about?
	status := channel.Status.Status

	l.Log.Info("encountered channel", "channel", channel.Name, "status", status)
	if status == crd.ChannelRequested {
		// check whether path exists and has enough resources (may be redundant)
		// next state: ChannelReviewed or ChannelRejected
		return l.HandleRequestedChannel(ctx, channel)
	}

	if status == crd.ChannelReviewed {
		// create IPAM CRs and check that they are ready
		// next state: ChannelImplementing or ChannelRejected
		return l.HandleReviewedChannel(ctx, channel)
	}

	if status == crd.ChannelRejected {
		// This can have been rejected by this controller or by the physical
		// controller. It means the channel cannot be implemented with the available
		// capabilities. This basic implementation just cancels rejected channels, but
		// it would also be conceivable to retry them later.
		return l.HandleRejectedChannel(ctx, channel)
	}

	if status == crd.ChannelImplementing {
		// create vlink CR (is handled by NetMA component)
		// all information available from Channel CR
		// next state: ChannelImplemented
		return l.HandleImplementingChannel(ctx, channel)
	}

	if status == crd.ChannelImplemented {
		// check vlink status
		// if vlink belonging to our Channel is ready, promote state to ChannelAck
		// if vlink cannot be set up, promote state to ChannelCanceled
		// next state: ChannelAck or ChannelCanceled
		return l.HandleImplementedChannel(ctx, channel)
	}

	if status == crd.ChannelAck {
		// create and write network annotations and l2sm label to Pods
		// => look at scheduler/network/cni/cni.go
		// PodAnnotations (source and target Pods):
		// - vlink name
		// - IP address
		// - label: l2sm: true
		// next state: ChannelRunning
		return l.HandleAcknowledgedChannel(ctx, channel)
	}

	if status == crd.ChannelRunning {
		// watch for vlinks to disappear
		// next state: ChannelCanceled
		return l.HandleRunningChannel(ctx, channel)
	}

	if status == crd.ChannelCanceled {
		// network side cancelled the Channel
		// release all resources and promote to ChannelDeleted
		return l.HandleCanceledChannel(ctx, channel)
	}

	if status == crd.ChannelDeleted {
		return l.HandleDeletedChannel(ctx, channel)
	}

	if status == "" {
		infrastructure.ResetChannelStatus(&channel)
		return ctrl.Result{}, l.Status().Update(ctx, &channel)
	}

	return ctrl.Result{}, nil
}

// HandleRequestedChannel verifies that path exists that accommodates the channel
// and that there is enough capacity for the channel in the chosen network.
// It records the physical network to use in a label on the channel (???) and updates the
// channel status to include the path to use.
func (l *L2SMReconciler) HandleRequestedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {

	/*
		networkImplementation := infrastructure.ChannelImplementation(&channel)
		potentialPaths, err := CanImplementChannel(ctx, l.Client, channel, string(networkImplementation))
		if len(potentialPaths) == 0 {
			if err != nil {
				l.Log.Info("channel cannot be implemented because ", "channel", channel.Name, "error", err)
			} else {
				l.Log.Info("channel cannot be implemented", "channel", channel.Name)
			}
			channel.Status.Status = crd.ChannelRejected
			err = l.Status().Update(ctx, &channel)
			return ctrl.Result{}, err
		}
		if err != nil {
			return ctrl.Result{}, err
		}
	*/

	channel.Status.Status = crd.ChannelReviewed
	err := l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleReviewedChannel requests a CIDR range for the channel to annotate its pods.
func (l *L2SMReconciler) HandleReviewedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// Do we have the pod and node ipams for this channel yet?
	ipams, err := GetIpamsForChannel(ctx, l.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	if len(ipams.Items) == 0 {
		podIpam := MakeIpam(channel, "pods", 4)
		l.Log.Info("Create Pod Ipam", "ipams", fmt.Sprintf("%s/%s", podIpam.Namespace, podIpam.Name))
		err = l.Create(ctx, &podIpam)
		return ctrl.Result{}, err
	}

	// We have ipams for this channel, check whether they are ready
	// wait for ipams creation (=> cnidemo_logical_handler.go) to avoid race condition
	// wait for ipams creation (=> cnidemo_logical_handler.go) to avoid race conditionhannel has been deleted
	for _, ip := range ipams.Items {
		switch ip.Status.StatusCode {
		case "REQUESTED":
			// this ipam is still not ready, we need to still wait
			return ctrl.Result{}, err
		case "RESERVED":
			// this ipam is ready, continue with next
			continue
		case "UNFULFILLABLE":
			{
				// this ipam cannot be fulfilled, we need to cancel the channel
				l.Log.Error(err, fmt.Sprintf("Could not create Ipam %s/%s", ip.Namespace, ip.Name))
				channel.Status.Status = crd.ChannelCanceled
				err = l.Status().Update(ctx, &channel)
				return ctrl.Result{}, err
			}
		}
	}

	// We have ipams for this channel, and they are all ready, go to next state (ChannelImplementing)
	l.Log.Info("All ipams for the Channel have been allocated and are ready", "channel", channel.Name, "status", channel.Status.Status)

	channel.Status.Status = crd.ChannelImplementing
	err = l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleRejectedChannel updates the channel status to canceled.
func (l *L2SMReconciler) HandleRejectedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	l.Log.Info("canceling rejected channel", "channel", channel.Name, "namespace", channel.Namespace)
	channel.Status.Status = crd.ChannelCanceled
	err := l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementingChannel creates vlink CR, which is handled by NetMA component
func (l *L2SMReconciler) HandleImplementingChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	/*	{
		"overlay-parameters": {
		"path": {
			"name": "app1-w2-app1-w1",			=> use the channel name as path name: channel.metadata.name
			"FromEndpoint": "netma-test-3",		=> channel.Spec.ChannelFrom
			"ToEndpoint": "netma-test-2",		=> channel.Spec.ChannelTo
			"links": ["25117dc4"],				=> get from NetworkPath: Path=channel.Spec.ChannelPath; Links[]=Path.Spec.Links
			"capabilities": {
			"bandwidthBits": "1000000",			=> channel.Spec.MinBandwidthBits
			"latencyNanos": "10e-3"				=> channel.Spec.MaxDelayNanos / 1E9
			}
		}
	} */
	var vlink l2sm.L2Network
	var path *crd.NetworkPath
	var err error

	if path, err = GetChannelPath(ctx, l.Client, &channel); err != nil {
		l.Log.Error(err, "Channel does not have path assigned, unable to create vlink CR")
		// TO DO: channel gets stuck in this state and retries (may be infinite loop)
		return ctrl.Result{}, err
	}

	vlink = MakeVlink(channel, path)
	l.Log.Info("Create Vlink", "vlink", fmt.Sprintf("%s/%s", vlink.Namespace, vlink.Name))
	// set the Channel as owner in k8s for the vlink to create
	if err := ctrl.SetControllerReference(&channel, &vlink, l.Scheme); err != nil {
		l.Log.Error(err, "unable to make channel a controller of vlink")
		// TO DO: channel gets stuck in this state and retries (may be infinite loop)
		return ctrl.Result{}, err
	}
	if err := l.Create(ctx, &vlink); err != nil {
		l.Log.Error(err, "unable to create vlink CR")
		// TO DO: channel gets stuck in this state and retries (may be infinite loop)
		return ctrl.Result{}, err
	}

	channel.Status.CniMetadata.VlinkName = vlink.Name
	channel.Status.Status = crd.ChannelImplemented
	err = l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementedChannel checks vlink status
func (l *L2SMReconciler) HandleImplementedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// TO DO: check vlink status
	// if vlink belonging to our Channel is ready, promote state to ChannelAck
	// if vlink belonging to our Channel failed, promote state to Channel???
	channel.Status.Status = crd.ChannelAck
	err := l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleAcknowledgedChannel constructs network annotation and labels and writes them to the Pod
// Pod resource update
// l2sm network annotations for source and target Pod
func (l *L2SMReconciler) HandleAcknowledgedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {

	// get the ipam(s) for the Channel
	ipams, err := GetIpamsForChannel(ctx, l.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	// get ip addresses for source and target Pod
	sourceIpCidr, targetIpCidr, err := GetPodIpv4Cidr(ipams.Items[0])
	if err != nil {
		return ctrl.Result{}, err
	}

	// get ip addresses for source and target Pod and write them to the channel CR
	sourceIp, targetIp, err := GetPodIpv4(ipams.Items[0])
	if err != nil {
		return ctrl.Result{}, err
	}
	channel.Status.CniMetadata.SourceIp = sourceIp
	channel.Status.CniMetadata.TargetIp = targetIp

	// Create and write network annotations and label to sourcePod
	// TO DO: We use the ChannelId as vlink name for now. This is a dependency to the place where we generate the vlink name.
	for {
		// get source Pod for the Channel
		sourcePod, err := getPod(l.Client, ctx, channel.Namespace, channel.Spec.SourceWorkload)
		if err != nil || sourcePod == nil {
			return ctrl.Result{}, err
		}

		if err := writeL2smAnnotations(sourcePod, infrastructure.ChannelId(channel), sourceIpCidr, channel.Spec.ChannelFrom); err != nil {
			return ctrl.Result{}, err
		}

		if err := l.Update(ctx, sourcePod); err != nil {
			if apierrors.IsConflict(err) {
				l.Log.Error(err, "Conflict error writing k8s resource for sourcepod, retrying...")
				time.Sleep(1 * time.Second) // Sleep for a while before retrying
				continue
			}
			l.Log.Error(err, "Failed to update pod")
			return ctrl.Result{}, err
		}

		// Successfully updated the Pod
		l.Log.Info("Updated source pod annotations.")
		break
	}

	// Create and write network annotations and label to targetPod
	for {
		// get target Pod for the Channel
		targetPod, err := getPod(l.Client, ctx, channel.Namespace, channel.Spec.TargetWorkload)
		if err != nil || targetPod == nil {
			return ctrl.Result{}, err
		}

		if err := writeL2smAnnotations(targetPod, infrastructure.ChannelId(channel), targetIpCidr, channel.Spec.ChannelTo); err != nil {
			return ctrl.Result{}, err
		}

		if err := l.Update(ctx, targetPod); err != nil {
			if apierrors.IsConflict(err) {
				l.Log.Error(err, "Conflict error writing k8s resource for targetpod, retrying...")
				time.Sleep(1 * time.Second) // Sleep for a while before retrying
				continue
			}
			l.Log.Error(err, "Failed to update pod")
			return ctrl.Result{}, err
		}

		// Successfully updated the Pod
		l.Log.Info("Updated target pod annotations")
		break
	}

	// Pod annotations for the channel have been written
	l.Log.Info("L2SM network annotations and labels for the Channel have been written to the source and target Pods.", "channel", channel.Name, "status", channel.Status.Status)

	// set channel.state to ChannelRunning
	// this triggers scheduling of the Pods (done by QoS scheduler)
	channel.Status.Status = crd.ChannelRunning
	err = l.Status().Update(ctx, &channel)
	// The scheduler will check the channel status in Permit(). It looks up the channels
	// that a pod waits for in the optimizer assignment plan, and then it looks at the
	// channel status by using its channel crd client. The pod can only proceed if all channels
	// that it depends on have been allocated.
	return ctrl.Result{}, err
}

// HandleRunningChannel checks vlink status and reacts if they got cancelled
func (l *L2SMReconciler) HandleRunningChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// TO DO: check vlinks for cancellation, if that happens promote state to ChannelCanceled
	/* if vlink.status == cancelled {
		channel.Status.Status = crd.ChannelCanceled
		err := l.Status().Update(ctx, &channel)
	}
	return ctrl.Result{}, err */
	return ctrl.Result{}, nil
}

// HandleCanceledChannel updates the channel's status to Deleted.
// It also deletes any ipams that are associated with the channel so their network cidrs
// can be released.
func (l *L2SMReconciler) HandleCanceledChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	l.Log.Info("deleting channel resources (ipams, vlinks)", "channel", channel.Name, "namespace", channel.Namespace)
	candidateIpams, err := GetIpamsForChannel(ctx, l.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}
	for _, ipam := range candidateIpams.Items {
		err = l.Delete(ctx, &ipam)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	channel.Status.Status = crd.ChannelDeleted
	err = l.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleDeletedChannel deletes the Channel CR.
func (l *L2SMReconciler) HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	l.Log.Info("deleting channel CR", "channel", channel.Name, "namespace", channel.Namespace)
	err := l.Delete(ctx, &channel)
	return ctrl.Result{}, err
}

// SetupWithManager initializes the controller and makes it watch channels.
// It also watches links that embed one of its channels as well as
// ipams that are used by one of its channels.
// It also watches the vlinks (l2sm) that implement the Channel.
func (l *L2SMReconciler) SetupWithManager(mgr ctrl.Manager) error {
	// TO DO: add watch for vlink crd (with appropriate filter). Only required if we check for vlink status.
	return ctrl.NewControllerManagedBy(mgr).For(&crd.Channel{}).
		Watches(&crd.NetworkLink{}, handler.EnqueueRequestsFromMapFunc(l.setupLinkHandlerMapFunc())).
		Watches(&crd.Ipam{}, handler.EnqueueRequestsFromMapFunc(l.setupIpamHandlerMapFunc())).
		//  disable Pod watches, probably not used/required currently
		//	Watches(&corev1.Pod{}, handler.EnqueueRequestsFromMapFunc(l.setupPodHandlerMapFunc())).
		Complete(l)
}
