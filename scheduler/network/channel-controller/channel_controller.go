// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"siemens.com/qos-scheduler/scheduler/application"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// ChannelReconciler is a base class for channel processing.
// This is not a full Reconciler because it does not implement
// the Reconcile() function.
type ChannelReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	// The list of network implementations that this controller
	// will process channels for.
	NetworkImplementations []nwlib.NetworkImplementationClass
}

func (c *ChannelReconciler) setupLinkHandlerMapFunc() handler.MapFunc {
	return func(_ context.Context, lnk client.Object) []reconcile.Request {
		link, ok := lnk.(*crd.NetworkLink)
		if !ok {
			return []reconcile.Request{}
		}
		// Get channels embedded on link
		embeddedChannels := link.Status.EmbeddedChannels
		ret := make([]reconcile.Request, len(embeddedChannels))
		for i, ec := range embeddedChannels {
			channelName, channelNamespace := infrastructure.ParseChannelId(ec)
			ret[i] = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      channelName,
					Namespace: channelNamespace,
				},
			}
			c.Log.V(1).Info("Map NetworkLink to Channel", "networklink", fmt.Sprintf("%s/%s", link.GetNamespace(), link.GetName()), "channel", fmt.Sprintf("%s/%s", ret[i].Namespace, ret[i].Name))
		}
		return ret
	}
}

func (c *ChannelReconciler) setupIpamHandlerMapFunc() handler.MapFunc {
	return func(_ context.Context, ipm client.Object) []reconcile.Request {
		// Get labels on object
		label, exists := ipm.GetLabels()["channel"]
		if exists {
			channelName, channelNamespace := infrastructure.ParseChannelId(label)
			rec := reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      channelName,
					Namespace: channelNamespace,
				},
			}
			c.Log.V(1).Info("Map Ipam to Channel", "ipam", fmt.Sprintf("%s/%s", ipm.GetNamespace(), ipm.GetName()), "channel", fmt.Sprintf("%s/%s", rec.Namespace, rec.Name))
			return []reconcile.Request{rec}
		}
		return []reconcile.Request{}
	}
}

func (c *ChannelReconciler) setupPodHandlerMapFunc() handler.MapFunc {
	return func(_ context.Context, p client.Object) []reconcile.Request {
		req := make([]reconcile.Request, 0)
		// if pod is part of an application group
		if ag, exists := p.GetLabels()["application-group"]; exists {
			// find all applications belonging to same app group
			apps := &crd.ApplicationList{}
			listOptions := []client.ListOption{
				client.InNamespace(p.GetNamespace()),
				client.MatchingLabels{"application-group": ag},
			}
			err := c.List(context.Background(), apps, listOptions...)
			if err != nil {
				c.Log.Error(err, "failure looking for applications for application group", "app-group", ag)
				return req
			}
			// iterate over apps and translate to channels
			for _, app := range apps.Items {
				for _, wl := range app.Spec.Workloads {
					for _, ch := range wl.Channels {
						sourceWlName := application.GetWorkloadName(app.Name, wl.Basename)
						targetWlName := application.GetWorkloadName(ch.OtherWorkload.ApplicationName, ch.OtherWorkload.Basename)
						name := application.GetChannelName(ch.Basename, sourceWlName, targetWlName)
						req = append(req, reconcile.Request{
							NamespacedName: types.NamespacedName{
								Name:      name,
								Namespace: app.Namespace,
							},
						})
					}
				}
			}
		}
		return req
	}
}
