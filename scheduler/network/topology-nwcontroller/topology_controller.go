// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package topologynw contains code for a network operator that obtains
// its topology from a NetworkTopology CRD.
package topologynw

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// TopologyReconciler watches a topology spec and creates networklinks from it.
type TopologyReconciler struct {
	base.BaseNetworkReconciler
}

// Reconcile is the main Reconcile loop of the controller.
// This loop gets called whenever a NetworkTopology resource is created, updated,
// or deleted. It updates NetworkLinks and NetworkPaths accordingly.
func (t *TopologyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var top crd.NetworkTopology

	// Check if the topology is in the same namespace as the controller
	if t.Namespace != req.NamespacedName.Namespace {
		return ctrl.Result{}, nil
	}

	err := t.Get(ctx, req.NamespacedName, &top)
	if err != nil && errors.IsNotFound(err) {
		// topology cannot be found
		// TODO: decide what to do here, remove the corresponding links and paths?
		return ctrl.Result{}, nil
	} else if err != nil {
		return ctrl.Result{}, err
	}

	// validation
	if top.Spec.NetworkImplementation == "" {
		return ctrl.Result{}, t.updateStatusRejected(ctx, "topology spec missing network implementation string", req.NamespacedName.String(), &top)
	}
	existingNetworkEndpoints, err := t.GetExistingNetworkEndpoints(ctx)
	if err != nil {
		return ctrl.Result{}, t.updateStatusRejected(ctx, "failure to retrieve network endpoints", req.NamespacedName.String(), &top)
	}
	if len(existingNetworkEndpoints) == 0 {
		return ctrl.Result{}, t.updateStatusRejected(ctx, "there are no network endpoints, but they are needed for processing a topology", req.NamespacedName.String(), &top)

	}
	err = nwlib.ValidateNetworkTopology(top.Spec, existingNetworkEndpoints)
	if err != nil {
		return ctrl.Result{}, t.updateStatusRejected(ctx, err.Error(), req.NamespacedName.String(), &top)
	}

	// Create or update network links
	newLinks := nwlib.NetworkLinksFromTopologySpec(top.Spec, top.Spec.NetworkImplementation)
	err = t.CrudLinks(ctx, newLinks, top.Spec.NetworkImplementation)
	if err != nil {
		return ctrl.Result{}, t.updateStatusRejected(ctx, "failure during creation of network links", req.NamespacedName.String(), &top)
	}

	// Topology for physical network
	if top.Spec.PhysicalBase == "" {
		newNodes := top.Spec.Nodes
		updates, err := nwlib.FindNetworkEndpointsToPatch(existingNetworkEndpoints, newNodes, top.Spec.NetworkImplementation)
		if err != nil {
			return ctrl.Result{}, t.updateStatusRejected(ctx, "failure while trying to find network endpoints to patch", req.NamespacedName.String(), &top)
		}
		err = t.UpdateNetworkEndpoints(ctx, updates)
		if err != nil {
			return ctrl.Result{}, t.updateStatusRejected(ctx, "failure during update of network endpoints", req.NamespacedName.String(), &top)
		}
		// Topology for logical network
	} else {
		var physicalLinks []crd.NetworkLink
		physicalLinks, err = t.GetExistingLinks(ctx, top.Spec.PhysicalBase)
		if err != nil {
			return ctrl.Result{}, t.updateStatusRejected(ctx, "failure when retrieving existing physical links", req.NamespacedName.String(), &top)
		}
		if len(physicalLinks) == 0 {
			return ctrl.Result{}, t.updateStatusRejected(ctx, fmt.Sprintf("the topology %s uses the physical network %s, but there are no links available", t.Namespace, top.Spec.PhysicalBase), req.NamespacedName.String(), &top)
		}
		// check whether logical links have matching physical links with sufficient capabilities
		for _, link := range newLinks {
			match := false
			for _, plink := range physicalLinks {
				if link.Spec.LinkFrom.Name == plink.Spec.LinkFrom.Name && link.Spec.LinkTo.Name == plink.Spec.LinkTo.Name {
					if nwlib.CapabilitiesCanBeSatisfied(link, plink) {
						match = true
						break
					} else {
						msg := fmt.Sprintf("the topology %s declares a logical link %s that has capabilities that cannot be satisfied by the physical base", t.Namespace, link.Name)
						return ctrl.Result{}, t.updateStatusRejected(ctx, msg, req.NamespacedName.String(), &top)
					}
				}
			}
			if !match {
				msg := fmt.Sprintf("the topology %s declares a logical link %s for which no physical base exists", t.Namespace, link.Name)
				return ctrl.Result{}, t.updateStatusRejected(ctx, msg, req.NamespacedName.String(), &top)
			}
		}
	}

	// Create or update network paths
	err = t.ComputePaths(ctx, top.Spec.NetworkImplementation)
	if err != nil {
		return ctrl.Result{}, t.updateStatusRejected(ctx, "failure during network path creation", req.NamespacedName.String(), &top)
	} else {
		t.Log.Info("topology implemented successfully", "topology", req.NamespacedName.String())
		top.Status = crd.TopologyStatus{Status: crd.TopologyImplemented}
		err := t.Client.Status().Update(ctx, &top)
		if err != nil {
			t.Log.Error(err, "error during topology status update", "topology", req.NamespacedName.String())
		}
		t.Recorder.Event(&top, "Normal", "topology implemented", fmt.Sprintf("topology %s implemented successfully", req.NamespacedName.String()))
	}
	return ctrl.Result{}, nil
}
func (t *TopologyReconciler) updateStatusRejected(ctx context.Context, msg string, ns string, top *crd.NetworkTopology) error {
	top.Status = crd.TopologyStatus{Status: crd.TopologyRejected}
	err := t.Client.Status().Update(ctx, top)
	if err != nil {
		t.Log.Error(err, "error during top status update", "topology", ns)
	}
	t.Recorder.Event(top, "Warning", "topology rejected", msg)
	return fmt.Errorf(msg)
}

// SetupWithManager initializes the controller and make it watch network topology resources.
func (t *TopologyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.NetworkTopology{}).Complete(t)
}
