// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/path"
	"gonum.org/v1/gonum/graph/simple"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// BuildNetworkPaths creates NetworkPaths from an array of NetworkLinks,
// currently uses Dijkstra algorithm for shortest paths between each two nodes.
// Uses go graph packages.
// Input: array of NetworkLinks (CRDs); Output: array of NetworkPaths (CRDs)
// Note: NetworkPaths are defined as a concatenation (ordered list) of NetworkLinks
func BuildNetworkPaths(networklinks []crd.NetworkLink) []crd.NetworkPathSpec {

	if len(networklinks) == 0 {
		return []crd.NetworkPathSpec{}
	}

	// map to find NetworkLink for each endpoint pair
	endpointsToLink := make(map[string]crd.NetworkLink)

	// translate network links to graph
	nodeNameToIndex := make(map[string]int)
	nodeIndexToNode := make(map[int]crd.LinkNode)
	simpleGraph := simple.NewUndirectedGraph()
	var graphNodes []graph.Node
	addGraphNode := func() int {
		graphNode := simpleGraph.NewNode()
		simpleGraph.AddNode(graphNode)
		graphNodes = append(graphNodes, graphNode)
		return len(graphNodes) - 1
	}
	addLinkNode := func(linkNode crd.LinkNode) string {
		if _, ok := nodeNameToIndex[linkNode.Name]; !ok {
			index := addGraphNode()
			nodeNameToIndex[linkNode.Name] = index
			nodeIndexToNode[index] = linkNode
		}
		return linkNode.Name
	}
	for _, link := range networklinks {
		fromNodeName := addLinkNode(link.Spec.LinkFrom)
		toNodeName := addLinkNode(link.Spec.LinkTo)
		endpointsToLink[fromNodeName+"/"+toNodeName] = link
		if fromNodeName != toNodeName {
			fromNode := graphNodes[nodeNameToIndex[fromNodeName]]
			toNode := graphNodes[nodeNameToIndex[toNodeName]]
			simpleGraph.SetEdge(simpleGraph.NewEdge(fromNode, toNode))
		}
	}

	// iterate over nodes twice to get all shortest paths between nodes
	var nps []crd.NetworkPathSpec
	for i, gni := range graphNodes {
		for o, gno := range graphNodes {
			fromNode := nodeIndexToNode[i]
			toNode := nodeIndexToNode[o]
			// no need to generate loopback paths (it is done later) or to network nodes
			if fromNode == toNode || fromNode.Type == crd.NETWORK_NODE || toNode.Type == crd.NETWORK_NODE {
				continue
			}
			shortest := path.DijkstraFrom(gni, simpleGraph)
			to, _ := shortest.To(gno.ID())
			// translate path to network path spec
			np := crd.NetworkPathSpec{
				Start: fromNode,
				End:   toNode,
				Links: []string{},
			}
			// create links for loopback links
			if len(to) == 1 {
				n := to[0]
				currentNode := nodeIndexToNode[int(n.ID())].Name
				// TODO (HM): use link names from CRD
				// np.Links = append(np.Links, currentNode+"-"+currentNode)
				if link, ok := endpointsToLink[currentNode+"/"+currentNode]; ok {
					np.Links = append(np.Links, link.ObjectMeta.Name)
				}
			}
			// create links for paths with multiple hops
			for l := 0; l < len(to)-1; l++ {
				n := to[l]
				currentNode := nodeIndexToNode[int(n.ID())].Name
				nextNode := nodeIndexToNode[int(to[l+1].ID())].Name
				// TODO (HM): use link names from CRD
				// np.Links = append(np.Links, currentNode+"-"+nextNode)
				if link, ok := endpointsToLink[currentNode+"/"+nextNode]; ok {
					np.Links = append(np.Links, link.ObjectMeta.Name)
				}
			}
			nps = append(nps, np)
		}
	}

	// add loopback paths
	// loopback paths are only created for nodes that have a loopback link specifically set
	// TODO (HM): Is this redundant? Loopback paths should already be set be lines 74ff
	for _, link := range networklinks {
		if link.Spec.LinkFrom.Name == link.Spec.LinkTo.Name {
			np := crd.NetworkPathSpec{
				Start: link.Spec.LinkFrom,
				End:   link.Spec.LinkTo,
				// Links: []string{link.Spec.LinkFrom.Name + "-" + link.Spec.LinkTo.Name},
				Links: []string{link.ObjectMeta.Name},
			}
			nps = append(nps, np)
		}
	}
	return nps
}
