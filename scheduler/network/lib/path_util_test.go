// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestBuildNetworkPaths(t *testing.T) {

	// starting with two links
	links := []crd.NetworkLink{
		MakeNetworkLink("a", "COMPUTE", "b", "COMPUTE", 10000, 1000),
		MakeNetworkLink("b", "COMPUTE", "a", "COMPUTE", 20000, 2000),
	}
	paths := BuildNetworkPaths(links)
	if len(paths) != 2 {
		t.Errorf("expected two paths but got %+v", len(paths))
	}

	// adding a network links should not have any effect
	links = append(links, MakeNetworkLink("a", "COMPUTE", "c", "NETWORK", 100, 400))
	links = append(links, MakeNetworkLink("c", "NETWORK", "a", "COMPUTE", 100, 400))
	paths = BuildNetworkPaths(links)
	if len(paths) != 2 {
		t.Errorf("expected two paths but got %+v", len(paths))
	}

	// Add another link so there is a new path to another compute node.
	links = append(links, MakeNetworkLink("c", "NETWORK", "d", "COMPUTE", 100, 400))
	links = append(links, MakeNetworkLink("d", "COMPUTE", "c", "NETWORK", 100, 400))
	paths = BuildNetworkPaths(links)
	if len(paths) != 6 {
		t.Errorf("expected six paths but got %+v", len(paths))
	}
}

func TestBuildNetworkPaths_LargeTopology(t *testing.T) {
	links := []crd.NetworkLink{
		MakeNetworkLink("c1", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c1", "COMPUTE", 200, 5),
		MakeNetworkLink("n1", "NETWORK", "n1", "NETWORK", 200, 5),
		MakeNetworkLink("c1", "COMPUTE", "c1", "COMPUTE", 200, 5),

		MakeNetworkLink("c2", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c2", "COMPUTE", 200, 5),
		MakeNetworkLink("c2", "COMPUTE", "c2", "COMPUTE", 200, 5),

		MakeNetworkLink("c3", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c3", "COMPUTE", 200, 5),
		MakeNetworkLink("c3", "COMPUTE", "c3", "COMPUTE", 200, 5),

		MakeNetworkLink("c4", "COMPUTE", "n2", "NETWORK", 100, 10),
		MakeNetworkLink("n2", "NETWORK", "c4", "COMPUTE", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("c4", "COMPUTE", "c4", "COMPUTE", 200, 5),

		MakeNetworkLink("c6", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c6", "COMPUTE", 200, 5),
		MakeNetworkLink("n3", "NETWORK", "n3", "NETWORK", 200, 5),
		MakeNetworkLink("c6", "COMPUTE", "c6", "COMPUTE", 200, 5),

		MakeNetworkLink("c7", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c7", "COMPUTE", 200, 5),
		MakeNetworkLink("c7", "COMPUTE", "c7", "COMPUTE", 200, 5),

		MakeNetworkLink("c5", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c5", "COMPUTE", 200, 5),
		MakeNetworkLink("c5", "COMPUTE", "c5", "COMPUTE", 200, 5),

		MakeNetworkLink("n1", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n1", "NETWORK", 200, 5),
		MakeNetworkLink("n3", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n3", "NETWORK", 200, 5),
	}
	paths := BuildNetworkPaths(links)
	// There are seven compute nodes and there is one path between each
	// pair of compute nodes without loopback paths. Thus, there need to be 42 paths.
	if len(paths) != 52 {
		t.Errorf("expected 52 paths but got %d: %+v", len(paths), paths)
	}
}
