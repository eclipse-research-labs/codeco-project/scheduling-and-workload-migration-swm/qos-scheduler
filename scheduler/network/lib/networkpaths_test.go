// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func MakeNetworkPath(fromName, toName string, links []string) crd.NetworkPath {
	node1 := crd.LinkNode{
		Name: fromName,
		Type: crd.NodeType("COMPUTE"),
	}
	node2 := crd.LinkNode{
		Name: toName,
		Type: crd.NodeType("COMPUTE"),
	}
	spec := crd.NetworkPathSpec{
		Start: node1,
		End:   node2,
		Links: links,
	}
	return crd.NetworkPath{
		TypeMeta: metav1.TypeMeta{Kind: "NetworkLink", APIVersion: "v1alpha1"},
		ObjectMeta: metav1.ObjectMeta{
			Name:      MakeNetworkPathName(spec),
			Namespace: "default",
		},
		Spec: spec,
	}
}

func TestPatchNetworkPaths_allnew(t *testing.T) {
	newPaths := []crd.NetworkPath{
		MakeNetworkPath("a", "b", []string{"a-t", "t-b"}),
		MakeNetworkPath("b", "c", []string{"b-c"}),
	}
	oldPathMap := MakeNetworkPathsMap([]crd.NetworkPath{})
	newPathMap := MakeNetworkPathsMap(newPaths)
	c, u, d := PatchNetworkPaths(oldPathMap, newPathMap)

	if len(c) != len(newPaths) {
		t.Errorf("expected %d new paths but got %d", len(newPaths), len(c))
	}
	if len(u) != 0 {
		t.Errorf("expected no updates but got %v", u)
	}
	if len(d) != 0 {
		t.Errorf("expected no deletions but got %v", d)
	}
}

func TestPatchNetworkPaths_deletion(t *testing.T) {
	oldPaths := []crd.NetworkPath{
		MakeNetworkPath("a", "b", []string{"a-t", "t-b"}),
		MakeNetworkPath("b", "c", []string{"b-c"}),
	}
	newPaths := []crd.NetworkPath{
		MakeNetworkPath("b", "c", []string{"b-c"}),
	}
	oldPathMap := MakeNetworkPathsMap(oldPaths)
	newPathMap := MakeNetworkPathsMap(newPaths)
	c, u, d := PatchNetworkPaths(oldPathMap, newPathMap)

	if len(c) != 0 {
		t.Errorf("expected no new paths but got %v", c)
	}
	if len(u) != 1 {
		t.Errorf("expected one update but got %v", u)
	}
	if len(d) != 1 {
		t.Errorf("expected no deletions but got %v", d)
	}
}

func TestPatchNetworkPaths_update(t *testing.T) {
	oldPaths := []crd.NetworkPath{
		MakeNetworkPath("a", "b", []string{"a-t", "t-b"}),
		MakeNetworkPath("b", "c", []string{"b-c"}),
	}
	newPaths := []crd.NetworkPath{
		MakeNetworkPath("b", "c", []string{"b-c"}),
	}
	oldPathMap := MakeNetworkPathsMap(oldPaths)
	newPathMap := MakeNetworkPathsMap(newPaths)
	c, u, d := PatchNetworkPaths(oldPathMap, newPathMap)

	if len(c) != 0 {
		t.Errorf("expected no new paths but got %v", c)
	}
	if len(u) != 1 {
		t.Errorf("expected one update but got %v", u)
	}
	if len(d) != 1 {
		t.Errorf("expected no deletions but got %v", d)
	}
}

func TestPathsForChannel(t *testing.T) {
	paths := []crd.NetworkPath{
		MakeNetworkPath("a", "b", []string{"a-t", "t-b"}),
		MakeNetworkPath("c", "d", []string{"c-d"}),
	}
	if paths[0].Name == "" {
		t.Fatalf("expected path to have a name: %+v", paths[0])
	}
	var cspec crd.ChannelSpec

	cspec.ChannelFrom = "x"
	cspec.ChannelTo = "y"
	ret := PathsForChannel(cspec, paths)
	if len(ret) != 0 {
		t.Errorf("no path should have matched channel spec but got %d", len(ret))
	}
	cspec.ChannelFrom = "a"
	cspec.ChannelTo = "d"
	ret = PathsForChannel(cspec, paths)
	if len(ret) != 0 {
		t.Errorf("no path should have matched channel spec but got %d", len(ret))
	}

	cspec.ChannelFrom = "c"
	ret = PathsForChannel(cspec, paths)
	if len(ret) != 1 {
		t.Errorf("one path should have matched channel spec but got %d", len(ret))
	}

	cspec.ChannelPath = crd.ChannelPathRef{
		Namespace: "default",
		Name:      "x",
	}
	ret = PathsForChannel(cspec, paths)
	if len(ret) != 0 {
		t.Errorf("no path should have matched channel spec but got %d", len(ret))
	}
	cspec.ChannelPath = crd.ChannelPathRef{
		Namespace: paths[0].Namespace,
		Name:      paths[0].Name,
	}
	ret = PathsForChannel(cspec, paths)
	if len(ret) != 1 {
		t.Errorf("one path should have matched channel spec but got %d", len(ret))
	}
	if ret[0].Name != paths[0].Name || ret[0].Namespace != paths[0].Namespace {
		t.Errorf("expected path a,b for channel spec %+v but got %+v", cspec, ret)
	}

}
