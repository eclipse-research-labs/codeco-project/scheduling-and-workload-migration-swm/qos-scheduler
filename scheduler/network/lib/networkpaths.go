// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"
	"hash/fnv"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// MakeNetworkPathName returns the name of a network path based on its network implementation
// class and a hash of its path entries.
func MakeNetworkPathName(path crd.NetworkPathSpec) string {
	hash := fnv.New32a()
	for _, l := range path.Links {
		hash.Write([]byte(l))
	}

	return fmt.Sprintf("%s-%s-%d", conformantString(path.Start.Name),
		conformantString(path.End.Name), hash.Sum32())
}

// MakeNetworkPathsMap takes a list of network paths and returns a map
// of path name to path.
func MakeNetworkPathsMap(paths []crd.NetworkPath) map[string]crd.NetworkPath {
	ret := make(map[string]crd.NetworkPath)
	for _, p := range paths {
		ret[p.Name] = p
	}
	return ret
}

// PatchNetworkPaths applies an update to network paths.
// pathMap is the current path map. newPaths is from an update.
// Return lists of paths to create, update, delete in order for the
// current set of paths to match newPaths.
func PatchNetworkPaths(pathMap map[string]crd.NetworkPath, newPaths map[string]crd.NetworkPath) ([]crd.NetworkPath, []crd.NetworkPath, []crd.NetworkPath) {

	pathsToCreate := make([]crd.NetworkPath, 0, len(newPaths))
	pathsToUpdate := make([]crd.NetworkPath, 0, len(newPaths))
	pathsToDelete := make([]crd.NetworkPath, 0, len(pathMap))

	for newName, newPath := range newPaths {
		_, found := pathMap[newName]
		if !found {
			pathsToCreate = append(pathsToCreate, newPath)
			continue
		}
		pathsToUpdate = append(pathsToUpdate, newPath)
	}

	for oldName, oldPath := range pathMap {
		_, found := newPaths[oldName]
		if !found {
			pathsToDelete = append(pathsToDelete, oldPath)
		}
	}
	return pathsToCreate, pathsToUpdate, pathsToDelete
}

// Create and return a new NetworkPathSpec that is the same as oldPath but with linkToAdd
// appended at the end.
// This does not verify that linkToAdd fits, i.e. that it starts at the end of oldPath.Links
func appendLinkToPath(oldPath crd.NetworkPathSpec, linkToAdd crd.NetworkLink) crd.NetworkPathSpec {
	ret := crd.NetworkPathSpec{
		Start: oldPath.Start,
		End:   linkToAdd.Spec.LinkTo,
		Links: make([]string, len(oldPath.Links), len(oldPath.Links)+1),
	}
	copy(ret.Links, oldPath.Links)
	ret.Links = append(ret.Links, linkToAdd.Name)
	return ret
}

// PathsForChannel returns the subset of paths that can be used for channel.
// Returns an empty list if none of the paths can be used.
// If the channel specifies a path, only that path (or the empty list) will be returned,
// otherwise all paths that link the channel's start and end points.
func PathsForChannel(channel crd.ChannelSpec, paths []crd.NetworkPath) []crd.NetworkPath {
	// If the channel specifies a path, use only that one.
	if channel.ChannelPath.Name != "" {
		for _, p := range paths {
			if channel.ChannelPath.Name == p.Name && channel.ChannelPath.Namespace == p.Namespace {
				return []crd.NetworkPath{p}
			}
		}
		return []crd.NetworkPath{}
	}
	ret := make([]crd.NetworkPath, 0, len(paths))
	for _, p := range paths {
		if p.Spec.Start.Name == channel.ChannelFrom && p.Spec.End.Name == channel.ChannelTo {
			ret = append(ret, p)
		}
	}
	return ret
}
