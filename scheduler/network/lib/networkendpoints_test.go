// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func MakeNetworkEndpoint(name string, interfaceNames []string, macAddresses []string, ipAddresses []string) crd.NetworkEndpoint {
	interf := make([]crd.NetworkEndpointNetworkInterface, len(interfaceNames))
	for i, inf := range interfaceNames {
		interf[i] = crd.NetworkEndpointNetworkInterface{
			Name:        inf,
			MacAddress:  macAddresses[i],
			IPv4Address: ipAddresses[i],
		}
	}
	return crd.NetworkEndpoint{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: crd.NetworkEndpointSpec{
			NodeName:                name,
			NetworkInterfaces:       interf,
			MappedNetworkInterfaces: make(map[string]string),
		},
	}
}

type matchingTestSpec struct {
	topoNode        crd.TopologyNodeSpec
	expectedMatches []string
	description     string
}

func TestFindMatchingNetworkEndpoints(t *testing.T) {
	cn := map[string]crd.NetworkEndpoint{
		"c1": MakeNetworkEndpoint("c1", []string{"eth0", "eth1"}, []string{"mac10", "mac11"}, []string{"ip10", "ip11"}),
		"c2": MakeNetworkEndpoint("c2", []string{"eth0", "eth1"}, []string{"mac20", "mac21"}, []string{"ip20", "ip21"}),
		"c3": MakeNetworkEndpoint("c3", []string{"eth0", "eth1"}, []string{"mac30", "mac31"}, []string{"ip30", "ip31"}),
		"c4": MakeNetworkEndpoint("c4", []string{"eth0", "eth1"}, []string{"mac40", "mac41"}, []string{"ip40", "ip41"}),
	}

	testSpecs := []matchingTestSpec{
		{
			topoNode:        crd.TopologyNodeSpec{Name: "c1"},
			expectedMatches: []string{"c1"},
			description:     "match based on node name",
		},
		{
			topoNode:        crd.TopologyNodeSpec{Name: "none", IPAddress: "nothing"},
			expectedMatches: []string{},
			description:     "nonexistent node",
		},
	}

	for _, spec := range testSpecs {
		matches := findMatchingNetworkEndpoints(cn, spec.topoNode, "testnet")
		if len(matches) != len(spec.expectedMatches) {
			t.Errorf("testing %s and found %d matches (%v) but expected %d", spec.description, len(matches), matches, len(spec.expectedMatches))
		}
		for _, m := range spec.expectedMatches {
			found := false
			for _, n := range matches {
				if n.Name == m {
					found = true
					break
				}
			}
			if !found {
				t.Errorf("testing %s and expected result %s is missing", spec.description, m)
			}
		}
	}
}

type patchTestSpec struct {
	target        string
	patchNode     crd.TopologyNodeSpec
	expectSuccess bool
	description   string
}

func TestPatchNetworkEndpointSpec(t *testing.T) {
	cn := map[string]crd.NetworkEndpoint{
		"c1": MakeNetworkEndpoint("c1", []string{"eth0", "eth1"}, []string{"mac10", "mac11"}, []string{"ip10", "ip11"}),
		"c2": MakeNetworkEndpoint("c2", []string{"eth0", "eth1"}, []string{"mac20", "mac21"}, []string{"ip20", "ip21"}),
		"c3": MakeNetworkEndpoint("c3", []string{"eth0", "eth1"}, []string{"mac30", "mac31"}, []string{"ip30", "ip31"}),
		"c4": MakeNetworkEndpoint("c4", []string{"eth0", "eth1"}, []string{"mac40", "mac41"}, []string{"ip40", "ip41"}),
		"c5": MakeNetworkEndpoint("c5", []string{"eth0", "eth1"}, []string{"mac50", "mac51"}, []string{"ip50", "ip51"}),
		"c6": MakeNetworkEndpoint("c6", []string{"eth0", "eth1"}, []string{"mac60", "mac61"}, []string{"ip60", "ip61"}),
	}

	cn["c3"].Spec.MappedNetworkInterfaces["testnet"] = "eth1"

	testSpecs := []patchTestSpec{
		{
			patchNode:     crd.TopologyNodeSpec{Name: "c1"},
			target:        "c1",
			expectSuccess: true,
			description:   "no-op patch should succeed",
		},
		{
			patchNode:     crd.TopologyNodeSpec{Name: "c1", MACAddress: "mac11"},
			target:        "c1",
			expectSuccess: true,
			description:   "should accept mac address information",
		},
		{
			patchNode:     crd.TopologyNodeSpec{Name: "c2", IPAddress: "mac31"},
			target:        "c2",
			expectSuccess: false,
			description:   "should refuse to update with an ip address that the node does not have",
		},
		{
			patchNode:     crd.TopologyNodeSpec{Name: "c3", IPAddress: "ip30"},
			target:        "c3",
			expectSuccess: false,
			description:   "should refuse update for the wrong network interface",
		},
		{
			patchNode:     crd.TopologyNodeSpec{Name: "c3", IPAddress: "ip31"},
			target:        "c3",
			expectSuccess: true,
			description:   "should accept update for the right network interface",
		},
	}
	for _, spec := range testSpecs {
		_, err := patchNetworkEndpointSpec(cn[spec.target].Spec, spec.patchNode, "testnet")
		if err != nil && spec.expectSuccess {
			t.Errorf("unexpected error for test %s: %v", spec.description, err)
		} else if err == nil && !spec.expectSuccess {
			t.Errorf("expected failure for %s but got success", spec.description)
		}
	}
}
