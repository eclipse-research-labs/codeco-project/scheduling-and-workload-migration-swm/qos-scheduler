# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= ""

define mkdir # path
$(eval $(1): ; mkdir -p $$@)
endef

# Install protoc by downloading it from
# https://github.com/protocolbuffers/protobuf/releases
# curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v3.15.8/protoc-3.15.8-linux-x86_64.zip
# mkdir -p bin/protoc
# unzip protoc-3.15.8-linux-x86_64.zip -d bin/protoc
ifeq (,$(shell which protoc))
PROTOC = $(shell pwd)/bin/protoc/bin/protoc
else
PROTOC = protoc
endif

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

MOCKGEN ?= $(GOBIN)/mockgen
ADDLICENSE ?= $(GOBIN)/addlicense
GINKGO ?= $(GOBIN)/ginkgo

CONTROLLER_GEN ?= $(GOBIN)/controller-gen

# PROTOC_GEN_GO comes from go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
# You also need the grpc plugin from go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@vlatest
PROTOC_GEN_GO ?= $(GOBIN)/protoc-gen-go

# Setting SHELL to bash allows bash commands to be executed by recipes.
# This is a requirement for 'setup-envtest.sh' in the test target.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

all: fmt vet build test

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc3_adv_awk.php

help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Development

CODE_PKGS ?= $(shell go list ./... | grep -v -E "v1alpha1|assignment")
CODE_PKG_LIST ?= $(shell go list ./... | grep -v -E "v1alpha1|assignment" | tr '\n' ',')

mocks: $(MOCKGEN) ## Generate mocks
	$(MOCKGEN) --copyright_file=./copyrightfile-golang.txt siemens.com/qos-scheduler/scheduler/assignment SchedulerClient,Scheduler_ScheduleClient > optimizer/client/mocks/mock_scheduler_client.go

# TODO: add generation of roles for the scheduler and the node daemon set.
# The network roles do not get generated here because the Helm chart creates them
# based on which custom networks exist.
manifests: $(CONTROLLER_GEN) ## Generate ClusterRole and CustomResourceDefinition objects.
	$(CONTROLLER_GEN) paths="./..." crd:headerFile="./copyrightfile-yaml.txt",crdVersions=v1,allowDangerousTypes=true,ignoreUnexportedFields=true output:crd:artifacts:config=../helm/qos-scheduler/charts/qos-crds/crds
	$(CONTROLLER_GEN) rbac:headerFile="./copyrightfile-yaml.txt",roleName=manager-role paths="./..." output:rbac:artifacts:config=../helm/qos-scheduler/charts/controller/templates

generate: $(CONTROLLER_GEN) ## Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
	$(CONTROLLER_GEN) object:headerFile="./copyrightfile-golang.txt" paths="./..."

fmt: $(ADDLICENSE) ## Run go fmt against code. 
	go fmt ${CODE_PKGS}
	$(ADDLICENSE) -c "Siemens AG" -l "Apache-2.0" -s=only .

vet: ## Run go vet against code (except generated code).
	go vet -tags=mock ${CODE_PKGS}
	staticcheck -tags=mock ${CODE_PKGS}

ENVTEST_ASSETS_DIR ?= $(shell pwd)/testbin
setup-envtest=$(ENVTEST_ASSETS_DIR)/setup-envtest

$(ENVTEST_ASSETS_DIR): ; mkdir -p $@
$(setup-envtest): | $(ENVTEST_ASSETS_DIR)
	GOBIN=${ENVTEST_ASSETS_DIR} go install sigs.k8s.io/controller-runtime/tools/setup-envtest@latest \
	&& $@ use $(ENVTEST_K8S_VERSION)

e2etests:
	KUBECONFIG=$(KUBECONFIG) USE_EXISTING_CLUSTER=true \
	ginkgo --randomize-all -cover --succinct -tags=integration,mock --flake-attempts=1 ./controllers/test/e2e/...

define mkdir # path(s)
$(eval $(1): ; mkdir -p $$@)
endef
GO-JUNIT-REPORT=go-junit-report
GO-JUNIT-OPTIONS=-set-exit-code -p go.version=$(VERSION_GOLANG)

$(call mkdir,../deliv/report)
largetest: test
define run-ginkgo-for # package,name
$(eval
large-test-$(if $(2),$(2),$(1)): manifests $(setup-envtest) | ../deliv/report
	source <($(setup-envtest) use -i -p env ${ENVTEST_K8S_VERSION}); $\
	ginkgo --randomize-all -cover --succinct --flake-attempts=1 $\
	    -coverpkg "siemens.com/qos-scheduler/scheduler/..."  $\
	    --coverprofile="coverage-out" $\
	    -tags=integration,mock ./$(1)/... $\
	| $$(GO-JUNIT-REPORT) $$(GO-JUNIT-OPTIONS) $\
       	-package-name siemens.com/qos-scheduler/scheduler/$(1) $\
	    >../deliv/report/$(subst /,-,$(1)).xml
	go tool cover -html=coverage-out -o ../deliv/report/$(subst /,-,$(1)).html
largetest: large-test-$(if $(2),$(2),$(1)))
endef
$(call run-ginkgo-for,controllers/test/integration,controllers)
$(call run-ginkgo-for,network)
$(call run-ginkgo-for,network/test/integration,network-integ)

focustest: manifests $(setup-envtest)
	source <($(setup-envtest) use -i -p env ${ENVTEST_K8S_VERSION});\
	ginkgo --succinct -tags=integration,mock ./network/...

$(call mkdir,controllers/testdata/)
test: generate mocks | controllers/testdata/
	mkdir -p ${PWD}/coverage/unit
	# cover package is broken in go 1.22
	#go test -cover -v -tags=mock -coverpkg "${CODE_PKG_LIST}" ./... -args -test.gocoverdir="${PWD}/coverage/unit/"
	go test -cover -v -tags=mock ./... -args -test.gocoverdir="${PWD}/coverage/unit/"

test-flags:=-v -cpuprofile=$(1).cpuprof -memprofile=$(1).memprof

# After running this, you can look at it with
# go tool pprof controllers.testing controllerslarge.cpuprof
# then type 'web' at the prompt to get a graph in your browser
perftest: generate $(setup-envtest)
	go test $(call test-flags,controllers) -tags=mock ./controllers/...
	$(setup-envtest) use ${ENVTEST_K8S_VERSION}
	source <($(setup-envtest) use -i -p env ${ENVTEST_K8S_VERSION});\
	go test $(call test-flags,controllerslarge) -tags=integration,mock ./controllers/..

##@ Build

protos: assignment/optimizer.pb.go assignment/optimizer-service_grpc.pb.go

assignment/optimizer.pb.go: proto/optimizer.proto | $(PROTOC_GEN_GO)
	$(PROTOC) -I proto --go_out=. --deepcopy_out=. proto/optimizer.proto

assignment/optimizer-service_grpc.pb.go: proto/optimizer-service.proto | $(PROTOC_GEN_GO)
	$(PROTOC) -I proto --go_out=. --go-grpc_out=. proto/optimizer-service.proto

cnitest:
	go test ./cni/node-subinterface/... -v

# The cnilargetest target must run as root because it creates network
# interfaces. Best to run it inside a docker container.
cnidockertest: cnitest
	docker build -t cnilargetest:latest -f cni/Dockerfile-cnitests .
	docker run --rm --network=none --privileged --entrypoint=/usr/bin/make -v ${PWD}:/localdev cnilargetest:latest cnilargetest

cnilargetest: cnitest
	ginkgo --succinct -tags=integration,mock,runasroot ./cni/node-subinterface/...

GOFLAGS=-ldflags "-s -w"

arch=arm64
binaries=$(addprefix bin/,controller_$(arch) scheduler_$(arch) daemon_$(arch) \
	network-k8s_$(arch) network-l2sm_$(arch) network-tsn_$(arch) network-topology_$(arch) \
	webhooks_$(arch) optimizer-stub_$(arch) cni-vlan_$(arch))
build: generate $(binaries) ## Build all binaries

define build-tool # name,file
$(eval
bin/$(1)_amd64: $(2) | bin
	GOOS=linux GOARCH=amd64 go build $$(GOFLAGS) -o $$@ $$<
bin/$(1)_arm64: $(2) | bin
	GOOS=linux GOARCH=arm64 go build $$(GOFLAGS) -o $$@ $$<)
endef

%.xz: %
	xz -z9kT1 $<
$(call mkdir,bin)
$(call build-tool,controller,main.go)
$(call build-tool,scheduler,schedulerplugins/main/app.go)
$(call build-tool,network-k8s,network/main/k8s-main.go)
$(call build-tool,network-l2sm,network/main/l2sm-main.go)
$(call build-tool,network-tsn,network/main/tsn-main.go)
$(call build-tool,network-topology,network/main/topology-main.go)
$(call build-tool,daemon,node/main/node-daemon.go)
$(call build-tool,webhooks,webhooks/main/main.go)
$(call build-tool,optimizer-stub,optimizer/server/main/scheduler_server.go)
$(call build-tool,cni-vlan,cni/node-subinterface/vlan.go)

images = controller cni-vlan networkoperator node-daemon optimizer-stub scheduler webhooks
container_dir = "../container/deploy"
current_branch = "$(shell git symbolic-ref --short HEAD)"
arch=amd64
builddockerlocal:
	for image in $(images) ; do \
		docker build --platform=$(arch) -f $(container_dir)/Dockerfile-$$image -t $$image:$(current_branch)_$(arch) bin ; \
	done


$(CONTROLLER_GEN): ## Download controller-gen locally if necessary.
	go install sigs.k8s.io/controller-tools/cmd/controller-gen@latest

$(MOCKGEN): ## Download mockgen locally if necessary.
	go get github.com/golang/mock/...
	go install github.com/golang/mock/mockgen@v1.6.0

$(ADDLICENSE): ## Download addlicense locally if necessary.
	go install github.com/google/addlicense@latest

$(PROTOC_GEN_GO):
	go install github.com/golang/protobuf/protoc-gen-go
	go install github.com/protobuf-tools/protoc-gen-deepcopy
