// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package utils contains generic utility functions
package utils

import "strings"

func PadLogName(name string) string {
	length := 40
	currentLength := len(name)

	if currentLength >= length {
		return name
	}

	spacesNeeded := length - currentLength
	padding := strings.Repeat(" ", spacesNeeded)
	return name + padding
}
