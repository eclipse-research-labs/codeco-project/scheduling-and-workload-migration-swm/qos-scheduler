# SPDX-FileCopyrightText: Siemens AG
# SPDX-License-Identifier: Apache-2.0

#Workload	Label
1	AccSens
2	AccSens
3	AccSens
4	AccSens
5	AccSens
6	TurbSens
7	TurbSens
8	TurbSens
9	TurbSens
10	TurbSens
11	Mic
12	Mic
13	Mic
14	Mic
15	Mic
16	Camera
17	Camera
18	Camera
19	Camera
20	Camera
21	PLC
29	MDSP
31	SDN
32	SDN
33	SDN
34	SDN
