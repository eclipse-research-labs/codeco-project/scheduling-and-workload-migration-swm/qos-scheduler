// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"context"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
)

// Some helper types and methods for the state machines in the application controller
// and the application group controller.

// How to use this:
// the Reconcile() loop looks at the object's current Status.Phase.
// it calls `Process<Phase>State`(ctx, object)`.
// this is a function that only reads data, it doesn't modify anything.
// It returns a new state, an `applyStateTransitionFn` and an error.
// If the error is not nil, the new state and the transition function are ignored, and Reconcile() returns a retry.
// Otherwise, the function is called. If this returns an error, the new state is ignored,
// and Reconcile() returns a retry.
// If the function call was successful, the state is updated to the new state.
// If that state update fails, Reconcile() returns a retry.

// Because of the possibility of failure and retry after calling the state transition function,
// state transition functions must be idempotent.
// However, because they are called by the Reconcile() loop on an object that the controller
// keys on, they do not need to guard against being called concurrently for the same object.
// (They can, of course, be executed concurrently for different instances of applications etc).

type applyStateTransitionFn = func() error

// An implementation of a state transition function that takes application-group-specific
// arguments.
type implementGroupStateTransitionFn = func(context.Context,
	*crd.ApplicationGroup, *application.OptimizerReply) error

// An implementation of a state transition function that takes application-specific
// arguments.
type implementStateTransitionFn = func(context.Context, *crd.Application) error

// Bind parameters and return an applyStateTransitionFn.
func makeGroupApplyFn(ctx context.Context, appGroup *crd.ApplicationGroup,
	reply *application.OptimizerReply, fn implementGroupStateTransitionFn) applyStateTransitionFn {
	return func() error { return fn(ctx, appGroup, reply) }
}

// Bind parameters and return an applyStateTransitionFn.
func makeApplyFn(ctx context.Context, application *crd.Application,
	fn implementStateTransitionFn) applyStateTransitionFn {
	return func() error { return fn(ctx, application) }
}

// A generic implementation of applyStateTransitionFn if you just want to do nothing.
func nopStateTransitionFunction() error { return nil }
