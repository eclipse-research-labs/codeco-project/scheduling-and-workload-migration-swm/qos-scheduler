// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
)

// These are the metrics that the controllers export.
// If you want to use them from elsewhere, probably best to move this file to a utilities package
// and change the variable names to uppercase.
var (
	metricPrefix       string
	applicationGroups  *prometheus.GaugeVec
	applications       *prometheus.GaugeVec
	channels           *prometheus.GaugeVec
	optimizerFailures  *prometheus.CounterVec
	optimizerTimeouts  *prometheus.CounterVec
	optimizerRetries   *prometheus.CounterVec
	optimizerExhausted *prometheus.CounterVec
)

func init() {
	metricPrefix = "qos_"
	applicationGroups = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: metricPrefix + "applicationGroups",
		Help: "Application groups with namespace, name, and phase",
	}, []string{"namespace", "name", "phase"})

	applications = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: metricPrefix + "applications",
		Help: "Applications with namespace, name, and phase",
	}, []string{"namespace", "name", "phase"})

	channels = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: metricPrefix + "channels",
		Help: "Channels with their status",
	}, []string{"namespace", "name", "serviceClass", "status"})

	optimizerFailures = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: metricPrefix + "optimizerFailures",
		Help: "how many times an optimizer call has ended in failure",
	}, []string{"namespace", "application_group"})

	optimizerTimeouts = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: metricPrefix + "optimizerTimeouts",
		Help: "how many times an optimizer call has timed out",
	}, []string{"namespace", "application_group"})

	optimizerRetries = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: metricPrefix + "optimizerRetries",
		Help: "total number of optimizer retries",
	}, []string{"namespace", "application_group"})

	// This number can be > 1 if you delete and re-create an application group that has trouble
	// getting scheduled.
	optimizerExhausted = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: metricPrefix + "optimizerExhausted",
		Help: "how many times the number of optimizer retries was exhausted",
	}, []string{"namespace", "application_group"})

	metrics.Registry.MustRegister(applicationGroups,
		applications,
		channels,
		optimizerFailures,
		optimizerTimeouts,
		optimizerRetries,
		optimizerExhausted)
}
