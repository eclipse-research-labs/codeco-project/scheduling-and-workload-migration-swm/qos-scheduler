// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package e2e

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
	"os"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
)

func deleteApplicationGroup(k8sClient client.Client, name string, namespace string) error {
	err := k8sClient.Delete(context.Background(), &crd.ApplicationGroup{ObjectMeta: metav1.ObjectMeta{
		Name:      name,
		Namespace: namespace,
	}})
	return err
}
func createNamespace(k8sClient client.Client, name string) error {
	obj := &corev1.Namespace{}
	err := k8sClient.Get(context.Background(), client.ObjectKey{Name: name}, obj)
	if err == nil {
		return nil
	}
	if err != nil {
		if !errors.IsNotFound(err) {
			return err
		}
	}
	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}
	return k8sClient.Create(context.Background(), ns)
}
func deleteNamespace(k8sClient client.Client, name string) error {
	err := k8sClient.Delete(context.Background(), &corev1.Namespace{ObjectMeta: metav1.ObjectMeta{
		Name: name,
	}})
	return err
}
func createApplicationGroup(k8sClient client.Client, name string, namespace string) error {
	appGroup := &crd.ApplicationGroup{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: crd.ApplicationGroupSpec{
			MinMember: 1,
		},
	}
	return k8sClient.Create(context.Background(), appGroup)
}

func deleteApplication(k8sClient client.Client, name string, namespace string) error {
	err := k8sClient.Delete(context.Background(), &crd.Application{ObjectMeta: metav1.ObjectMeta{
		Name:      name,
		Namespace: namespace,
	}})
	return err
}

func createSimpleApplication(k8sClient client.Client, applicationGroupName string, appName string, namespace string, workload1 string, workload2 string, serviceClassW1 string, serviceClassW2 string) error {
	wl1Pod := makeApplicationPodTemplateSpec(workload1, "wbitt/network-multitool")
	wl2PodSpec := makeApplicationPodTemplateSpec(workload2, "wbitt/network-multitool")
	ch1 := makeApplicationChannel(appName, "ch-wl1", "wl2", serviceClassW1)
	ch2 := makeApplicationChannel(appName, "ch-wl2", "wl1", serviceClassW2)
	wl1 := makeApplicationWorkloadSpec(workload1, wl1Pod, []crd.NetworkChannel{ch1})
	wl2 := makeApplicationWorkloadSpec(workload2, wl2PodSpec, []crd.NetworkChannel{ch2})
	app := makeApplication(applicationGroupName, appName, namespace, []crd.ApplicationWorkloadSpec{wl1, wl2})
	err := k8sClient.Create(context.Background(), app)
	return err
}

func makeApplication(applicationGroupName string, appName string, namespace string, workloads []crd.ApplicationWorkloadSpec) *crd.Application {
	return &crd.Application{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "qos-scheduler.siemens.com/v1alpha1",
			Kind:       "Application",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      appName,
			Namespace: namespace,
			Labels: map[string]string{
				"application-group": applicationGroupName,
			},
		},
		Spec: crd.ApplicationSpec{
			Workloads: workloads,
		},
	}
}

func resourceQuantity(quantity string) resource.Quantity {
	q, _ := resource.ParseQuantity(quantity)
	return q
}

func makeApplicationPodTemplateSpec(name string, image string) crd.QosPodTemplateSpec {
	return crd.QosPodTemplateSpec{
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  name,
					Image: image,
					Resources: corev1.ResourceRequirements{
						Limits: corev1.ResourceList{
							"cpu":    resourceQuantity("100m"),
							"memory": resourceQuantity("20Mi"),
						},
						Requests: corev1.ResourceList{
							"cpu":    resourceQuantity("100m"),
							"memory": resourceQuantity("20Mi"),
						},
					},
				},
			},
		},
	}
}

func makeApplicationWorkloadSpec(name string, workload crd.QosPodTemplateSpec, channels []crd.NetworkChannel) crd.ApplicationWorkloadSpec {
	return crd.ApplicationWorkloadSpec{
		Basename: name,
		Template: workload,
		Channels: channels,
	}
}

func makeApplicationChannel(appName string, channelName string, otherWorkload string, serviceClass string) crd.NetworkChannel {
	return crd.NetworkChannel{
		Basename:      channelName,
		OtherWorkload: crd.WorkloadId{Basename: otherWorkload, ApplicationName: appName, Port: 3333},
		ServiceClass:  crd.NetworkServiceClass(serviceClass),
		MinBandwidth:  "1M",     // This is 100 MBits/s
		MaxDelay:      "100E-4", // This is 0.005 milliseconds aka 5 microseconds
		Framesize:     "150",    // This is in bytes. The max is 1518 bytes for ethernet.
	}
}

func podExec(k8sScheme *runtime.Scheme, restConfig *rest.Config, podName string, containerName string, namespace string, cmd string) error {

	execOptions := &corev1.PodExecOptions{
		Command:   []string{"sh", "-c", cmd},
		Container: containerName,
		Stdin:     true,
		Stdout:    true,
		Stderr:    true,
		TTY:       false,
	}
	gvk := schema.GroupVersionKind{
		Group:   "",
		Version: "v1",
		Kind:    "Pod",
	}
	httpClient, err := rest.HTTPClientFor(restConfig)
	if err != nil {
		return err
	}
	podRestClient, err := apiutil.RESTClientForGVK(gvk, false, restConfig, serializer.NewCodecFactory(k8sScheme), httpClient)

	request := podRestClient.Post().Namespace(namespace).Resource("pods").Name(podName).SubResource("exec").VersionedParams(execOptions, scheme.ParameterCodec)
	exec, err := remotecommand.NewSPDYExecutor(restConfig, "POST", request.URL())
	if err != nil {
		return err
	}

	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stdout,
	})
	return err
}
