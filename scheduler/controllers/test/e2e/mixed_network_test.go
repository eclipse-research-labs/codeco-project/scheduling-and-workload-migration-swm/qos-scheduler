// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package e2e

import (
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Mixed Network test", func() {
	const (
		namespace            = "e2e-mixed"
		applicationGroupName = "ag-mixed"
		applicationName      = "app-mixed"
		workload1            = "wl1"
		workload2            = "wl2"
		serviceClassW1       = "ASSURED"
		serviceClassW2       = "BEST_EFFORT"
		timeout              = time.Second * 1000
		interval             = time.Millisecond * 250
	)

	BeforeEach(func() {
		// create namespace
		err := createNamespace(k8sClient, namespace)
		Expect(err).To(BeNil())
		// create application group
		err = createApplicationGroup(k8sClient, applicationGroupName, namespace)
		Expect(err).To(BeNil())
		// create applications
		err = createSimpleApplication(k8sClient, applicationGroupName, applicationName, namespace, workload1, workload2, serviceClassW1, serviceClassW2)
		Expect(err).To(BeNil())
	})

	AfterEach(func() {
		err := deleteApplicationGroup(k8sClient, applicationGroupName, namespace)
		Expect(err).To(BeNil())
		err = deleteApplication(k8sClient, applicationName, namespace)
		Expect(err).To(BeNil())
		err = deleteNamespace(k8sClient, namespace)
		Expect(err).To(BeNil())
	})

	When("an Application Group and Applications are created", func() {
		It("this should result in running Pods", func() {
			Eventually(func() error {
				pod1Name := applicationName + "-" + workload1
				svc1Name := "svc-ch-" + workload1 + "-" + namespace
				svc2Name := "svc-ch-" + workload2 + "-" + namespace
				pod2Name := applicationName + "-" + workload2
				return awaitWorkloadsMixed(svc1Name, svc2Name, pod1Name, pod2Name, workload1, workload2, namespace)
			}, timeout, interval).Should(BeNil())
		})
	})

})

func awaitWorkloadsMixed(svc1Name string, svc2Name string, pod1Name string, pod2Name string, workload1Name string, workload2Name string, namespace string) error {
	var pod1 corev1.Pod
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: pod1Name, Namespace: namespace}, &pod1); err != nil {
		return err
	}
	var svc1 corev1.Service
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: svc1Name, Namespace: namespace}, &svc1); err != nil {
		return err
	}
	var pod2 corev1.Pod
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: pod2Name, Namespace: namespace}, &pod2); err != nil {
		return err
	}
	var svc2 corev1.Service
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: svc2Name, Namespace: namespace}, &svc2); err != nil {
		return err
	}
	if pod1.Status.Phase != corev1.PodRunning {
		return fmt.Errorf("pod %s not running", pod1Name)
	}

	if pod2.Status.Phase != corev1.PodRunning {
		return fmt.Errorf("pod %s not running", pod2Name)
	}

	err := podExec(k8sScheme, k8sConfig, pod1Name, workload1Name, namespace, "ping -c2 "+svc2Name)
	Expect(err).To(BeNil())

	err = podExec(k8sScheme, k8sConfig, pod2Name, workload2Name, namespace, "ping -c2 "+svc1Name)
	Expect(err).To(BeNil())

	return nil
}
