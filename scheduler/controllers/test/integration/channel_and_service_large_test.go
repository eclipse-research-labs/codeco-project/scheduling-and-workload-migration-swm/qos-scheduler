// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

var _ = Describe("AppGroup_Channel and service requests in the controllers", func() {

	const (
		timeout   = time.Second * 10
		interval  = time.Millisecond * 250
		namespace = "default"
	)

	var (
		applicationGroupName string
		ctx                  context.Context
	)

	BeforeEach(func() {
		applicationGroupName = ""
	})

	JustBeforeEach(func() {
		if applicationGroupName == "" {
			return
		}
		ctx = context.Background()
		createApplicationGroup(applicationGroupName, namespace)

		app1Name := fmt.Sprintf("%s-app-1", applicationGroupName)
		app2Name := fmt.Sprintf("%s-app-2", applicationGroupName)
		app1 := *application.MakeApplication(app1Name, namespace, []string{"workload"},
			[]qosscheduler.WorkloadId{
				{
					ApplicationName: app2Name,
					Basename:        "workload",
					Port:            3333,
				},
			})
		app1.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app1)

		app2 := *application.MakeApplication(app2Name, namespace, []string{"workload"},
			[]qosscheduler.WorkloadId{
				{
					ApplicationName: app1Name,
					Basename:        "workload",
					Port:            4444,
				},
			})
		app2.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app2)
	})

	AfterEach(func() {
		if applicationGroupName == "" {
			return
		}
		var group qosscheduler.ApplicationGroup
		if err := readGroup(applicationGroupName, namespace, &group); err == nil {
			k8sClient.Delete(ctx, &group)
		}
	})

	When("the assignment plan contains channels", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-with-channels" })

		It("should create channel requests", func() {
			app1Name := applicationGroupName + "-app-1"
			app2Name := applicationGroupName + "-app-2"
			Eventually(func() error {
				var app qosscheduler.Application
				if err := k8sClient.Get(context.Background(), types.NamespacedName{Name: app1Name, Namespace: namespace}, &app); err != nil {
					return err
				}
				if app.Status.Phase != "" {
					return nil
				}
				return fmt.Errorf("application %s not ready yet", app1Name)
			}, timeout, interval).Should(BeNil())

			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))

			// create a valid assignment plan with channels.
			ass := assignment.Assignment{
				Workloads: []*assignment.Assignment_Workload{
					{
						Application: app1Name,
						Workload:    app1Name + "-workload",
						Node:        "node1",
					},
					{
						Application: app2Name,
						Workload:    app2Name + "-workload",
						Node:        "node2",
					},
				},
				Channels: []*assignment.Assignment_Channel{
					{
						Channel: "if0-to-workload-" + app2Name,
						Source:  "node1",
						Target:  "node2",
					},
					{
						Channel: "if0-to-workload-" + app1Name,
						Source:  "node2",
						Target:  "node1",
					},
				},
			}
			applications := &qosscheduler.ApplicationList{}
			Expect(k8sClient.List(ctx, applications)).Should(Succeed())
			model := &assignment.Model{
				AppGroup:       application.GetApplicationModel(applications.Items),
				Infrastructure: infrastructure.MakeInfrastructureModel(),
			}
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.QoSModel = *model.DeepCopy()
			plan.Spec.OptimizerReply.Assignment = &ass
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = true
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())

			// This verifies that the optimizer plan has been received and turned into
			// a schedule.
			Eventually(func() error {
				if err := readPlan(applicationGroupName, namespace, &plan); err != nil {
					return err
				}
				if len(plan.Status.PodStatus) == 0 {
					return fmt.Errorf("schedule is empty")
				}
				return nil
			}, 2*timeout, interval).Should(BeNil())

			// Verify that channels have been created for the right node connections and with the right target ports.
			Eventually(func() error {
				var channel1 qosscheduler.Channel
				if err := k8sClient.Get(context.Background(), types.NamespacedName{
					Name: "if0-to-workload-" + app2Name, Namespace: namespace}, &channel1); err != nil {
					return err
				}
				var channel2 qosscheduler.Channel
				if err := k8sClient.Get(context.Background(), types.NamespacedName{
					Name: "if0-to-workload-" + app1Name, Namespace: namespace}, &channel2); err != nil {
					return err
				}
				return nil
			}, timeout, interval).Should(BeNil())

		})
	})
})
