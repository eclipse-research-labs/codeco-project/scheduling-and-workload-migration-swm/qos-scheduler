// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
)

var _ = Describe("Applicationgroup controller", func() {
	const (
		namespace = "test-namespace"
		timeout   = time.Second * 20
		interval  = time.Millisecond * 250
	)
	var (
		applicationGroupName string
		ctx                  context.Context
		lookupKey            types.NamespacedName
	)
	BeforeEach(func() {
		applicationGroupName = ""
	})

	JustBeforeEach(func() {
		ctx = context.Background()
		createApplicationGroup(applicationGroupName, namespace)
		lookupKey = types.NamespacedName{Name: applicationGroupName, Namespace: namespace}
	})

	AfterEach(func() {
		var group qosscheduler.ApplicationGroup
		if err := readGroup(applicationGroupName, namespace, &group); err == nil {
			k8sClient.Delete(ctx, &group)
		}
	})

	When("Creating an Application Group", func() {
		BeforeEach(func() {
			applicationGroupName = "ag-creation"
		})
		It("Should start in phase Waiting", func() {
			var createdGroup qosscheduler.ApplicationGroup
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := k8sClient.Get(ctx, lookupKey, &createdGroup)
				if err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return createdGroup.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupWaiting))
		})
	})
	When("an Application Group is waiting for applications", func() {
		BeforeEach(func() {
			applicationGroupName = "ag-waiting"
		})
		It("should stay in Waiting until the desired number of applications", func() {
			var createdGroup qosscheduler.ApplicationGroup
			app := application.MakeApplication("ap-waiting-1", namespace, []string{"workload-1"}, []qosscheduler.WorkloadId{})
			app.Labels["application-group"] = applicationGroupName
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())

			Consistently(func() bool {
				err := k8sClient.Get(ctx, lookupKey, &createdGroup)
				if err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())
			Expect(createdGroup.Status.Phase).Should(Equal(qosscheduler.ApplicationGroupWaiting))

			By("adding another application but without the label, nothing changes")
			app = application.MakeApplication("ap-waiting-2", namespace, []string{"workload-2"}, []qosscheduler.WorkloadId{})
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())
			Consistently(func() bool {
				err := k8sClient.Get(ctx, lookupKey, &createdGroup)
				if err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())
			Expect(createdGroup.Status.Phase).Should(Equal(qosscheduler.ApplicationGroupWaiting))

			By("adding a second labelled application, the group's phase should advance to Optimizing")
			app = application.MakeApplication("ap-waiting-3", namespace, []string{"workload-3"}, []qosscheduler.WorkloadId{})
			app.Labels["application-group"] = applicationGroupName
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := k8sClient.Get(ctx, lookupKey, &createdGroup)
				if err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return createdGroup.Status.Phase
				// This test suite uses an empty optimizer, so the group will be stuck in Optimizing.
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))
		})
	})
	When("an Application Group finds applications", func() {
		BeforeEach(func() {
			applicationGroupName = "ag-ownership"
		})
		It("should take ownership of the applications that it contains and create an assignment plan", func() {

			// Create three applications, two of which are labeled for this application group.
			// The applicationgroup only takes ownership when it's ready to call the optimizer,
			// so this needs two labeled applications to exist.
			app := application.MakeApplication("ap-ownership-1", namespace, []string{"workload-1", "workload-a"}, []qosscheduler.WorkloadId{
				{ApplicationName: "ap-ownership-1",
					Basename: "workload-1",
					Port:     4444,
				},
			})
			app.Labels["application-group"] = applicationGroupName
			ownedAppKey := types.NamespacedName{Name: app.Name, Namespace: namespace}
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())
			app = application.MakeApplication("ap-ownership-2", namespace, []string{"workload-2"}, []qosscheduler.WorkloadId{})
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())
			unownedAppKey := types.NamespacedName{Name: app.Name, Namespace: namespace}
			app = application.MakeApplication("ap-ownership-3", namespace, []string{"workload-2"}, []qosscheduler.WorkloadId{})
			app.Labels["application-group"] = applicationGroupName
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, ownedAppKey, app)
				if err != nil {
					return false
				}
				cref := metav1.GetControllerOf(app)
				if cref == nil {
					return false
				}
				return cref.Name == applicationGroupName
			}, timeout, interval).Should(BeTrue())

			Consistently(func() bool {
				err := k8sClient.Get(ctx, unownedAppKey, app)
				if err != nil {
					return false
				}
				cref := metav1.GetControllerOf(app)
				if cref == nil {
					return false
				}
				return cref.Name == applicationGroupName
			}, timeout, interval).Should(BeFalse())

			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			Expect(plan.Spec.ReplyReceived).Should(Equal(false))
		})
	})
	When("an Application Group is deleted", func() {
		BeforeEach(func() {
			applicationGroupName = "ag-deleted"
		})
		It("should not cause a crash", func() {
			app := application.MakeApplication("ap-deletion-1", namespace, []string{"workload-1"}, []qosscheduler.WorkloadId{})
			app.Labels["application-group"] = applicationGroupName
			ownedAppKey := types.NamespacedName{Name: app.Name, Namespace: namespace}
			Expect(k8sClient.Create(ctx, app)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, ownedAppKey, app)
				if err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())

			var createdGroup qosscheduler.ApplicationGroup
			Expect(k8sClient.Get(ctx, lookupKey, &createdGroup)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, &createdGroup)).Should(Succeed())
			// It's possible to verify that the applications get deleted here too,
			// but it can take longer than 10s and it's really a Kubernetes feature
			// so doesn't seem to require testing here.
		})
	})
})
