// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

func getPodNames(app qosscheduler.Application) []string {
	ret := make([]string, 0)
	for _, wl := range app.Spec.Workloads {
		podName := fmt.Sprintf("%s-%s", app.Name, wl.Basename)
		ret = append(ret, podName)
	}
	return ret
}

var _ = Describe("Application controller", func() {

	const (
		ApplicationName = "test-application"
		NamespaceName   = "default"

		timeout  = time.Second * 10
		interval = time.Millisecond * 250
	)

	Context("After creating an Application", func() {
		ctx := context.Background()
		appLookupKey := types.NamespacedName{Name: ApplicationName, Namespace: NamespaceName}

		It("should go through its lifecycle", func() {
			By("creating a new Application, phase should be Waiting")
			app := createApplication(ApplicationName, NamespaceName)
			k8sClient.Create(ctx, app)

			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationWaiting))

			By("updating the application's status to Scheduling, it should create Pods")
			app.Status.Phase = qosscheduler.ApplicationScheduling
			Expect(k8sClient.Status().Update(ctx, app)).Should(Succeed())

			// The application goes into the scheduling phase, and then the pending phase.
			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationPending))

			// At this point, the pods for the application should exist and be in state
			// Pending because there is no scheduler to bring them out of that state.
			podNames := getPodNames(*app)
			for _, name := range podNames {
				podLookupKey := types.NamespacedName{Namespace: NamespaceName, Name: name}
				createdPod := &v1.Pod{}
				Expect(k8sClient.Get(ctx, podLookupKey, createdPod)).Should(Succeed())
				Expect(createdPod.Status.Phase).Should(Equal(v1.PodPending))
			}

			By("setting one pod's phase to Running, the application's phase stays pending")
			podLookupKey := types.NamespacedName{Namespace: NamespaceName, Name: podNames[0]}
			firstPod := &v1.Pod{}
			Expect(k8sClient.Get(ctx, podLookupKey, firstPod)).Should(Succeed())
			firstPod.Status.Phase = v1.PodRunning
			Expect(k8sClient.Status().Update(ctx, firstPod)).Should(Succeed())

			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationPending))

			By("setting both pods to running, the application's phase changes to running")
			podLookupKey = types.NamespacedName{Namespace: NamespaceName, Name: podNames[1]}
			secondPod := &v1.Pod{}
			Expect(k8sClient.Get(ctx, podLookupKey, secondPod)).Should(Succeed())
			secondPod.Status.Phase = v1.PodRunning
			Expect(k8sClient.Status().Update(ctx, secondPod)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationRunning))

			By("deleting one pod, the application should create that pod")
			podLookupKey = types.NamespacedName{Namespace: NamespaceName, Name: podNames[0]}
			firstPod = &v1.Pod{}
			Expect(k8sClient.Get(ctx, podLookupKey, firstPod)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, firstPod)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationPending))

			// Pod must have been created again.
			Expect(k8sClient.Get(ctx, podLookupKey, firstPod)).Should(Succeed())

			By("failing one pod, the application's phase changes to failing, then both pods are deleted and the application goes back to Waiting")

			Expect(k8sClient.Get(ctx, podLookupKey, firstPod)).Should(Succeed())
			firstPod.Status.Phase = v1.PodFailed
			Expect(k8sClient.Status().Update(ctx, firstPod)).Should(Succeed())

			Eventually(func() qosscheduler.ApplicationPhase {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationWaiting))

			Eventually(func() qosscheduler.ApplicationConditionType {
				ok := getApplication(appLookupKey, app)
				if !ok {
					return qosscheduler.ApplicationOk
				}
				if len(app.Status.Conditions) != 1 {
					return qosscheduler.ApplicationOk
				}
				return app.Status.Conditions[0].Type
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationPodFailed))

			// Verify that the not-failed pod gets terminated.
			podLookupKey = types.NamespacedName{Namespace: NamespaceName, Name: podNames[1]}
			Eventually(func() bool {
				err := k8sClient.Get(ctx, podLookupKey, secondPod)
				return err != nil
			}, timeout, interval).Should(Equal(true))
		})
	})
})
