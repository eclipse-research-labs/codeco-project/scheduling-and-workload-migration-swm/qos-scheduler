// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"os"

	"go.uber.org/zap/zapcore"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
	k8s "siemens.com/qos-scheduler/scheduler/network/k8s-nwcontroller"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	nwc "siemens.com/qos-scheduler/scheduler/network/topology-nwcontroller"
	"siemens.com/qos-scheduler/scheduler/utils"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup k8s network controller")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(crd.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var probeAddr string
	var networkImplementations string
	var channelController string
	var isPhysical bool
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.StringVar(&networkImplementations, "network-implementations", "K8S", "This flag has no effect and only exists to keep helm happy")
	flag.StringVar(&channelController, "channel-controller", "", "This flag has no effect on this controller")
	flag.BoolVar(&isPhysical, "physical-network", true, "This flag has no effect on this controller")
	opts := zap.Options{
		Development: true,
		Level:       zapcore.InfoLevel,
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme: scheme,
		Metrics: metricsserver.Options{
			BindAddress: metricsAddr,
		},
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager for k8s network controller")
		os.Exit(1)
	}
	baseForK8s := base.BaseNetworkReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName(utils.PadLogName("controllers.BaseNetworkReconciler")),
		Scheme: mgr.GetScheme(),
	}
	k8sReconciler := k8s.K8sNetworkReconciler{
		BaseNetworkReconciler: baseForK8s,
	}
	if err = k8sReconciler.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup fallback network controller")
		os.Exit(1)
	}

	channelReconciler := k8s.K8sChannelReconciler{
		ChannelReconciler: chctrl.ChannelReconciler{
			Client:                 mgr.GetClient(),
			Log:                    ctrl.Log.WithName(utils.PadLogName("controllers.K8sChannelReconciler")),
			Scheme:                 mgr.GetScheme(),
			NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.K8sNetwork, nwlib.Loopback},
		},
	}
	if err = channelReconciler.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup fallback channel controller")
		os.Exit(1)
	}

	baseReconciler := base.BaseNetworkReconciler{
		Client:    mgr.GetClient(),
		Log:       ctrl.Log.WithName(utils.PadLogName("controllers.BaseNetworkReconciler")),
		Scheme:    mgr.GetScheme(),
		Recorder:  mgr.GetEventRecorderFor("topology"),
		Namespace: nwlib.NamespaceForNi(string(nwlib.K8sNetwork)),
	}

	topology := nwc.TopologyReconciler{
		BaseNetworkReconciler: baseReconciler,
	}

	if err = topology.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup topology network reconciler")
		os.Exit(1)
	}

	setupLog.Info("starting k8s network operator")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running k8s network operator")
		os.Exit(1)
	}
}
