// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/node"
)

func main() {
	log.SetOutput(os.Stdout)
	log.Println("Starting node daemon")

	scheme := runtime.NewScheme()
	clientgoscheme.AddToScheme(scheme)
	crd.AddToScheme(scheme)

	var cl client.Client
	config, err := config.GetConfig()
	if err != nil {
		fmt.Println("Failed to get a kubernetes config, I hope you are just debugging.")
		cl = nil
	} else {
		cl, err = client.New(config, client.Options{Scheme: scheme})
		if err != nil {
			log.Fatalf("Failed to get kubernetes client: %v", err)
		}
	}

	initializer := &node.NodeInterfaceFinder{Client: cl, Scheme: scheme}
	if err := initializer.InitializeNodeInfo(); err != nil {
		log.Fatal(err)
	}

	// TODO: export network metrics via http here in a loop

	// Use a context for cancellation
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	stopch := make(chan struct{})
	nw, err := node.NewNetWatcher(&stopch)
	if err != nil {
		log.Fatalf("Failed to create NetWatcher: %v", err)
	}

	go nw.Run(&stopch)

	// Simulate waiting for an OS signal to terminate
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-sigs:
		log.Println("Received termination signal")
		cancel()
		close(stopch)
	case <-ctx.Done():
		log.Println("Context canceled")
	}

	// nw.Run(&stopch)
	// select {}
}
