// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"os"

	"go.uber.org/zap/zapcore"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	l2smv1 "siemens.com/qos-scheduler/scheduler/api/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	nwc "siemens.com/qos-scheduler/scheduler/network/topology-nwcontroller"
	"siemens.com/qos-scheduler/scheduler/utils"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup l2sm network controller")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(crd.AddToScheme(scheme))
	utilruntime.Must(l2smv1.AddToScheme(scheme))
}

func main() {
	var metricsAddr string
	var probeAddr string
	var networkImplementations string
	var isPhysical bool
	var channelController string
	var networkAttachmentName string
	var vlanId int
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.StringVar(&networkImplementations, "network-implementations", "", "Irrelevant for this implementation")
	flag.BoolVar(&isPhysical, "physical-network", false, "Irrelevant for this implementation")
	flag.StringVar(&channelController, "channel-controller", "", "Irrelevant for this implementation")
	flag.StringVar(&networkAttachmentName, "network-attachment-name", "", "Irrelevant for this implementation")
	flag.IntVar(&vlanId, "vlan-id", -1, "Irrelevant for this implementation")

	opts := zap.Options{
		Development: true,
		Level:       zapcore.InfoLevel,
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	// NewManager is an alias for manager.New
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme: scheme,
		Metrics: metricsserver.Options{
			BindAddress: metricsAddr,
		},
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager for l2sm network reconciler")
		os.Exit(1)
	}

	baseReconciler := base.BaseNetworkReconciler{
		Client:    mgr.GetClient(),
		Log:       ctrl.Log.WithName(utils.PadLogName("controllers.BaseNetworkReconciler")),
		Scheme:    mgr.GetScheme(),
		Recorder:  mgr.GetEventRecorderFor("topology"),
		Namespace: nwlib.NamespaceForNi("l2sm"),
	}
	setupLog.Info("starting l2sm network operator")
	topology := nwc.TopologyReconciler{
		BaseNetworkReconciler: baseReconciler,
	}

	if err = topology.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup l2sm network reconciler")
		os.Exit(1)
	}

	l2SMReconciler := chctrl.L2SMReconciler{
		ChannelReconciler: chctrl.ChannelReconciler{
			Client:                 mgr.GetClient(),
			Log:                    ctrl.Log.WithName(utils.PadLogName("controllers.L2SMReconciler")),
			Scheme:                 mgr.GetScheme(),
			NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.L2SMNetwork},
		},
	}
	if err = l2SMReconciler.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup l2sm reconciler")
		os.Exit(1)
	}

	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running topology network operator")
		os.Exit(1)
	}
}
