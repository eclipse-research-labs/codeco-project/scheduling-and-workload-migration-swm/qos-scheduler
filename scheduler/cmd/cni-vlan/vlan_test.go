// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"net"
	"testing"

	"github.com/vishvananda/netlink"
)

func TestCalculateAddresses(t *testing.T) {
	network := net.IPNet{
		IP:   net.ParseIP("10.10.0.0"),
		Mask: net.CIDRMask(29, 32),
	}
	gwip := "10.10.0.1"
	pod, gw, err := calculateAddresses(network, gwip)
	if err != nil {
		t.Errorf("Failed to calculate addresses: %v", err)
	}
	if pod.IP.String() != "10.10.0.2" {
		t.Errorf("expected pod address to be 10.10.0.2 but got %s", pod.IP.String())
	}
	if gw.IP.String() != gwip {
		t.Errorf("expected gw address to be %s but got %s", gwip, gw.IP.String())
	}

	gwip = "10.10.0.3"
	pod, gw, err = calculateAddresses(network, gwip)
	if err != nil {
		t.Errorf("Failed to calculate addresses: %v", err)
	}
	if pod.IP.String() != "10.10.0.4" {
		t.Errorf("expected pod address to be 10.10.0.4 but got %s", pod.IP.String())
	}
	if gw.IP.String() != gwip {
		t.Errorf("expected gw address to be %s but got %s", gwip, gw.IP.String())
	}

	gwip = "10.10.0.9"
	pod, gw, err = calculateAddresses(network, gwip)
	if err == nil {
		t.Errorf("gateway address %s is outside of network %q", gwip, network)
	}
}

func TestCalculateNodeRoutes(t *testing.T) {
	network := net.IPNet{
		IP:   net.ParseIP("10.10.0.0"),
		Mask: net.CIDRMask(29, 32),
	}
	pod, _, err := calculateAddresses(network, "10.10.0.1")
	if err != nil {
		t.Fatalf("unexpected address calculation failure: %v", err)
	}

	otherNodeIp := "192.168.12.2"

	// network, pod, ip of other node, vethIndex, subinterface
	addThese := calculateNodeRoutes(&network, pod, otherNodeIp, 2, 1)

	if len(addThese) != 2 {
		t.Errorf("expected to get two routes to add to the node but got %+v", addThese)
	}

	if addThese[0].LinkIndex != 2 {
		t.Errorf("first route to add on node should use bridge device but is using %d", addThese[0].LinkIndex)
	}
	if addThese[0].Dst.IP.String() != pod.IP.String() {
		t.Errorf("should add a route to pod %s to node but am adding %s", pod.IP.String(), addThese[0].Dst.IP.String())
	}
	if addThese[0].Scope != netlink.SCOPE_HOST {
		t.Errorf("route to pod from node should have host scope but has %q", addThese[0].Scope)
	}

	if addThese[1].LinkIndex != 1 {
		t.Errorf("second route to add on node should go via interface %d but is using %d", 1, addThese[1].LinkIndex)
	}
	if addThese[1].Dst.String() != network.String() {
		t.Errorf("second route to add should have destination %s but has %s",
			network.String(), addThese[1].Dst.String())
	}
	if addThese[1].Gw.String() != otherNodeIp {
		t.Errorf("second route to add to node should use gateway %s but is using %s",
			otherNodeIp, addThese[1].Gw.String())
	}
}

func TestCalculatePodRoutes(t *testing.T) {
	network := net.IPNet{
		IP:   net.ParseIP("10.10.0.0"),
		Mask: net.CIDRMask(29, 32),
	}
	pod, gw, err := calculateAddresses(network, "10.10.0.1")
	if err != nil {
		t.Fatalf("unexpected address calculation failure: %v", err)
	}

	deleteThese, addThese := calculatePodRoutes(&network, pod, gw, 2)

	if len(deleteThese) != 1 {
		t.Errorf("should delete one route from pod but got %v", deleteThese)
	}

	if len(addThese) != 2 {
		t.Errorf("should add two routes to pod but got %v", addThese)
	}

	if deleteThese[0].LinkIndex != 2 {
		t.Errorf("deleted route should be via veth device 2 but uses %d", deleteThese[0].LinkIndex)
	}
	if deleteThese[0].Dst.String() != network.String() {
		t.Errorf("deleted route should be for destination %s but is for %s",
			network.String(), deleteThese[0].Dst.String())
	}

	if addThese[0].LinkIndex != 2 {
		t.Errorf("route from pod to gateway should be via veth device 2 but uses %d",
			addThese[0].LinkIndex)
	}
	if addThese[0].Dst.IP.String() != gw.IP.String() {
		t.Errorf("first route added to pod should have destination %s but has %s",
			gw.IP.String(), addThese[0].Dst.IP.String())
	}
	if addThese[0].Src.String() != pod.IP.String() {
		t.Errorf("source address should be %s but is %s", pod.IP.String(), addThese[0].Src.String())
	}

	if addThese[1].LinkIndex != 2 {
		t.Errorf("route from pod to network should be via veth device 2 but uses %d",
			addThese[1].LinkIndex)
	}
	if addThese[1].Dst.String() != network.String() {
		t.Errorf("second route added to pod should have destination %s but has %s",
			network.String(), addThese[1].Dst.String())
	}
	if addThese[1].Src != nil {
		t.Errorf("source address should be nil but is %s", addThese[1].Src.String())
	}
	if addThese[1].Gw.String() != gw.IP.String() {
		t.Errorf("second route added to pod should use gateway %s but is using %s",
			gw.IP.String(), addThese[1].Gw.String())
	}
}
