// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func BuildInfrastructureModel(topologies []crd.NetworkTopology, nodes []corev1.Node, paths []crd.NetworkPath, links []crd.NetworkLink, scheduleOnMaster bool) (*assignment.Infrastructure, error) {
	ret := &assignment.Infrastructure{}

	// Determine the compute nodes based on which nodes exist in Kubernetes.
	modelNodes := getComputeNodeInfo(nodes, scheduleOnMaster)
	// Extend with network node information based on the links.
	ret.Nodes = getNetworkNodeInfo(links, modelNodes)

	var err error
	linksMap, mediaMap := getNetworkLinkInfo(links)
	allPaths, err := getNetworkPathInfo(paths, linksMap)
	if err != nil {
		return ret, err
	}
	for _, t := range topologies {
		var qos assignment.ServiceClass
		if t.Spec.NetworkImplementation == "K8S" {
			qos = assignment.ServiceClass_BEST_EFFORT
		} else {
			// TODO this needs to support service classes dynamically in the future
			qos = assignment.ServiceClass_ASSURED
		}
		network := &assignment.Network{}
		ret.Networks = append(ret.Networks, network)
		network.Id = t.Name
		network.Qos = qos
		network.Links = linksMap[t.Spec.NetworkImplementation]
		network.Media = mediaMap[t.Spec.NetworkImplementation]
		network.Paths = allPaths[t.Spec.NetworkImplementation]
	}
	return ret, err
}

func getNetworkPathInfo(paths []crd.NetworkPath, linksMap map[string][]*assignment.Network_Link) (
	map[string][]*assignment.Network_Path, error) {

	ret := make(map[string][]*assignment.Network_Path)

	for _, p := range paths {
		ni, err := nwlib.NiFromNamespace(p.Namespace)
		if err != nil {
			return ret, fmt.Errorf("cannot find ni tag for namespace %s of path %s", p.Namespace, p.Name)
		}
		pathsForNi, found := ret[ni]
		if !found {
			pathsForNi = make([]*assignment.Network_Path, 0)
		}
		ret[ni] = append(pathsForNi, &assignment.Network_Path{
			Id:    p.Name,
			Links: p.Spec.Links,
		})
	}
	return ret, nil
}

func getNetworkLinkInfo(links []crd.NetworkLink) (map[string][]*assignment.Network_Link, map[string][]*assignment.Medium) {
	linkMap := make(map[string][]*assignment.Network_Link, 0)
	mediaMap := make(map[string][]*assignment.Medium, 0)
	for _, l := range links {
		networkImplementation := nwlib.NetworkLinkClass(l)
		niName := string(networkImplementation)
		var exists bool
		_, exists = linkMap[niName]
		if !exists {
			linkMap[niName] = make([]*assignment.Network_Link, 0)
			mediaMap[niName] = make([]*assignment.Medium, 0)
		}
		linkMap[niName] = append(linkMap[niName], &assignment.Network_Link{
			Id:      l.Name,
			Source:  l.Spec.LinkFrom.Name,
			Target:  l.Spec.LinkTo.Name,
			Latency: l.Spec.LatencyNanos,
			Medium:  l.Name,
		})
		mediaMap[niName] = append(mediaMap[niName], &assignment.Medium{
			Id: l.Name,
			Bandwidth: &assignment.Resource{
				Capacity: l.Spec.BandWidthBits,
			},
		})
	}
	return linkMap, mediaMap
}
