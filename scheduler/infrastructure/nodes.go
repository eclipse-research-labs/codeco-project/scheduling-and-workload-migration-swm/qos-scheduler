// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package infrastructure contains code dealing with nodes, links, and channels.
package infrastructure

import (
	corev1 "k8s.io/api/core/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"strings"
)

// IsWorkloadLabel returns true if the parameter is a workload label.
func IsWorkloadLabel(lb string) bool {
	return strings.HasPrefix(lb, "siemens.com.qosscheduler.")
}

func AddNamespaceToLabel(lb string) string {
	if IsWorkloadLabel(lb) {
		return lb
	}
	return "siemens.com.qosscheduler." + lb
}

// This returns just the label keys because that's what the optimizer uses.
func workloadLabelsFromNodeLabels(nodeLabels map[string]string) []string {
	ret := make([]string, 0, len(nodeLabels))
	for key := range nodeLabels {
		if IsWorkloadLabel(key) {
			ret = append(ret, key)
		}
	}
	return ret
}

// GetNodeInfo translates the cluster's nodes into
// a format for the optimizer.
func getComputeNodeInfo(nodes []corev1.Node, scheduleOnMaster bool) []*assignment.Node {
	ret := make([]*assignment.Node, 0, len(nodes))
	for _, node := range nodes {
		// Skip nodes that aren't running. The Phase can be an empty string,
		// so this doesn't just compare against NodeRunning.
		if node.Status.Phase == corev1.NodePending || node.Status.Phase == corev1.NodeTerminated {
			continue
		}
		if !scheduleOnMaster {
			// Skip the control-plane node unless scheduleOnMaster is true.
			// Other taints should be implemented using pod and node labels.
			taints := node.Spec.Taints
			skipNode := false
			for _, t := range taints {
				if t.Key == "node-role.kubernetes.io/master" && t.Effect == "NoSchedule" {
					skipNode = true
					break
				}
			}
			if skipNode {
				continue
			}
		}
		capacity := node.Status.Capacity
		info := &assignment.Node{
			Id:       node.Name,
			NodeType: assignment.Node_COMPUTE,
			Labels:   workloadLabelsFromNodeLabels(node.Labels),
			// For cpu, this should be millicpu, for memory, it should be in bytes.
			CPU: &assignment.Resource{
				Capacity: capacity.Cpu().MilliValue(),
			},
			Memory: &assignment.Resource{
				Capacity: capacity.Memory().Value(),
			},
		}
		ret = append(ret, info)
	}
	return ret
}

// GetNetworkNodeInfo determines which network nodes need to exist based on the links we know about.
// Basically, every endpoint of a link that isn't a compute node has to be a network node.
// Returns a list of compute nodes and network nodes.
func getNetworkNodeInfo(links []crd.NetworkLink,
	computeNodes []*assignment.Node) []*assignment.Node {

	nodemap := make(map[string]*assignment.Node)
	for _, node := range computeNodes {
		nodemap[node.Id] = node
	}

	counter := 0
	for _, link := range links {
		if nodemap[link.Spec.LinkFrom.Name] == nil {
			nodemap[link.Spec.LinkFrom.Name] = &assignment.Node{
				Id:       link.Spec.LinkFrom.Name,
				NodeType: assignment.Node_NETWORK,
			}
			counter++
		}
		if nodemap[link.Spec.LinkTo.Name] == nil {
			nodemap[link.Spec.LinkTo.Name] = &assignment.Node{
				Id:       link.Spec.LinkTo.Name,
				NodeType: assignment.Node_NETWORK,
			}
			counter++
		}
	}
	ret := make([]*assignment.Node, len(computeNodes)+counter)
	i := 0
	for _, node := range nodemap {
		ret[i] = node
		i++
	}
	return ret
}
