<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Scheduler Plugin

This is the plugin code for the custom scheduler extension to do QoS-aware and network-aware scheduling.

## How to debug QoS Scheduler in Visual Studio Code (VSC) live in k8s cluster

Debugging the customer scheduler- (qos-scheduler Pod) in a k8s target environment works in principle similar to all other containers/Pods of the QoS-Scheduler project. There is however the difference, that you must provide a configuration file for the qos-scheduler, and that this needs to contain a section where to access the kubeconfig file of the target cluster. Follow these steps:

- deploy the complete QoS-Scheduler project in the k8s target cluster (using the helm charts) 
- run the QoS-Scheduler project in VSC in the .devcontainer
- select the go file with the main() function of the qos-scheduler container (scheduler/schedulerplugins/main/app.go)
- select "Run and Debug" in VSC
- add configuration for the container to launch.json
    ```
    "configurations": [
        {
            "name": "qos-scheduler",
            "type": "go",
            "request": "launch",
            "mode": "debug",
            "program": "${workspaceFolder}/scheduler/schedulerplugins/main/app.go",
            "args": ["--bind-address=0.0.0.0", "-v=10", "--config=${workspaceFolder}/tmp/scheduler-config.yaml"],
        }        
    ]
    ```
- create a `scheduler-config.yaml` file and put it to a place accessible by the VSC .devcontainer (in this example, we used tmp/scheduler-config.yaml) and reference that file in the launch.json entry above. You have to extend the usual `KubeSchedulerConfiguration` by an entry for the kubeconfig file to access the k8s cluster (under `clientConnection`). The `scheduler-config.yaml` file looks like this:
```
apiVersion: kubescheduler.config.k8s.io/v1
kind: KubeSchedulerConfiguration
leaderElection:
  leaderElect: false
clientConnection:
  kubeconfig: "<kubecconfig file for your k8s cluster>"
profiles:
- schedulerName: qos-scheduler
  plugins:
    filter:
      enabled:
      - name: QosScheduler
    preScore:
      enabled:
      - name: QosScheduler
    score:
      enabled:
      - name: QosScheduler
        weight: 100
    permit:
      enabled:
      - name: QosScheduler
    preBind:
      enabled:
      - name: QosScheduler
    bind:
      # Have to remove the DefaultBinder first and then add it after the QosScheduler.
      # This is so that pods not handled by the QosScheduler get bound normally.
      disabled:
      - name: DefaultBinder
      enabled:
      - name: QosScheduler
      - name: DefaultBinder
    postBind:
      enabled:
      - name: QosScheduler
  pluginConfig:
  - name: QosScheduler
    args: {"master": "master", "kubeconfig": "kubeconfig"}
```
- before running the scheduler in the VSC debugger, stop the qos-scheduler Pod in the target cluster
  - the simplest way to do that is to scale down the replicas in the qos-scheduler deployment to 0
- Press "Start Debugging" (F5)