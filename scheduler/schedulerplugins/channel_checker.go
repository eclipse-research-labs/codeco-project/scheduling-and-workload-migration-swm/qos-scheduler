// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package schedulerplugins

import (
	"context"
	"fmt"
	"time"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	"k8s.io/klog/v2"
	"k8s.io/kubernetes/pkg/scheduler/framework"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	channelStatusCheckIntervalSeconds = 5
)

type podChannelStatusChecker struct {
	frameworkHandle framework.Handle
	k8sClient       client.Client
	pluginName      string
	ctx             context.Context
}

// podKey returns the internal name of the pod
func podKey(podname string, namespace string) string {
	return fmt.Sprintf("%s/%s", namespace, podname)
}

func NewPodChannelStatusChecker(handle framework.Handle, k8sClient client.Client, pluginName string) *podChannelStatusChecker {
	p := podChannelStatusChecker{
		frameworkHandle: handle,
		k8sClient:       k8sClient,
		pluginName:      pluginName,
		ctx:             context.Background(),
	}
	go func() {
		podChannelStatusTicker := time.NewTicker(time.Second * channelStatusCheckIntervalSeconds)
		for range podChannelStatusTicker.C {
			p.frameworkHandle.IterateOverWaitingPods(func(waitingPod framework.WaitingPod) {
				klog.V(3).Infof("is pod %s ready now?", waitingPod.GetPod().Name)
				p.checkPodStatus(waitingPod)
			})
		}
	}()
	return &p
}

// See if the pod can be removed from the waiting list.
func (p *podChannelStatusChecker) checkPodStatus(waitingPod framework.WaitingPod) {
	status := p.getChannelStatus(waitingPod.GetPod())
	klog.V(3).Infof("pod %s now has channel status %v", waitingPod.GetPod().Name, *status)
	if status.Code() == framework.Success {
		waitingPod.Allow(p.pluginName)
	} else if status.Code() == framework.UnschedulableAndUnresolvable {
		waitingPod.Reject(p.pluginName, status.Message())
	}
}

func (p *podChannelStatusChecker) getAssignmentPlanList(pod *v1.Pod) (*crd.AssignmentPlanList, error) {
	apList := &crd.AssignmentPlanList{}
	listOptions := []client.ListOption{
		client.InNamespace(pod.Namespace),
		client.MatchingLabels{"application-group": pod.Labels["application-group"]},
	}
	err := p.k8sClient.List(p.ctx, apList, listOptions...)
	return apList, err
}

func (p *podChannelStatusChecker) getChannels(pod *v1.Pod) ([]string, *framework.Status) {
	apList, err := p.getAssignmentPlanList(pod)
	if err != nil {
		return []string{}, framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Error getting assignment plan list: %v\n", err))
	}
	for idx := range apList.Items {
		if apStatus, ok := apList.Items[idx].Status.PodStatus[podKey(pod.Name, pod.Namespace)]; ok {
			if ok {
				return apStatus.Channels, framework.NewStatus(framework.Success, "")
			}
		}
	}
	return []string{}, framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Missing schedule plan entry for pod %s/%s", pod.Name, pod.Namespace))
}

func (p *podChannelStatusChecker) getChannelStatus(pod *v1.Pod) *framework.Status {
	channelIDs, status := p.getChannels(pod)
	if len(channelIDs) == 0 {
		return status
	}
	ooqos := false
	rejected := false
	allAck := true
	hasL2sm := false
	var channel crd.Channel
	for _, channelID := range channelIDs {
		err := p.k8sClient.Get(p.ctx, types.NamespacedName{Namespace: pod.Namespace, Name: channelID}, &channel)
		if err == nil {
			channelStatus := infrastructure.EvaluateChannel(&channel)
			if channelStatus.ChannelStatus != crd.ChannelRunning {
				allAck = false
			}
			if infrastructure.ChannelImplementation(&channel) == "L2SM" {
				klog.Info("QoS-Scheduler Permit: The Pod has an L2SM channel")
				hasL2sm = true
			}
			if channelStatus.ChannelStatus == crd.ChannelRejected {
				rejected = true
				break
			}
			if channelStatus.ChannelStatus == crd.ChannelOOQOS {
				ooqos = true
				break
			}
		} else {
			allAck = false
		}
	}
	if ooqos {
		// This is theoretically fixable if the channel recovers, but probably not.
		return framework.NewStatus(framework.Unschedulable, "Channel out of qos")
	}
	if rejected {
		return framework.NewStatus(framework.Unschedulable, "Channel rejected")
	}
	if allAck {
		// All channels are good, let the Pod schedule.

		// TO DO: This is a workaround for the l2sm network. Remove it when l2sm error is fixed.
		// Workaround: We set the label "l2sm" = "true" here, only after all channels for this pod are running,
		// and thus all network annotations "/l2sm/network" for this Pod are written
		if hasL2sm {
			// Retry logic to handle concurrent updates
			retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
				// Get the latest version of the Pod
				latestPod := &v1.Pod{}
				if err := p.k8sClient.Get(p.ctx, types.NamespacedName{Name: pod.Name, Namespace: pod.Namespace}, latestPod); err != nil {
					return err
				}
				// Update the label
				if latestPod.Labels == nil {
					latestPod.Labels = make(map[string]string)
				}
				latestPod.Labels["l2sm"] = "true"
				return p.k8sClient.Update(p.ctx, latestPod)
			})

			if retryErr != nil {
				klog.Errorf("Could not write l2sm label to the pod %s: %v", pod.Name, retryErr)
				return framework.NewStatus(framework.Error, "Failed to update pod with l2sm label")
			}
		}
		// TO DO: End of workaround

		return framework.NewStatus(framework.Success, "")
	}

	// Not all channels are ACK yet, but none have definitely failed. Wait.
	return framework.NewStatus(framework.Wait, "Pod waiting for channels to ACK")
}
