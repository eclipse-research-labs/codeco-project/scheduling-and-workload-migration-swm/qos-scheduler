// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// GetApplicationModel builds an assignment.AppGroup containing compute requirements derived
// from the applications passed in, as well as channels derived from the workload channels.
func GetApplicationModel(apps []qosschedulerv1alpha1.Application) *assignment.ApplicationGroup {
	return &assignment.ApplicationGroup{
		Applications: getComputeRequirements(apps),
		Channels:     getChannels(apps),
	}
}

func GetCostModel(apps []qosschedulerv1alpha1.Application) *assignment.Cost {
	return &assignment.Cost{
		WorkloadCosts: getWorkloadCosts(apps),
	}
}

func GetRecommendationModel(apps []qosschedulerv1alpha1.Application) *assignment.Recommendation {
	workloadRecommendations := []*assignment.Recommendation_Workload{}
	for _, app := range apps {
		for _, wl := range app.Spec.Workloads {
			nodes := []*assignment.Recommendation_Info{}
			for node, val := range wl.NodeRecommendations {
				nodes = append(nodes, &assignment.Recommendation_Info{
					Node:  node,
					Value: val,
				})
			}
			workloadRecommendations = append(workloadRecommendations, &assignment.Recommendation_Workload{
				Application: app.Name,
				Workload:    GetWorkloadName(app.Name, wl.Basename),
				Nodes:       nodes,
			})
		}
	}
	return &assignment.Recommendation{
		Workload: workloadRecommendations,
	}
}

// GetExpectedChannels identifies those channels that originate or terminate at a given application.
// Input parameters: an application and a list of channels.
// Returns: a list of channels originating at the application, and a list of channels terminating at the application.
func GetExpectedChannels(app qosschedulerv1alpha1.Application, channels []qosschedulerv1alpha1.Channel) (map[string]([]qosschedulerv1alpha1.Channel), map[string]([]qosschedulerv1alpha1.Channel)) {

	channelsFromPods := make(map[string]([]qosschedulerv1alpha1.Channel))
	channelsToPods := make(map[string]([]qosschedulerv1alpha1.Channel))

	for _, s := range channels {
		ta := infrastructure.ChannelTargetApplication(&s)
		if ta == app.Name {
			targetPodName := s.Spec.TargetWorkload
			if channelsToPods[targetPodName] == nil {
				channelsToPods[targetPodName] = []qosschedulerv1alpha1.Channel{s}
			} else {
				channelsToPods[targetPodName] = append(channelsToPods[targetPodName], s)
			}
		}

		src := infrastructure.ChannelSourceApplication(&s)
		if src == app.Name {
			sourcePodName := s.Spec.SourceWorkload
			if channelsFromPods[sourcePodName] == nil {
				channelsFromPods[sourcePodName] = []qosschedulerv1alpha1.Channel{s}
			} else {
				channelsFromPods[sourcePodName] = append(channelsFromPods[sourcePodName], s)
			}
		}
	}
	return channelsFromPods, channelsToPods
}

func getAppChannels(app qosschedulerv1alpha1.Application) []*assignment.Channel {
	ret := make([]*assignment.Channel, 0)
	for _, wl := range app.Spec.Workloads {
		sourceWlName := GetWorkloadName(app.Name, wl.Basename)
		for _, ch := range wl.Channels {
			targetWlName := GetWorkloadName(ch.OtherWorkload.ApplicationName, ch.OtherWorkload.Basename)
			bandwidth, err := nwlib.GetQuantityValue(ch.MinBandwidth)
			if err != nil {
				fmt.Printf("failed to convert bandwidth spec to number: %v\n", err)
				continue
			}
			latency, err := nwlib.GetQuantityNanoValue(ch.MaxDelay)
			if err != nil {
				fmt.Printf("failed to convert latency spec to number: %v\n", err)
				continue
			}
			framesize, err := nwlib.GetQuantityValue(ch.Framesize)
			if err != nil {
				fmt.Printf("failed to convert Framesize spec %v to number: %v\n", ch.Framesize, err)
				continue
			}
			interfaceBasename := GetChannelName(ch.Basename, sourceWlName, targetWlName)

			// TODO how to make this more dynamic
			var qos assignment.ServiceClass
			if ch.ServiceClass == "ASSURED" {
				qos = assignment.ServiceClass_ASSURED
			} else if ch.ServiceClass == "BESTEFFORT" {
				qos = assignment.ServiceClass_BEST_EFFORT
			} else {
				// This is too noisy in the tests. We need a better solution for this
				//fmt.Printf("Unknown service class: %s\n", ch.ServiceClass)
				qos = assignment.ServiceClass_BEST_EFFORT
			}
			spec := &assignment.Channel{
				SourceWorkload:    sourceWlName,
				TargetWorkload:    targetWlName,
				SourceApplication: app.Name,
				TargetApplication: ch.OtherWorkload.ApplicationName,
				Bandwidth:         bandwidth,
				Latency:           latency,
				FrameSize:         int32(framesize),
				Id:                interfaceBasename,
				Qos:               qos,
				TargetPort:        int32(ch.OtherWorkload.Port),
			}
			ret = append(ret, spec)
		}
	}
	return ret
}

func getWorkloadCosts(apps []qosschedulerv1alpha1.Application) []*assignment.Cost_Workload {
	ret := make([]*assignment.Cost_Workload, 0)
	for _, app := range apps {
		for _, wl := range app.Spec.Workloads {
			for node, cost := range wl.Costs {
				ret = append(ret, &assignment.Cost_Workload{
					Cost:        cost,
					Node:        node,
					Application: app.Name,
					Workload:    wl.Basename,
				})
			}
		}
	}
	return ret
}

func getChannels(apps []qosschedulerv1alpha1.Application) []*assignment.Channel {
	ret := make([]*assignment.Channel, 0, len(apps))
	for _, app := range apps {
		if IsEligibleForScheduling(&app) {
			ret = append(ret, getAppChannels(app)...)
		}
	}
	return ret
}

// Get a list of application protos for the optimizer.
func getComputeRequirements(apps []qosschedulerv1alpha1.Application) []*assignment.Application {
	ret := make([]*assignment.Application, 0, len(apps))
	for _, app := range apps {
		if IsEligibleForScheduling(&app) {
			ret = append(ret, getAppComputeRequirements(app))
		}
	}
	return ret
}

// Transform an Applications resource into an Applications proto for the optimizer.
func getAppComputeRequirements(app qosschedulerv1alpha1.Application) *assignment.Application {
	ret := &assignment.Application{
		Id:        app.Name,
		Workloads: make([](*assignment.Workload), 0, len(app.Spec.Workloads)),
	}

	for _, wl := range app.Spec.Workloads {
		wlName := fmt.Sprintf("%s-%s", app.Name, wl.Basename)

		podspec := wl.Template
		totalMilliCPU := int32(0)
		totalMemoryBytes := int64(0)
		for _, cont := range podspec.Spec.Containers {
			r := cont.Resources // this has type ResourceRequirements
			totalMilliCPU += int32(r.Requests.Cpu().MilliValue())
			totalMemoryBytes += int64(r.Requests.Memory().Value())
		}
		// Noderesources/fit actually takes the max over the init containers, maybe
		// do that here too.
		for _, cont := range podspec.Spec.InitContainers {
			r := cont.Resources // this has type ResourceRequirements
			totalMilliCPU += int32(r.Requests.Cpu().MilliValue())
			totalMemoryBytes += int64(r.Requests.Memory().Value())
		}
		labels, forbiddenLabels, _ := GetLabels(wl)
		workload := &assignment.Workload{
			Id:              wlName,
			RequiredLabels:  labels,
			ForbiddenLabels: forbiddenLabels,
			CPU:             totalMilliCPU,
			Memory:          totalMemoryBytes,
		}
		ret.Workloads = append(ret.Workloads, workload)
	}
	return ret
}

func GetWorkloadName(applicationName string, workloadBasename string) string {
	return fmt.Sprintf("%s-%s", applicationName, workloadBasename)
}

func GetChannelName(channelBasename string, sourceWorkloadName string, targetWorkloadName string) string {
	if channelBasename != "" {
		return channelBasename
	} else {
		return fmt.Sprintf("%s-to-%s", sourceWorkloadName, targetWorkloadName)
	}
}
