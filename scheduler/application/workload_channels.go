// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// channelsForWorkloadChannels returns an array containing Channel CRs for all WorkloadChannels in an ApplicationGroup.
// Information about the Channels are passed in the qosImplementationMap according to the AssignmentPlan from the solver.
func channelsForWorkloadChannels(workloadChannels []*assignment.Channel, paths []*assignment.Assignment_Channel, namespace string, qosImplementationMap map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass)) ([]*crd.Channel, error) {
	channels := make([]*crd.Channel, 0, 2*len(workloadChannels))
	pathMap := make(map[string]*assignment.Assignment_Channel)

	for _, p := range paths {
		pathMap[p.Channel] = p
	}

	for _, chann := range workloadChannels {
		path, ok := pathMap[chann.Id]
		if !ok {
			if len(paths) > 0 {
				fmt.Printf("missing a path assignment for channel %s\n", chann.Id)
			}
			// TODO: else case logging that there are no path assignments in the optimizer reply?
		}
		podFrom := chann.SourceWorkload
		podTo := chann.TargetWorkload
		networkServiceClass := crd.ServiceClassBestEffort
		var networkImplementation nwlib.NetworkImplementationClass
		if chann.Qos == assignment.ServiceClass_ASSURED {
			networkServiceClass = crd.ServiceClassAssured
		}
		ni, ok := qosImplementationMap[crd.NetworkServiceClass(networkServiceClass)]
		if ok {
			networkImplementation = ni
		}
		var channelpath crd.ChannelPathRef
		if path != nil {
			channelpath.Name = path.Path
			channelpath.Namespace = nwlib.NamespaceForNi(string(networkImplementation))
		}
		s := &crd.Channel{
			ObjectMeta: metav1.ObjectMeta{
				Name:      chann.Id,
				Namespace: namespace,
				Labels: map[string]string{"application": chann.SourceApplication,
					"to-application": chann.TargetApplication,
					crd.NetworkLabel: string(networkImplementation),
				},
			},
			Spec: crd.ChannelSpec{
				SourceWorkload:   podFrom,
				TargetWorkload:   podTo,
				ChannelPath:      channelpath,
				MinBandwidthBits: chann.Bandwidth,
				MaxDelayNanos:    chann.Latency,
				Framesize:        chann.FrameSize,
				SendInterval:     chann.SendInterval,
				ServiceClass:     crd.NetworkServiceClass(networkServiceClass),
				TargetPort:       chann.TargetPort,
			},
		}
		infrastructure.CompleteChannelSpec(s)
		channels = append(channels, s)
	}
	return channels, nil
}

func ChannelsForApplication(app crd.Application, qosMap map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass)) ([]*crd.Channel, error) {
	workloadChannels := getAppChannels(app)
	return channelsForWorkloadChannels(workloadChannels, []*assignment.Assignment_Channel{}, app.Namespace, qosMap)
}
