// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// IsGroupPhaseChangePermitted returns true if an application group may
// change from oldPhase to newPhase.
func IsGroupPhaseChangePermitted(oldPhase, newPhase crd.ApplicationGroupPhase) bool {
	if oldPhase == newPhase {
		return true
	}
	if newPhase == "" {
		return false
	}
	switch oldPhase {
	case "":
		return true
	case crd.ApplicationGroupWaiting:
		return newPhase == crd.ApplicationGroupOptimizing
	case crd.ApplicationGroupOptimizing:
		return newPhase == crd.ApplicationGroupScheduling ||
			newPhase == crd.ApplicationGroupWaiting ||
			newPhase == crd.ApplicationGroupFailed
	case crd.ApplicationGroupScheduling:
		return newPhase == crd.ApplicationGroupWaiting ||
			newPhase == crd.ApplicationGroupFailed
	case crd.ApplicationGroupFailed:
		return false
	}
	// Shouldn't get here.
	return false
}

// IsGroupReady decides whether there are enough ready applications to satisfy
// the group's co-scheduling condition.
func IsGroupReady(appGroup *crd.ApplicationGroup, applications []crd.Application) bool {

	// Count applications by phase.
	countByPhase := make(map[crd.ApplicationPhase]int)
	for _, app := range applications {
		phase := app.Status.Phase
		countByPhase[phase]++
	}

	// If no applications are waiting, then there is nothing to do.
	// This doesn't technically mean the group isn't ready, it's just an
	// optimization.
	// Failed applications go to "Waiting" when all their pods have terminated,
	// so this will pick them up at that time.
	if countByPhase[crd.ApplicationWaiting] == 0 {
		return false
	}

	// If an application has failed, it needs to do some cleanup before it
	// goes back to 'Waiting'. The application group should wait for that to be done.
	if countByPhase[crd.ApplicationFailing] > 0 {
		return false
	}

	// Count applications in waiting, scheduling, pending, running.
	readyApplicationCount := countByPhase[crd.ApplicationWaiting] +
		countByPhase[crd.ApplicationScheduling] +
		countByPhase[crd.ApplicationPending] +
		countByPhase[crd.ApplicationRunning]

	// For now, just compare this count against MinMember.
	return readyApplicationCount >= int(appGroup.Spec.MinMember)
}

func NewPhaseFromApplications(current crd.ApplicationGroupPhase,
	apps []crd.Application) crd.ApplicationGroupPhase {

	if current != crd.ApplicationGroupScheduling {
		return current
	}

	for _, app := range apps {
		if app.Status.Phase == crd.ApplicationScheduling {
			// Stay in Scheduling until the applications are at least pending
			return crd.ApplicationGroupScheduling
		}
	}
	return crd.ApplicationGroupWaiting
}
