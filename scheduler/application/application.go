// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package application has utility code for dealing with applications.
package application

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

func IsEligibleForScheduling(app *crd.Application) bool {
	return app.Status.Phase == crd.ApplicationWaiting
}

// IsPhaseChangePermitted returns true if the phase change is a legal one in
// the application state model, false otherwise.
func IsPhaseChangePermitted(oldPhase, newPhase crd.ApplicationPhase) bool {
	// A no-op change is always ok.
	if oldPhase == newPhase {
		return true
	}

	if newPhase == "" {
		return false
	}

	// This is initialization.
	if oldPhase == "" {
		return true
	}

	switch oldPhase {
	case crd.ApplicationWaiting:
		return newPhase == crd.ApplicationScheduling
	case crd.ApplicationScheduling:
		return newPhase == crd.ApplicationPending
	case crd.ApplicationPending:
		return newPhase == crd.ApplicationRunning ||
			newPhase == crd.ApplicationFailing
	case crd.ApplicationRunning:
		return newPhase == crd.ApplicationFailing ||
			newPhase == crd.ApplicationWaiting ||
			newPhase == crd.ApplicationPending
	case crd.ApplicationFailing:
		return newPhase == crd.ApplicationWaiting
	}
	return false
}

// NewStatusFromWorkloads computes the phase that the application would be in
// based on the phases of its workloads.
func NewStatusFromWorkloads(pods *corev1.PodList) *crd.ApplicationStatus {
	phaseMap := make(map[corev1.PodPhase]int)

	for _, pod := range pods.Items {
		if pod.Status.Phase == corev1.PodFailed {
			return &crd.ApplicationStatus{
				Phase:      crd.ApplicationFailing,
				Conditions: []crd.ApplicationCondition{*DescribePodFailure(pod)},
			}
		}
		phaseMap[pod.Status.Phase]++
	}

	if phaseMap[corev1.PodPending] > 0 {
		return &crd.ApplicationStatus{Phase: crd.ApplicationPending}
	}
	if phaseMap[corev1.PodSucceeded] == len(pods.Items) {
		return &crd.ApplicationStatus{Phase: crd.ApplicationWaiting}
	}
	return &crd.ApplicationStatus{Phase: crd.ApplicationRunning}
}

func MakeApplicationCondition(conditionType crd.ApplicationConditionType,
	entity string, reason string) *crd.ApplicationCondition {
	return &crd.ApplicationCondition{
		Type: conditionType, LastUpdateTime: metav1.Now(), Entity: entity, Reason: reason, Status: corev1.ConditionTrue,
	}
}

// DescribePodFailure takes a pod (presumed to have failed) and returns an ApplicationCondition
// with a reason for why the pod failed. This condition can be evaluated when a new optimizer
// request is made.
func DescribePodFailure(pod corev1.Pod) *crd.ApplicationCondition {
	if pod.Status.Phase != corev1.PodFailed {
		return nil
	}
	for _, cond := range pod.Status.Conditions {
		if cond.Status == corev1.ConditionTrue {
			if cond.Type == corev1.PodReady {
				return nil
			}
		} else if cond.Status == corev1.ConditionFalse {
			if cond.Type == corev1.PodScheduled {
				// the pod hasn't been scheduled yet. This doesn't have
				// to be final but the assumption here is that the pod has
				// already been determined to have failed when this function
				// is called.
				return MakeApplicationCondition(
					crd.ApplicationPodUnschedulable, pod.Name, "The pod could not be scheduled")
			} else if cond.Type == corev1.ContainersReady {
				for _, cs := range pod.Status.ContainerStatuses {
					if cs.Ready || cs.State.Running != nil {
						continue
					}
					if cs.State.Terminated != nil {
						return MakeApplicationCondition(
							crd.ApplicationPodFailed, pod.Name,
							fmt.Sprintf("container %s for pod %s terminated: exit code %d, signal %d, message %s, reason %s", cs.ContainerID, pod.Name, cs.State.Terminated.ExitCode,
								cs.State.Terminated.Signal, cs.State.Terminated.Message,
								cs.State.Terminated.Reason))
					}
				}
				return MakeApplicationCondition(
					crd.ApplicationPodFailed, pod.Name,
					fmt.Sprintf("not all containers for pod %s are ready", pod.Name))
			} else if cond.Type == corev1.PodInitialized {
				for _, cs := range pod.Status.InitContainerStatuses {
					if cs.Ready {
						continue
					}
					if cs.State.Terminated == nil {
						return MakeApplicationCondition(crd.ApplicationNotRunning, pod.Name,
							fmt.Sprintf("init container %s for pod %s has not started", cs.ContainerID, pod.Name))
					}
				}
				return MakeApplicationCondition(crd.ApplicationNotRunning, pod.Name,
					fmt.Sprintf("pod %s has not started: %s", pod.Name, pod.Status.Message))
			}
		}
	}
	return MakeApplicationCondition(crd.ApplicationPodFailed, pod.Name,
		fmt.Sprintf("no running container found for pod %s. message: %s, reason %s",
			pod.Name, pod.Status.Message, pod.Status.Reason))
}

// GetPodSpecs obtains the pod template specs for the application's workloads.
func GetPodSpecs(app crd.Application) map[string]crd.QosPodTemplateSpec {
	ret := make(map[string]crd.QosPodTemplateSpec)
	for _, wl := range app.Spec.Workloads {
		podName := fmt.Sprintf("%s-%s", app.Name, wl.Basename)
		ret[podName] = wl.Template
	}
	return ret
}

// GetMissingPodSpecs compares the application's workload specs against a list
// of existing pods. It returns a list of those pod specs that aren't in the list
// of running pods. These could be pods that have been deleted in Kubernetes or
// they could be pods that have been added to the application spec.
func GetMissingPodSpecs(app crd.Application, existingPods corev1.PodList) map[string]crd.QosPodTemplateSpec {
	existing := make(map[string]bool)
	for _, p := range existingPods.Items {
		// Assume existingPods all have app as their owner, so no need to check
		// the namespace in addition to the name.
		existing[p.Name] = true
	}
	expectedPods := GetPodSpecs(app)
	ret := make(map[string]crd.QosPodTemplateSpec)
	for name, expected := range expectedPods {
		if !existing[name] {
			ret[name] = expected
		}
	}
	return ret
}

// LabelValidationFailure is a code for why a pod's affinity settings cannot
// be turned into required/forbidden labels for the optimizer.
type LabelValidationFailure int

const (
	OK LabelValidationFailure = iota
	POD_AFFINITY
	MULTIPLE_NODE_SELECTOR_TERMS
	MATCH_FIELDS_IN_NODE_SELECTOR
	UNSUPPORTED_OPERATOR_IN_NODE_SELECTOR
	NOT_A_WORKLOAD_LABEL
)

func (f LabelValidationFailure) String() string {
	return []string{
		"",
		"pod affinity is not supported",
		"multiple node selector terms are not supported",
		"match fields in the node selector are not supported",
		"only the Exists and DoesNotExist operators are supported",
		"skipping non-workload labels",
	}[f]
}

// ValidateLabels validates the label-related settings on a workload spec.
// Returns a list of validation failures found; empty list if everything is ok.
// Labels can be extracted even if there are failures, the failures are just for
// informational logging.
// The only supported affinity settings are:
// NodeAffinity with RequiredDuringSchedulingIgnoredDuringExecution, exactly one
// NodeSelectorTerm. In the NodeSelectorTerm, MatchFields are not supported
// but you can have one or more MatchExpressions. Only the "Exists" and "DoesNotExist"
// operators in MatchExpressions are supported.
func ValidateLabels(wl crd.ApplicationWorkloadSpec) []LabelValidationFailure {
	validation := make([]LabelValidationFailure, 0, 4)

	if affinity := wl.Template.Spec.Affinity; affinity != nil {
		if affinity.PodAffinity != nil || affinity.PodAntiAffinity != nil {
			validation = append(validation, POD_AFFINITY)
		}
		if nodeAffinity := affinity.NodeAffinity; nodeAffinity != nil {
			if selector := nodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution; selector != nil {
				// More than one node selector term means they'll be OR'ed together, so the
				// pod can schedule if the node satisfies at least one of the terms.
				// That's not part of the optimizer API.
				if len(selector.NodeSelectorTerms) != 1 {
					validation = append(validation, MULTIPLE_NODE_SELECTOR_TERMS)
				}
				term := selector.NodeSelectorTerms[0]
				// The term can contain multiple MatchExpressions, which will be AND'ed together.
				// So each of them becomes a label.
				// However, MatchFields isn't supported.
				if len(term.MatchFields) > 0 {
					validation = append(validation, MATCH_FIELDS_IN_NODE_SELECTOR)
				}
				for _, req := range term.MatchExpressions {
					if !infrastructure.IsWorkloadLabel(req.Key) {
						validation = append(validation, NOT_A_WORKLOAD_LABEL)
						continue
					}
					if req.Operator != corev1.NodeSelectorOpExists && req.Operator != corev1.NodeSelectorOpDoesNotExist {
						validation = append(validation, UNSUPPORTED_OPERATOR_IN_NODE_SELECTOR)
					}
				}
			}
		}
	}
	return validation
}

// GetLabels obtains labels (positive and negative selectors) from a workload spec.
// These can be used as required / forbidden labels in an optimizer model.
func GetLabels(wl crd.ApplicationWorkloadSpec) ([]string, []string, []LabelValidationFailure) {

	validationMessages := ValidateLabels(wl)

	labels := make([]string, 0, 3)
	forbiddenLabels := make([]string, 0, 3)

	for label, value := range wl.Template.Spec.NodeSelector {
		if infrastructure.IsWorkloadLabel(label) && value == "true" {
			labels = append(labels, label)
		}
	}

	if affinity := wl.Template.Spec.Affinity; affinity != nil {
		if nodeAffinity := affinity.NodeAffinity; nodeAffinity != nil {
			if selector := nodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution; selector != nil {
				if len(selector.NodeSelectorTerms) > 0 {
					term := selector.NodeSelectorTerms[0]
					for _, req := range term.MatchExpressions {
						if !infrastructure.IsWorkloadLabel(req.Key) {
							continue
						}
						if req.Operator == corev1.NodeSelectorOpExists {
							labels = append(labels, req.Key)
						} else if req.Operator == corev1.NodeSelectorOpDoesNotExist {
							forbiddenLabels = append(forbiddenLabels, req.Key)
						}
					}
				}
			}
		}
	}
	return labels, forbiddenLabels, validationMessages
}

// DescribeChannelFailures tells you why your channels failed.
func DescribeChannelFailures(channels *crd.ChannelList) []crd.ApplicationCondition {
	ret := make([]crd.ApplicationCondition, 0, len(channels.Items))
	for _, channel := range channels.Items {
		channelStatus := infrastructure.EvaluateChannel(&channel)
		if channelStatus.AppCondition != crd.ApplicationOk {
			ret = append(ret, crd.ApplicationCondition{
				Type:           channelStatus.AppCondition,
				LastUpdateTime: metav1.Now(),
				Entity:         channelStatus.Name,
				Reason:         channelStatus.Reason,
			})
		}
	}
	return ret
}

// IsChannelSource returns true if this workload is the source of channel.
func IsChannelSource(channel crd.Channel, app string, podname string) bool {
	if infrastructure.ChannelSourceApplication(&channel) != app {
		return false
	}
	return channel.Spec.SourceWorkload == podname
}

// IsChannelTarget returns true if this workload is the target of channel.
func IsChannelTarget(channel crd.Channel, app string, podname string) bool {
	if infrastructure.ChannelTargetApplication(&channel) != app {
		return false
	}
	return channel.Spec.TargetWorkload == podname
}
