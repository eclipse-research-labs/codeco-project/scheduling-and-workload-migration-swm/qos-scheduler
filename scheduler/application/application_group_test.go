// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestIsGroupReady(t *testing.T) {
	scenarios := [][]crd.ApplicationPhase{
		{crd.ApplicationWaiting},
		{crd.ApplicationScheduling},
		{crd.ApplicationPending},
		{crd.ApplicationRunning},
		{crd.ApplicationWaiting, crd.ApplicationWaiting},
		{crd.ApplicationWaiting, crd.ApplicationScheduling},
		{crd.ApplicationWaiting, crd.ApplicationPending},
		{crd.ApplicationWaiting, crd.ApplicationRunning},
	}

	expected := []bool{false, false, false, false,
		true, true, true, true}

	appGroup := crd.ApplicationGroup{}
	appGroup.Spec.MinMember = 2

	for idx, scenario := range scenarios {
		var expectedResult string
		apps := make([]crd.Application, len(scenario))
		for i, phase := range scenario {
			apps[i] = crd.Application{}
			apps[i].Status.Phase = phase
		}
		if IsGroupReady(&appGroup, apps) != expected[idx] {
			if expected[idx] {
				expectedResult = "should have been ready"
			} else {
				expectedResult = "should not have been ready"
			}
			t.Errorf("application group with application phases %v %s",
				scenario, expectedResult)
		}
	}
}

func TestIsGroupPhaseChangePermitted(t *testing.T) {
	allPhases := []crd.ApplicationGroupPhase{
		crd.ApplicationGroupWaiting,
		crd.ApplicationGroupOptimizing,
		crd.ApplicationGroupScheduling,
		crd.ApplicationGroupFailed,
	}

	successPairs := map[crd.ApplicationGroupPhase][]crd.ApplicationGroupPhase{
		crd.ApplicationGroupWaiting: {
			crd.ApplicationGroupWaiting,
			crd.ApplicationGroupOptimizing},
		crd.ApplicationGroupOptimizing: {
			crd.ApplicationGroupWaiting,
			crd.ApplicationGroupScheduling,
			crd.ApplicationGroupFailed,
			crd.ApplicationGroupOptimizing},
		crd.ApplicationGroupScheduling: {
			crd.ApplicationGroupScheduling,
			crd.ApplicationGroupFailed,
			crd.ApplicationGroupWaiting},
		crd.ApplicationGroupFailed: {
			crd.ApplicationGroupFailed},
	}
	for _, oldPhase := range allPhases {
		allowedTo, exists := successPairs[oldPhase]
		if exists {
			toPhases := make(map[crd.ApplicationGroupPhase]bool)
			for _, newPhase := range allowedTo {
				toPhases[newPhase] = true
			}
			for _, newPhase := range allPhases {
				permittedDesc := "should not be permitted"
				if toPhases[newPhase] {
					permittedDesc = "should be permitted"
				}
				if IsGroupPhaseChangePermitted(oldPhase, newPhase) != toPhases[newPhase] {
					t.Errorf("phase change from %s to %s %s",
						string(oldPhase), string(newPhase), permittedDesc)
				}
			}
		} else {
			for _, newPhase := range allPhases {
				if IsGroupPhaseChangePermitted(oldPhase, newPhase) {
					t.Errorf("phase change from %s to %s should not be permitted",
						string(oldPhase), string(newPhase))
				}
			}
		}
	}
}
