// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package optimizerclient

import (
	"context"
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"siemens.com/qos-scheduler/scheduler/assignment"
)

// EmptyOptimizer does nothing. This is for tests where I inject a fake
// optimizer plan.
type EmptyOptimizer struct {
	LastCall       map[string]*assignment.Model
	ReceivedCancel map[string]bool
}

func (o *EmptyOptimizer) Optimize(model *assignment.Model, applicationGroup string, namespace string) error {
	if o.LastCall == nil {
		o.LastCall = make(map[string]*assignment.Model)
	}
	o.LastCall[applicationGroup] = model
	return nil
}

func (o *EmptyOptimizer) Cancel(applicationGroup string, namespace string) error {
	if o.ReceivedCancel == nil {
		o.ReceivedCancel = make(map[string]bool)
	}
	o.ReceivedCancel[applicationGroup] = true
	return nil
}

// FakeOptimizer implements a round-robin scheduler that ignores node capabilities
// completely. It is really just for testing.
// This avoids external calls but it still needs a Kubernetes client
// for creating the assignment plan.
type FakeOptimizer struct {
	client.Client
	Ctx context.Context
}

// Cancel implementats cancellation of an optimizer request.
// The FakeOptimizer does not support cancellation.
func (o *FakeOptimizer) Cancel(string, string) error { return nil }

// Optimize obtains a workload assignment and writes it to an assignment plan.
func (o *FakeOptimizer) Optimize(model *assignment.Model, applicationGroup string, namespace string) error {
	assignment, err := o.optimizeAndReturn(model)

	if err != nil {
		return err
	}

	return WriteOptimizerResultToPlan(o.Ctx, assignment, model, applicationGroup,
		namespace, o.Client)
}

// OptimizeAndReturn just does a round-robin schedule.
func (o *FakeOptimizer) optimizeAndReturn(model *assignment.Model) (*assignment.Reply, error) {

	ret := assignment.Reply{}
	ass := &assignment.Assignment{Workloads: make([]*assignment.Assignment_Workload, 0)}

	lastNodeIndex := 0
	for _, app := range model.AppGroup.Applications {
		fmt.Printf("application %s: \n", app.Id)
		for _, wl := range app.Workloads {
			decision := lastNodeIndex
			fmt.Printf("I choose node %s for pod %s. Good luck!\n", model.Infrastructure.Nodes[decision].Id, wl.Id)
			ass.Workloads = append(ass.Workloads, &assignment.Assignment_Workload{
				Application: app.Id,
				Workload:    wl.Id,
				Node:        model.Infrastructure.Nodes[decision].Id,
			})
			for model.Infrastructure.Nodes[lastNodeIndex].NodeType != assignment.Node_COMPUTE {
				lastNodeIndex = (lastNodeIndex + 1) % len(model.Infrastructure.Nodes)
			}
		}
	}

	ret.Assignment = ass

	return &ret, nil
}
