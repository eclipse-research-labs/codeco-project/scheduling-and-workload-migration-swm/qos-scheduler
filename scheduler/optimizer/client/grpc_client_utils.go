// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package optimizerclient

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	//	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"time"
)

type clientFactory interface {
	newSchedulerClient() (assignment.SchedulerClient, error)
}

type grpcClientFactory struct {
	tls                bool
	caFile             string
	serverAddr         string
	serverHostOverride string
}

// Instantiate a grpc client based on factory settings.
func (f *grpcClientFactory) newSchedulerClient() (assignment.SchedulerClient, error) {
	var opts []grpc.DialOption
	if f.tls {
		creds, err := credentials.NewClientTLSFromFile(f.caFile, f.serverHostOverride)
		if err != nil {
			return nil, err
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}

	connParams := grpc.ConnectParams{
		Backoff: backoff.DefaultConfig,
	}
	connParams.Backoff.BaseDelay = 100 * time.Millisecond
	connParams.Backoff.MaxDelay = 3 * time.Second
	opts = append(opts, grpc.WithConnectParams(connParams))

	// TODO: maybe pass a context into this method and then call DialContext here so it
	// is possible to time out or cancel a pending connection.
	conn, err := grpc.Dial(f.serverAddr, opts...)
	if err != nil {
		return nil, fmt.Errorf("failed to establish grpc connection: %v", err)
	}
	return assignment.NewSchedulerClient(conn), nil
}
