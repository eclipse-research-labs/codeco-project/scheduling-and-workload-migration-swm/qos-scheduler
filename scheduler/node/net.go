// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package node

// NetWatcher runs a loop that watches the network.
type NetWatcher struct {
	StopChan *chan struct{}
}

// NewNetWatcher creates a new net watcher.
func NewNetWatcher(stopchan *chan struct{}) (*NetWatcher, error) {
	nw := &NetWatcher{
		StopChan: stopchan,
	}
	return nw, nil
}

// Run is just a stub at the moment.
func (nw *NetWatcher) Run(stopch *chan struct{}) {
}
