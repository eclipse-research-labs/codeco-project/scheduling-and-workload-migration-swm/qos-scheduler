// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package webhooks

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"strings"
	"testing"
)

func TestValidateWorkloadChannels_length(t *testing.T) {
	app := application.MakeApplication("appname", "default", []string{"wla", "wlb"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})

	ok, msg := ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if !ok {
		t.Errorf("expected application to pass validation but %s", msg)
	}
	app = application.MakeApplication("app2",
		"default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1" + strings.Repeat("x", 255), Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})
	ok, _ = ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to fail validation")
	}

	app = application.MakeApplication("appname", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1-really-really-long-basename" + strings.Repeat("x", 255),
				ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})
	ok, msg = ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to fail validation")
	}
	if msg == "" {
		t.Errorf("expected application to fail validation")
	}
}

func TestValidateWorkloadChannels_forbidden_characters(t *testing.T) {

	// Must be rejected because of upper-case character.
	app := application.MakeApplication("appname", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "Wl2", ApplicationName: "app2", Port: 1234},
		})

	ok, _ := ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to fail validation")
	}

	// Must be rejected because of underscore
	app = application.MakeApplication("appname", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl_1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})

	ok, _ = ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to fail validation")
	}
}

func TestValidateWorkloadChannels_duplicates(t *testing.T) {

	app := application.MakeApplication("appname", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
		})

	ok, _ := ValidateWorkloadChannels(*app, []crd.Application{}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to fail validation because of duplicate channels")
	}
}

func TestValidateWorkloadChannels(t *testing.T) {
	app := application.MakeApplication("appname", "default", []string{"wl1"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})

	otherChannels := []crd.Channel{
		infrastructure.MakeChannel("some channel", "default"),
		infrastructure.MakeChannel("some other channel", "other-namespace"),
	}

	ok, _ := ValidateWorkloadChannels(*app, []crd.Application{}, otherChannels)
	if !ok {
		t.Errorf("expected application to pass validation")
	}

	// Name clashes in a different namespace are fine
	otherChannels[1].Name = "if0-to-wl1-app1"
	ok, msg := ValidateWorkloadChannels(*app, []crd.Application{}, otherChannels)
	if !ok {
		t.Errorf("expected no name clashes but got %s", msg)
	}

	// Name clashes should be rejected
	otherChannels[0].Name = "if0-to-wl1-app1"
	ok, _ = ValidateWorkloadChannels(*app, []crd.Application{}, otherChannels)
	if ok {
		t.Errorf("expected rejection for duplicate channel names")
	}

	// But name clashes for the exact same channel are fine, those are updates.
	otherChannels[0].Spec.SourceWorkload = "appname-wl1"
	otherChannels[0].Spec.TargetWorkload = "app1-wl1"
	ok, msg = ValidateWorkloadChannels(*app, []crd.Application{}, otherChannels)
	if !ok {
		t.Errorf("expected name clashes to be ignored for identical endpoints but got %s", msg)
	}
}

func TestValidateWorkloadChannels_same_channels(t *testing.T) {
	app := application.MakeApplication("appname", "default", []string{"wl1"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})
	app2 := application.MakeApplication("appname2", "default", []string{"wl1"},
		[]crd.WorkloadId{
			{Basename: "wl3", ApplicationName: "app1", Port: 1234},
			{Basename: "wl4", ApplicationName: "app2", Port: 1234},
		})

	// The channel names are different: should be fine
	ok, msg := ValidateWorkloadChannels(*app, []crd.Application{*app, *app2}, []crd.Channel{})
	if !ok {
		t.Errorf("expected application to pass validation but %s", msg)
	}

	app2.Spec.Workloads[0].Channels[0].Basename = "if0-to-wl1-app1"

	// Same channel names should be rejected
	ok, _ = ValidateWorkloadChannels(*app, []crd.Application{*app2}, []crd.Channel{})
	if ok {
		t.Errorf("expected application to be rejected because it uses the same channel name as another")
	}

	// Same channel names are ok if the namespaces are different
	app2.Namespace = "other"
	ok, msg = ValidateWorkloadChannels(*app, []crd.Application{*app, *app2}, []crd.Channel{})
	if !ok {
		t.Errorf("expected application to pass validation but %s", msg)
	}
}
