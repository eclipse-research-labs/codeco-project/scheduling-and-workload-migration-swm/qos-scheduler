#!/bin/sh
# SPDX-FileCopyrightText: 2024 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# shellcheck disable=SC1111 # Unicode characters are OK

if output=$(git status --untracked-files=no --porcelain) && [ -n "${output}" ] ; then
	echo "Working directory not clean; stop."
	exit 1
fi
if [ "$(git symbolic-ref --short HEAD)" != main ] ; then
	echo "Not on branch “main”; stop."
	exit 1
fi
version=v$(cat VERSION)
relTag="release-${version}"
if [ -n "$(git tag --list "${relTag}")" ] ; then
	echo "Release for ${version} already exists; stop."
	exit 1
fi

. scripts/gitlab-infos.sh

# Copy files to orphan branch for releases
git ls-tree -r main --name-only >tmp/tracked-files.txt
tar -cf tmp/solver.tar -T tmp/tracked-files.txt

name=$(field siemens branch)
git switch -q "${name}"
git ls-tree -r "${name}" --name-only | xargs -n 400 rm
find . -empty -print0 | xargs -0n 400 rm -rf

tar -xf tmp/solver.tar
rm tmp/solver.tar tmp/tracked-files.txt

# Commit release in release branch
git add --all
git commit -q -m "Release ${version}"

# Tag new commit as a release
msg="OSS release ${version}"
git tag -a "${relTag}" -m "${msg}"

# Publish release
git push --tags
git push
git switch -q main
