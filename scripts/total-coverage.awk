# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

BEGIN { N=0;T=0;S=10 }
/coverage: / {
    match($$0,"coverage: [0-9]+\\.[0-9]+");
    split(substr($$0,RSTART,RLENGTH),a,": ");
    sub("\\.","",a[2]);
    T+=a[2];N+=1
}
/coverage.statements.pct/ {
    match($$0,"value=\"[0-9]+\\.[0-9]+");
    split(substr($$0,RSTART,RLENGTH),a,"=\"");
    sub("\\.","",a[2]);
    T+=a[2];N+=1;S=100
}
END {
    if (S==10) {
        A=sprintf("%0.1f%%",T/(N*10))
    } else {
        A=sprintf("%0.2f%%",T/(N*100))
    }
    print "$(moduleN) coverage:",A,"of statements"
}
