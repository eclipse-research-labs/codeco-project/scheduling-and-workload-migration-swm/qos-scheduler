#!/bin/sh
# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# Parameters:
#   1 - job name
#   2 - image suffix

arch=${1##*-}
moduleN=$(sed -Ene "/^module[ \t]+/{s///;p;}" scheduler/go.mod)
moduleNN=${moduleN##*-}
if [ -n "${CI}" ] ; then
    if [ "${CI}" = local ] ; then
        imageName=script-${moduleNN}
    else
        imageName=script
    fi
else
    imageName=script-${moduleNN}
fi

currB=${CI_COMMIT_REF_SLUG}
if [ -z "${currB}" ] ; then
    currB=$({ git symbolic-ref --short HEAD 2>/dev/null \
              || git describe --all HEAD \
                 | sed -ne "/^heads/{s|heads/||;p;}" \
                 | head -n 1 ; \
            } | sed -Ee "/^[0-9]+-/s/^([0-9]+)-.*/0.\\1/")
fi
mainB=$(if [ "${CI_DEFAULT_BRANCH}" = "${currB}" ] ; then printf "yes" ; fi)
if [ -n "${mainB}" ] ; then
    VERSION_SCRIPT=$(cat build/ci/script/VERSION)
else
    VERSION_SCRIPT=${currB}
fi
VERSION_ALPINE=$(sed -ne "/^VERSION_ALPINE/{s/^.*=//;p;}" Makefile)

export imageDir=build/ci
scripts/build-image.sh \
    "script" "${imageName}" "${VERSION_SCRIPT}_${arch}${2}" "linux/${arch}" \
    "${VERSION_ALPINE}"
