#!/bin/sh
# SPDX-FileCopyrightText: 2024 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# Parameters
#   1 - target repo
#   2 - version (default: current version)

# shellcheck disable=SC1111 # Unicode characters are OK

. scripts/gitlab-infos.sh

if output=$(git status --untracked-files=no --porcelain) && [ -n "${output}" ] ; then
	echo "Working directory not clean; stop."
	exit 1
fi
target=${1}
case "${target}" in
	codeco|eclipse)
		;;
	*)
		echo "Unknown target: ${target}"
		exit 1
		;;
esac
if [ "$(git symbolic-ref --short HEAD)" != main ] && [ -z "${2}" ] ; then
	echo "Version not given; stop."
	exit 1
fi
version=v${2:-$(cat VERSION)}
relTag="release-${version}"
if [ -z "$(git tag --list "${relTag}")" ] ; then
	echo "Release for ${version} does not exist; stop."
	exit 1
fi

# Get archive for files in release
git checkout -q -b __export__ "${relTag}"
git ls-tree -r __export__ --name-only >tmp/tracked-files.txt
tar -cf tmp/solver.tar -T tmp/tracked-files.txt
msg="Export to $(field "${target}" host)"
git tag -a "${target}$(date "+-%F")" -m "${msg}"

# Clone target repository
git clone -q "$(gitlab_url "${target}" scheduler)" "$(repo)"
cd "$(repo)" || exit 1

# Preserve settings for Solver and Optimizer
f=helm/qos-scheduler/charts/solver/values.yaml
solver_prefix="$(yq '.image.repositoryPrefix' "${f}")"
solver_repo="$(yq '.image.repository' "${f}")"
solver_tag="$(yq '.image.tag' "${f}")"
f=helm/qos-scheduler/charts/optimizer/values.yaml
optimizer_prefix="$(yq '.image.repositoryPrefix' "${f}")"
optimizer_repo="$(yq '.image.repository' "${f}")"
optimizer_tag="$(yq '.image.tag' "${f}")"

# Update branch for release imports
name=$(target_field branch)
git checkout -q -B "${name}"
git ls-tree -r "${name}" --name-only | xargs -n 400 rm
find . -empty -print0 | xargs -0n 400 rm -rf
tar -xf ../solver.tar
rm ../solver.tar ../tracked-files.txt

fn=$(mktemp "/tmp/$(basename "${0}").XXXXXX") || exit 1
cmdChNs=$(printf "s/${siemens_namespace}/%s/" "$(field "${target}" namespace)")

# Use “open-source” README
mv README_release.md README.md

# Insert proper namespace in Helm README
f=helm/qos-scheduler/README.md
sed -E -e "${cmdChNs}" "${f}" >"${fn}"
mv "${fn}" "${f}"

# Insert namespace and repository in global values file
f=helm/qos-scheduler/values.yaml
prefix=$(field "${target}" repo_prefix)
if [ -z "${prefix}" ] ; then
    prefix=$(printf "%s%s/%s/%s/" \
                    "$(target_field host)" "$(target_field port)" \
                    "$(target_field group)" "$(target_field scheduler)")
fi
sed -E -e "${cmdChNs}" -e "/(repositoryPrefix: ).+/s||\\1${prefix}|" \
    "${f}" >"${fn}"
mv "${fn}" "${f}"

# Restore image properties in Solver’s values file
f=helm/qos-scheduler/charts/solver/values.yaml
cmdP="s|(repositoryPrefix: ).+|\\1\"${solver_prefix}\"|"
cmdR="s|(repository: ).+|\\1\"${solver_repo}\"|"
cmdT="s|(tag: ).+|\\1\"${solver_tag}\"|"
sed -E -e "/^# External/,/^\\$/{${cmdP};${cmdR};${cmdT};}" "${f}" >"${fn}"
mv "${fn}" "${f}"

# Restore image properties in Optimizer’s values file
f=helm/qos-scheduler/charts/optimizer/values.yaml
cmdP="s|(repositoryPrefix: ).+|\\1\"${optimizer_prefix}\"|"
cmdR="s|(repository: ).+|\\1\"${optimizer_repo}\"|"
cmdT="s|(tag: ).+|\\1\"${optimizer_tag}\"|"
sed -E -e "/^# External/,/^\\$/{${cmdP};${cmdR};${cmdT};}" "${f}" >"${fn}"
mv "${fn}" "${f}"

# Insert names in templates for Prometheus
for n in controller network ; do
    f="helm/qos-scheduler/charts/prometheusOperator/templates/${n}_monitor.yaml"
    sed -E -e "${cmdChNs}" "${f}" >"${fn}"
    mv "${fn}" "${f}"
done

# Increment version
git checkout -q VERSION
awk -v FS=. -f scripts/inc-version.awk VERSION >VERSION.new
mv VERSION.new VERSION

# Commit and push changes
git add --all
msg=$(printf "Import %s\n\nImport %s from %s as v%s." \
			 "${version}" "${version}" "$(field siemens host)" \
			 "$(cat VERSION)")
git commit -q -m "${msg}"
git push --set-upstream origin "${name}"
cd ../..
rm -rf "$(repo)"
git switch -q main
git push --tags
git branch -q -D __export__
