# SPDX-FileCopyrightText: 2024 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# shellcheck shell=sh
# shellcheck disable=SC2034 # intentional: indirect references
siemens_host=code.siemens.com
siemens_port=
siemens_group=itp_cloud_research
siemens_scheduler=qos-scheduler
siemens_solver=qos-solver
siemens_branch=oss-release
siemens_repo_prefix=
siemens_namespace=controllers-system

codeco_host=colab-repo.intracom-telecom.com
codeco_port=:5050
codeco_group=colab-projects/he-codeco/swm
codeco_scheduler=qos-scheduler
codeco_solver=workload-placement-solver
codeco_branch=import
codeco_repo_prefix=
codeco_namespace=he-codeco-swm

eclipse_host=gitlab.eclipse.org
eclipse_port=
eclipse_group=eclipse-research-labs/codeco-project/scheduling-and-workload-migration-swm
eclipse_scheduler=qos-scheduler
eclipse_solver=workload-placement-solver
eclipse_branch=import
eclipse_repo_prefix=hecodeco/swm-
eclipse_namespace=he-codeco-swm

# shellcheck disable=SC2120 # intentional: optional parameters
repo () { printf "tmp/release-%s" "${1:-${target}}" ; }
field () { eval "echo \$${1}_${2}" ; }
target_field () { eval "echo \$${target}_${1}" ; }

gitlab_url () { # target, project
	printf "https://%s/%s/%s.git" \
		   "$(field "${1}" host)" \
		   "$(field "${1}" group)" \
		   "$(field "${1}" "${2}")"
}
