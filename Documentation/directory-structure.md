<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# An overview of what is where

The root of the git repository contains  the following directories:

1. config: yaml files for the demo
2. container: implementation of the ci/cd pipeline.
3. Documentation: several pieces of documentation, background information, uml diagrams.
4. optimizer: some c++ code that implements a fake optimizer for tests. This uses c++ in order to test code and build paths more thoroughly, but it does not require the custom-built optimizer binary or its libraries.
5. scheduler: the code for the custom resources and their controllers lives here, as well as the code for the network and optimizer services. ALso the directory contains the code for the custom scheduler plugin and the QoS CRD client.
6. helm: the Helm chart and its subcharts for installing the whole system; the CRDs (yaml files)

Also at the root of the git repository there is a shell script:

1. start_cluster.sh brings up a kind (Kubernetes in docker) cluster locally and starts a demo system on it.
