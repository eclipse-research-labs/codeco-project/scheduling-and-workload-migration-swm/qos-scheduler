<!---
SPDX-FileCopyrightText: 2024 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# ADR000 - Title
Creation date: YYYY-MM-DD

## Motivation/ Problem Statement
...

## Alternatives and Evaluation

### A. Alternative A

Description

#### Advantages

- ...

#### Disadvantages

- ...

### B. Alternative B

Description

#### Advantages

- ...

#### Disadvantages

- ...


## Decision
...

## Consequences
...
