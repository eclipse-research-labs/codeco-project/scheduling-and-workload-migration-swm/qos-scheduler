<!---
SPDX-FileCopyrightText: 2024 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# ADR100 - Rescheduling result

Creation date: 2024-08-15

## Motivation/ Problem Statement

- Situation: The QoS-Scheduler calls the Solver/Optimizer to perform a re-scheduling, i.e. there is already an existing placement. The Solver/Optimizer calculates a new placement, which may be different to the existing one.
- Problem: Shall the Solver/Optimizer return the complete new placement or only the delta to the existing placement?

## Alternatives and Evaluation

### A. Return complete new placement

The Solver/Optimizer always returns the complete new placement.

#### Advantages

- There is no deviation possible, the Solver/Optimizer statement explicitely contains the intended state

#### Disadvantages

- Not compact, we need to always return the complete placement, even if no or little changes

### B. Return only delta of new placement to existing placement

The Solver/Optimizer only returns the changed placements:
- For workloads with changed assignment: workload, new node (empty of no longer assigned), old node
- For channels with changed assignment: channel, new path (empty if no longer assigned), old path

#### Advantages

- Compact return value (only deltas)
- QoS-Solver does not need to compare new placement with existing placement to detect changes, it can directly work with the returned delta placement

#### Disadvantages

- Only transmitting delta could lead to error cumulation. This is however not critical, as if there are errors, the actual state (incl. the error) will be trasnmitted to the Solver/Optimizer (they are stateless) with the next placement call, i.e. errors cannot cumulate.

## Decision

Alternative B. Return only delta

## Consequences

- Implement accordingly in Solver, Optimizer, and QoS-Scheduler
- gRPC interface already fit for this solution (but update documentation/ comments)
