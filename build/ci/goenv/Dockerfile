# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

ARG VERSION=1.22
FROM golang:${VERSION}-alpine as tools

RUN apk add --no-cache binutils curl

ENV GOPROXY="https://proxy.golang.org,direct"
# This is the version of the envtest tools.
ENV ENVTEST_K8S_VERSION=1.23.5

WORKDIR /root

# /go/bin ; combine commands to avoid multiple snapshots
RUN\
 go install honnef.co/go/tools/cmd/staticcheck@v0.4.3 ;\
 go install github.com/jstemmer/go-junit-report/v2@latest ;\
 go install github.com/onsi/ginkgo/v2/ginkgo@v2.14.0 ;\
 go install github.com/golang/mock/mockgen@v1.6.0 ;\
 go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28 ;\
 go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2 ;\
 go install github.com/protobuf-tools/protoc-gen-deepcopy@latest ;\
 go install github.com/google/addlicense@latest ;\
 mkdir generator ; cd generator ;\
 go mod init generator ;\
 go install sigs.k8s.io/controller-tools/cmd/controller-gen@latest ;\
 strip /go/bin/*

# /usr/local/bin & /usr/local/include
RUN v="3.15.8" ;\
 path=releases/download/v${v}/protoc-${v}-linux-x86_64.zip ;\
 curl -sL -o ar.zip "https://github.com/protocolbuffers/protobuf/${path}" ;\
 unzip ar.zip -d /usr/local/ ;\
 rm ar.zip

WORKDIR /envtest
RUN v="0.18.5" ; export GOBIN=/envtest ;\
 path=controller-runtime/archive/refs/tags/v${v}.tar.gz ; \
 curl -sL https://github.com/kubernetes-sigs/${path} | tar -xz ;\
 cd controller-runtime-${v}/tools/setup-envtest ;\
 go install . ;\
 cd - ; rm -rf controller-runtime-${v} ;\
 strip setup-envtest ;\
 ./setup-envtest use ${ENVTEST_K8S_VERSION}

FROM golang:${VERSION}-alpine

# protobuf is a shared library. Having the headers and the library here
# makes it possible to have a "fake" setup for testing without the full
# optimizer library.
RUN apk add --no-cache git build-base curl bash libprotobuf protobuf-dev openssl yq

ENV GOPROXY="https://proxy.golang.org,direct"
ENV ENVTEST_ASSETS_DIR=/envtest

COPY --from=tools /go/bin /go/bin
COPY --from=tools /usr/local/bin /usr/local/bin
COPY --from=tools /usr/local/include /usr/local/include
COPY --from=tools /root/.local /root/.local
COPY --from=tools /envtest /envtest

RUN go install github.com/onsi/gomega@latest ; echo done
RUN go install github.com/golang/mock@latest ; echo done

# It used to be possible to copy just the go.mod files and run "go mod
# download", but as of go 1.17 the transitive dependencies in the go.mod
# files are no longer being downloaded and then go later "go mod tidy" or
# "go build" try to download whatever is missing. Since this container is
# supposed to contain everything needed to build, the current workaround is
# to copy the source code and then make sure "go mod tidy" determines the
# correct dependencies. I think it is possible to copy the module cache
# instead but I'm not sure that would be an overall improvement.
WORKDIR /root
COPY ./scheduler ./scheduler
RUN cd scheduler && go mod download && go mod tidy
