# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

export imageDir:=tmp/img
ci-src-dir=build/ci/$(1)
pi-src-dir=build/package/$(1)

define image-tag # version,arch
$(if $(subst main,,$(CI_COMMIT_REF_SLUG)),$(CI_COMMIT_REF_SLUG),$(1))$\
$(if $(2),_$(2))$\
$(if $(strip $(TAG_SUFFIX)),$(strip $(TAG_SUFFIX)))
endef

define make-image/recipe # context,platform,baseVersion
scripts/build-image.sh $\
    "$(1)" "$$($(1)IN)" "$$($(1)V)" $\
    "$(2)"$\
    $(if $(strip $(3)), "$(strip $(3))")
endef

define make-image/vars # name,version,arch,platform,context,baseVersion
$(5)D:=$(imageDir)/$(5)
$(5)V:=$(call image-tag,$(2),$(3))
$(5)IN:=$(if $(CI),$(if $(subst local,,$(CI)),$(1),$(1)-$(moduleNN)),$(1)-$(moduleNN))
$(5)F:=$$($(5)D)/iid.txt
define $(5)R
$(call make-image/recipe,$(5),$(4),$(6))
endef
endef

define make-image/cleaner # prefix,name,context
.PHONY .IGNORE: clean-$(1)i-$(3)
clean-$(1)i-$(3):
>id=`cat $$($(3)F) 2>/dev/null` && docker image rm --force $$$${id}
>rm -rf $$($(3)D)
endef

define make-image/aux # prefix,name,archs
.PHONY: $(1)c-$(2) $(1)i-$(2) clean-$(1)i-$(2)
$(1)-contexts: $(1)c-$(2)
$(1)c-$(2):$(if $(local-build), $(foreach a,$(3),$(1)c-$(2)-$(a)))
$(1)-images: $(1)i-$(2)
$(1)i-$(2):$(if $(local-build), $(foreach a,$(3),$(1)i-$(2)-$(a)))
clean-$(1)i-$(2): $(foreach a,$(3),clean-$(1)i-$(2)-$(a))
endef

define make-image/goals # prefix,name,version,arch,platform,context,prereqs,files
.PHONY: $(1)c-$(6) $(1)i-$(6)
$(1)c-$(6): $$(addprefix $$($(6)D)/,$\
    .dockerignore Dockerfile build.sh$\
    $(if $(strip $(7)), $(notdir $(7)))$\
    $(if $(strip $(8)), $(8)))
$(call copy-rule,$$($(6)D),$(call $(1)i-src-dir,$(2)),%)
$$($(6)D)/.dockerignore: | $$($(6)D)/
>printf "**$(if $(strip $(7) $(8)),$\
    $(foreach f,$(strip $(notdir $(7)) $(8)),\n!$(f)))\n" $\
    >$$@
$$($(6)D)/build.sh: export recipe=$$($(6)R)
$$($(6)D)/build.sh: | $$($(6)D)/
>printf "$$$${recipe}" >$$@
$(1)i-$(6): $$($(6)F)
$$($(6)F): $(1)c-$(6)
>. $$($(6)D)/build.sh
endef

define make-image # prefix,name,version,arch,platform,prereqs,files,baseVersion
$(call make-image/vars,$(2),$(3),$(4),$(5),$(2)$(if $(4),-$(4)),$(8))
$(call make-image/goals,$(1),$(2),$(3),$(4),$(5),$(2)$(if $(4),-$(4)),$(6),$(7))
$(call make-image/cleaner,$(1),$(2),$(2)$(if $(4),-$(4)))
endef

define image-prerequisites # name
$(strip $(shell yq '.".$(1)-rules".rules[0].changes[]' .gitlab-ci.yml))
endef

define make-images # prefix,name,version,archs,prereqs,files,baseVersion
$(call make-image/aux,$(1),$(2),$(4))
$(foreach a,$(4),$(call make-image,$(1),$(2),$(3),$(a),linux/$(a),$(5),$(6),$(7))
)$(foreach a,$(4),$$($(2)-$(a)F)): $(call image-prerequisites,$(2))
endef

define ci-images # name,version,archs,prereqs,files,baseVersion
$(eval $(call make-images,c,$(1),$(2),$(3),$(4),$(5),$(6)))
endef

define production-image # name,version,arch,platform,prereqs,files,baseVersion
$(eval $(call make-image,p,$(1),$(2),$(3),$(4),$(5),$(6),$(7)))
endef

define tool-images/recipe # nameI,arch,nameB
$(if $(local-build),,cp $$($(or $(3),$(1))B_$(2)) $$($(1)-$(2)D)
)$(call make-image/recipe,$(1)-$(2),linux/$(2),$(VERSION_ALPINE))
endef

define tool-images # nameI,archs,nameB
$(eval $(call make-image/aux,p,$(1),$(2))
$(foreach a,$(2),$\
$(call make-image,p,$(1),$(moduleV),$(a),linux/$(a),$\
    $($(or $(3),$(1))B_$(a)),,$(VERSION_ALPINE))
define $(1)-$(a)R
$(call tool-images/recipe,$(1),$(a),$(3))
endef
$$($(1)-$(a)D)/$(or $(3),$(1)): $$($(or $(3),$(1))B_$(a))
>cp $$< $$@
))
endef

define manifest-for/cmd # image,src-tag,dst-tag,archs
$(if $(local-build),$(foreach a,$(4),
>docker push $(1):$(2)_$(a))
>docker manifest create --insecure $(1):$(3) $\
    $(addprefix $(1):$(2)_,$(4)),
>manifest-tool$(if $(strip $(CI_REGISTRY_IMAGE)), $\
    --username $$$${CI_REGISTRY_USER} --password $$$${CI_REGISTRY_PASSWORD}) $\
    push from-args $\
    --platforms $(subst $(space),$(comma),$(addprefix linux/,$(4))) $\
    --template $(1):$(call image-tag,$(2),ARCH) $\
    --target $(1):$(call image-tag,$(3),))
endef

define manifest-for # image,src-tag,dst-tag,archs
$(call manifest-for/cmd,$\
    $(if $(CI_REGISTRY_IMAGE),$\
        $(CI_REGISTRY_IMAGE)/)$(1)$(if $(CI),,-$(moduleNN)),$\
    $(2),$(3),$(4))
endef

define multi-arch-image # prefix,image,version,archs
$(eval .PHONY: $(1)-$(2)
$(1)-$(2):$(if $(local-build), $(foreach a,$(4),$$($(2)-$(a)F)))$\
$(if $(and $(local-build),$(if $(CI),,t)),
>@echo "Skip creation of multi-arch image; use CI=local.",$\
$(call manifest-for,$(2),$(3),$(3),$(4))$(if $(main-branch),$\
$(call manifest-for,$(2),$(3),latest,$(4)),)))
endef
