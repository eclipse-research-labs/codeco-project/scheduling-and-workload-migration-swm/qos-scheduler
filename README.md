<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# SWM QoS Scheduler

The SWM QoS Scheduler is an extension of the Kubernetes scheduler that
implements an extended model for describing enhanced application requirements
and infrastructure capabilities, according to the Seamless Computing concept.
“QoS” is short for “quality of service”.

The SWM QoS Scheduler considers the extended application (QoS) requirements
when placing application components (further called workloads) to compute
nodes in the Kubernetes cluster. The main enhancements beyond the existing
Kubernetes scheduler are:

- schedule multiple workloads (Kubernetes pods) at once, considering
  dependencies between these workloads
- network-aware scheduling: make Kubernetes aware of the network topology,
  connectivity, and resource availability between worker nodes, and consider
  this in the scheduling decision (according to the communication requirements
  of the applications)

## Getting Started

For installing the SWM QoS Scheduler (including all CRDs and required
containers) and to deploy a small demo application, see [quick start][1].

[1]: Documentation/quick-start-oss.md

## License

All code files are licensed under Apache license version 2.0.  All
documentation is licensed under Creative Commons Attribution-ShareAlike 4.0
International.
