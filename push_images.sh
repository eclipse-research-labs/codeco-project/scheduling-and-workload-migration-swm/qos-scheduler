#!/bin/bash
# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# This script can be used to transfer all required container images to docker hub
# Prerequisites: 
# - (sudo) Credentials for source docker registry need to be stored (~/.docker/config.json)
# - (sudo) Logged in to docker hub as user hecodeco (docker login -u hecodeco)
# - (sudo) Execute script

SCHEDULER_VERSION="1.3.2"
SOLVER_VERSION="0.3.15"
DOCKERHUB_VERSION_SCHEDULER="2.0.0"
DOCKERHUB_VERSION_SOLVER="2.0.0"

SCHEDULER_PREFIX="cr.siemens.com/itp_cloud_research/qos-scheduler/"
SOLVER_PREFIX="cr.siemens.com/itp_cloud_research/qos-solver/"
DOCKERHUB_PREFIX="hecodeco/swm-"

SCHEDULER_IMAGES=(cni-vlan validator scheduler node-daemon controller network-k8s network-l2sm network-tsn network-topology)
SOLVER_IMAGES=(solved)

for i in "${SCHEDULER_IMAGES[@]}"; do 
    img="$SCHEDULER_PREFIX$i"
    ver=$SCHEDULER_VERSION
    img_tag="$img:$ver"
    echo "Pulling image $img_tag"
    docker pull ${img_tag}_amd64
    docker pull ${img_tag}_arm64
#    img_id_amd64=$(docker images | grep $img.*${ver}_amd64 | awk {'print $3'})
#    img_id_amd64=$(docker images | grep $img.*${ver}_amd64 | awk {'print $3'})
    docker_img_tag="$DOCKERHUB_PREFIX$i:$DOCKERHUB_VERSION_SCHEDULER"
    echo "Tagging image $docker_img_tag"
    docker tag ${img_tag}_amd64 ${docker_img_tag}_amd64
    docker tag ${img_tag}_arm64 ${docker_img_tag}_arm64
    echo "Pusing image $docker_img_tag"
    docker push ${docker_img_tag}_amd64
    docker push ${docker_img_tag}_arm64
    echo "Building multiarch image ${docker_img_tag}"
    manifest-tool \
    push from-args \
    --platforms linux/amd64,linux/arm64 \
    --template ${docker_img_tag}_ARCH \
    --target ${docker_img_tag}
    done

for i in "${SOLVER_IMAGES[@]}"; do 
    img="$SOLVER_PREFIX$i"
    ver=$SOLVER_VERSION
    img_tag="$img:$ver"
    echo "Pulling image $img_tag"
    docker pull ${img_tag}_amd64
    docker pull ${img_tag}_arm64
#    img_id=$(docker images | grep $img.*$ver | awk {'print $3'})
    docker_img_tag="$DOCKERHUB_PREFIX$i:$DOCKERHUB_VERSION_SOLVER"
    echo "Tagging image $docker_img_tag"
    docker tag ${img_tag}_amd64 ${docker_img_tag}_amd64
    docker tag ${img_tag}_arm64 ${docker_img_tag}_arm64
    echo "Pusing image $docker_img_tag"
    docker push ${docker_img_tag}_amd64
    docker push ${docker_img_tag}_arm64
    echo "Building multiarch image ${docker_img_tag}"
    manifest-tool \
    push from-args \
    --platforms linux/amd64,linux/arm64 \
    --template ${docker_img_tag}_ARCH \
    --target ${docker_img_tag}
    done
